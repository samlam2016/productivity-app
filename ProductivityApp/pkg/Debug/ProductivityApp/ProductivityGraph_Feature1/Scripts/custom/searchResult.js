﻿

var resize768 = false; //global resize checker
var businessCard = [];	//global array for business card
var bufferBusinessCard = [];

var outlookPageUrl = []; //global array to store page url 


function searchResultInit() {
        $('#sp13custom-bodyContainer').show();
		loadQueryTitle(undefined, true);
		searchHandler();
		searchBoxEventHandler();
		
		//change expand/collapse in lower res
		setTimeout(function(){
			if($(window).width() <=768){
				$('.SearchResultContent_Right .expand-button').removeClass('expand');
				$('.SearchResultContent_Right .expand-button').html('–');
	
				resize768 = true;
			}
			else{
	
				resize768 = false;
			}
		},1000);
		
		searchBoxHandlerResize();
    
		getOnedriveSearch(getHashString(true), 1, true);
		getMail(userEmail, getHashString(true), 1, true);

        //still use token for yammer
		var yammerToken = getCookie("yammer_access_token");
		if (yammerToken) {
		    loadYammerSearch(yammerToken, getHashString(true), 1, true);
		}
        else {
        	//$('.search-box-container.yammer-box .search-box-content').html('<div id="yamButtonContainer"><div class="yamButtonElement"><img class="yamButtonImageIcon" src="../Content/img/yammer-icon.png" /><div class="yamButtonTitle">VIEW YAMMER SEARCHES</div></div><div id="yamButton" class="yamButtonElement">LOGIN</div></div>');
		}

		getSPSearch(getHashString(true), 1, true);
}

function refreshYammer() {
    $('body').addClass('animated fadeOut');
    $('body').one('animationend', function () {
        $('body').removeClass('animated fadeOut');
        $('#yamButtonContainer').attr('style', null);
        var yammerToken = getCookie("yammer_access_token");
        if (yammerToken) {
            loadYammerSearch(yammerToken, getHashString(), 1, true);
        }
        $('body').addClass('animated fadeInFull');
    });
}

function searchHandler(){
	$('.custom-search-button').on('click',function(event){
		var search = $('.custom-search-box').val().trim();
		if (search != '') {

		    window.location.hash = 'k=' + search;

            //reset collapsed and expanded
		    if ($(window).width() > 768) {
		        $('.search-box-container').removeClass('collapsed');
		        $('.search-box-container').removeClass('expanded');
		        $('.search-box-container .expand-button').html('+');
		    }
		    else {
		        $('.search-box-container').removeClass('collapsed');
		        $('.search-box-container').removeClass('expanded');
		        $('.search-box-container .expand-button').html('–');
		    }

		    loadQueryTitle(search);

			var yammerToken = getCookie("yammer_access_token");
			if (yammerToken) {
			    //loadYammerSearch(yammerToken, getHashString(), 1);
			    loadYammerSearch(yammerToken, search, 1, true);
			}
            /*
			else {
			    //window.location.href = _spPageContextInfo.webAbsoluteUrl + '/Pages/Yammer-Login.aspx' + "#k=" + yammerID;
			}
            */
			
			getMail(userEmail, search, 1, true);

			getSPSearch(search,1,true);
			getOnedriveSearch(search,1,true);
            

		    //filterBusinessCard(getHashString(),bufferBusinessCard);
		}
	});
	
	$('.custom-search-box').keypress(function(e){
		if(event.keyCode == 13){
    	    $('.custom-search-button').trigger('click');
    	}
	});
}

//to set all search box height to be the same
function searchBoxHeightHandler() {
    //reset
    $('.SearchResultContent_Right .search-box-container').attr('style', null);

    var height = [];
    //change handler to search-box-container
    $('.SearchResultContent_Right .search-box-container').each(function (index, value) {
	//$('.SearchResultContent_Right .ms-webpartzone-cell').each(function(index,value){
		height[index] = $(this).height();
	});
	
	//var i = height.indexOf(Math.max.apply(Math, arr));
	var maxHeight = Math.max.apply(Math, height);
	$('.SearchResultContent_Right .search-box-container').css('height',maxHeight);
}

//hash string changed to inputbox value
function getHashString(first) {
   
    if (first) {
        return webPartConfig['k'];
    }

    var sParam = document.location.href.split("#")[1];

      if(!sParam){
      	sParam = window.location.search.substring(1).split('=');
      	sParam = decodeURIComponent(sParam[1]);
      }
      else{
      	sParam = decodeURIComponent(sParam.split('=')[1]);
      }

      if (!sParam) {
          return '';
      }
      
  	  	return sParam;
     
}

function loadQueryTitle(a,first){
	var searchString = getHashString(first);
	
	if(a!=undefined){
		searchString = a;
	}
		//reset
		//console.log(searchString);
		/*$('.custom-search-result-title').text('SEARCH RESULT');*/
	if (searchString != "" && searchString != undefined && searchString != null) {
			/*
			var title = $('.custom-search-result-title').text();
			title += ": " + searchString;
			*/
			$('.custom-search-box').val(searchString);
		}
}


//resize768, checker for mobile res
function searchBoxHandlerResize(){
	$(window).on('resize',function(){
	    if ($(window).width() > 768) {

            //resize search box height
	        searchBoxHeightHandler();

			if(resize768){
			    //reset
			    //change handler to search-box-container
                //ms-webpartzone-cell
			    $('.SearchResultContent_Right .search-box-container').removeClass('collapsed expanded');
				//$('.SearchResultContent_Left').removeClass('expanded');   //no refiner
				$('.SearchResultContent_Right .expand-button').addClass('expand');
				$('.SearchResultContent_Right .expand-button').html('+');
				
				resize768 = false; 
			}
		}
		else if($(window).width() <=768){
			if(!resize768){
				//reset
				$('.SearchResultContent_Right .search-box-container').removeClass('collapsed expanded');
				//$('.SearchResultContent_Left').removeClass('expanded');
				//change expand button to collapse
				$('.SearchResultContent_Right .expand-button').removeClass('expand');
				$('.SearchResultContent_Right .expand-button').html('–');
				
				resize768 = true;
			}
		}
	});
}

function searchBoxEventHandler(){
	$('.search-box-expand').on('click',function(event){
			var parentWebpart = $(this).closest('.search-box-container');
			//css3 transition
			if($(window).width() > 768){
				$('.SearchResultContent_Right .search-box-container').not(parentWebpart).toggleClass('collapsed');
				parentWebpart.toggleClass('expanded');
			}				
			else{
				parentWebpart.toggleClass('collapsed');
			
			}
			if($(this).find('.expand-button').hasClass('expand')){
					//hide all the other search boxes
					$(this).find('.expand-button').toggleClass('expand');				
					$(this).find('.expand-button').html('–');
			}
			else{			
				$(this).find('.expand-button').toggleClass('expand');
				$(this).find('.expand-button').html('+');
			}
	});
}


//need to change
function getMail(email,searchString,page,heightAdjust){
    try {
        if (searchString != undefined && searchString != null && searchString != '') {
            
            //local search box modal
            $('.search-box-container.mail-box .search-box-content').append('<div class="search-modal"></div>');

            var accessToken = getAccessToken();

            //token not found
            if (checkIsNull(accessToken)) {
                refreshAccessToken(function () {
                    getMail(email, searchString, page, heightAdjust);
                }, function (err) {
                    if (err == 'EG0004') {
                        $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult"><div class="ms-textLarge ms-srch-result-noResultsTitle">You are not authorized to access this feature.</div></div>');
                    }
                });
            }
            else {
                var endpointUrl = endpoint + '/api/mails/search?accessToken=' + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + '&clientId=' + encodeURIComponent(office365ID) + '&tenantId=' + encodeURIComponent(tenantId) + '&email=' + encodeURIComponent(email) + '&licenseKey=' + encodeURIComponent(licenseKey) + '&searchString=' + searchString;
                var charLimit = 90;


                if (page > 1) {
                    endpointUrl = outlookPageUrl[page - 1];
                }
                else if (page == 1) {
                    //reset
                    outlookPageUrl = [];
                    outlookPageUrl[page - 1] = endpointUrl;
                }

                $.ajax({
                    url: endpointUrl,
                    method: 'GET',
                    async: true,
                    success: function (json) {

                        if (json.StatusCode == 0) {
                            //reset
                            var html = "";
                            var msgs = json.result;
                            if (msgs.length > 0) {
                                $(msgs).each(function (index) {
                                    var r = new RegExp('<', 'gi');
                                    var s = new RegExp('>', 'gi');

                                    var cleanBody = this.Body.replace(r, '&lt;').replace(s, '&gt;');

                                    var regex = RegExp(searchString, 'gi');
                                    var body = textAbstract(cleanBody, charLimit);
                                    body = body.replace(regex, '<strong>$&</strong>');

                                    var subject = this.Subject.replace(regex, '<strong>$&</strong>');

                                    var fullBody = cleanBody.replace(regex, '<strong>$&</strong>');

                                    html += '<div class="mail-item">'
                                                + '<a href="' + this.WebLink + '" target="_blank">'
                                                + '<div class="mail-sender">'
                                                + this.SenderName
                                                + '</div>'
                                                + '<div class="mail-subject">'
                                                + subject
                                                + '</div>'
                                                + '<div class="mail-bodypreview">'
                                                + body
                                                + '</div>'
                                                + '<div class="mail-fullBody">'
                                                + fullBody
                                                + '</div>'
                                                + '</a>'
                                                + '</div>';
                                });

                                $('.search-box-container.mail-box .search-box-content').addClass('animated fast fadeOutFull');
                                $('.search-box-container.mail-box .search-box-content').one('animationend', function () {
                                    $('.search-box-container.mail-box .search-box-content').removeClass('animated fast fadeOutFull');
                                    $('.search-box-container.mail-box .search-box-content').html(html);
                                    $('.search-box-container.mail-box .search-box-content').addClass('animated fast fadeInFull');


                                    //need to generate paging, there is a next page
                                    if ((json.nextLink != '') && (page == 1)) {
                                        generatePaging($('.search-box-container.mail-box .search-box-content'), page, 'first', 'custom-outlook');
                                    }
                                        //middle page
                                    else if ((json.nextLink != '') && page > 1) {
                                        generatePaging($('.search-box-container.mail-box .search-box-content'), page, 'middle', 'custom-outlook');
                                    }
                                        //last page
                                    else if ((json.nextLink == '') && (page > 1)) {
                                        generatePaging($('.search-box-container.mail-box .search-box-content'), page, 'last', 'custom-outlook');
                                    }



                                    if (heightAdjust) {
                                        //adjust search box height
                                        searchBoxHeightHandler();
                                    }
                                });

                                //populate array url
                                if (json.nextLink) {
                                    var uri = decodeURIComponent(json.nextLink);
                                    var skipToken = uri.split('&$skiptoken=')[1];
                                    outlookPageUrl[page] = endpoint + '/api/mails/search?accessToken=' + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + '&clientId=' + encodeURIComponent(office365ID) + '&tenantId=' + encodeURIComponent(tenantId) + '&email=' + encodeURIComponent(email) + '&licenseKey=' + encodeURIComponent(licenseKey) + '&searchString=' + searchString + '&link=' + skipToken;
                                }

                            }
                            else {
                                $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>           </ul>                        </div>');

                                if (heightAdjust) {
                                    //adjust search box height
                                    searchBoxHeightHandler();
                                }
                            }

                        }
                        //web service exception error
                        else {
                            console.log("Error Code: " + json.error_code + ", message: " + json.error_message);
                            
                            //token invalid
                            if (json.error_code == 'ET0001') {
                                refreshAccessToken(function () {
                                    getMail(email, searchString, page, heightAdjust);
                                },
                                function (err) {
                                    if (err == 'EG0004') {
                                        $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult"><div class="ms-textLarge ms-srch-result-noResultsTitle">You are not authorized to access this feature.</div></div>');
                                    }
                                });
                            }
                            //no credentials
                            else if (json.error_code == 'EG0004') {
                                $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult"><div class="ms-textLarge ms-srch-result-noResultsTitle">You are not authorized to access this feature.</div></div>');
                               
                            }
                            else {
                                $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                          </ul>                        </div>');
                            }
                        }
                    },
                    error: function (err) {
                        console.log('error: ' + err);
                        $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                    </ul>                        </div>');
                    }
                });
            }
        }
        else {
            $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                    </ul>                        </div>');
        }
    }
    catch (ex) {
        console.log(ex.message);
        if (ex.message == 'EG0004') {
            $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult"><div class="ms-textLarge ms-srch-result-noResultsTitle">You are not authorized to access this feature.</div></div>');
        }
        else {
            $('.search-box-container.mail-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                               </ul>                        </div>');
        }
    }
}

//can be combined into a common function with one drive search later
function getSPSearch(searchString, page, heightAdjust) {
    try {
        if (searchString != undefined && searchString != null && searchString != '') {

            //local search box modal
            $('.search-box-container.sharepoint-box .search-box-content').append('<div class="search-modal"></div>');
            var numPerPage = 10;
            var charLimit = 90;
            var tenantUrl = window.location.protocol + '//' + window.location.host.replace(/(-[\w]*)/, '');
            var hostUrl = _spPageContextInfo.siteAbsoluteUrl.replace(/(-[\w]*)/, '');
            var scriptbase = tenantUrl + "/_layouts/15/";
            getScriptCache(scriptbase + "SP.RequestExecutor.js", function () {
                //SP.SOD.executeFunc('sp.requestexecutor.js', 'SP.RequestExecutor', function () {

                var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;

                var executor = new SP.RequestExecutor(appWebUrl);

                //path to host web
                //var path = ' Path:' + tenantUrl;

                executor.executeAsync({
                    url: appWebUrl + '/_api/search/query?' + "querytext='" + searchString + "'&startrow=" + (page - 1) * 10 + "&selectproperties='Title%2c+Author%2c+HitHighlightedSummary%2c+Path%2c+FileExtension%2c+FileType%2c+Description%2c+PictureThumbnailURL%2c+ServerRedirectedURL%2c+ServerRedirectedEmbedURL'",
                    //url: appWebUrl + "/_api/SP.AppContextSite(@target)/search/query?@target='" + hostUrl + "'&querytext='" + searchstring + "'",
                    method: "GET",
                    headers: { "accept": "application/json;odata=verbose" },
                    success: function (data) {
                        data = JSON.parse(data.body);

                        var lastPage = Math.ceil((data.d.query.PrimaryQueryResult.RelevantResults.TotalRows) / numPerPage);

                        var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;

                        var html = '';

                        if (results.length > 0) {

                            $(results).each(function (index) {

                                var buffer = {};
                                var dataKeys = this.Cells.results;

                                $(dataKeys).each(function (ind) {
                                    buffer[this["Key"]] = this["Value"];
                                });

                                var fullSum = '';
                                var title = '';
                                var fullDesc = '';
                                var desc = '';
                                var summary = '';
                                var author = '';

                                var regex = new RegExp(searchString, 'gi');
                                var sumRegex = new RegExp(/(<c0>)|(<\/c0>)|(<ddd\/>)/);

                                //highlights summary

                                fullSum = buffer["HitHighlightedSummary"].replace(sumRegex, '');

                                if (buffer["Title"]) {
                                    title = buffer["Title"].replace(regex, '<strong>$&</strong>');
                                }

                                if (buffer["Description"]) {
                                    fullDesc = buffer["Description"];
                                }

                                if (fullDesc != '') {
                                    desc = textAbstract(fullDesc, charLimit);
                                }

                                if (fullSum != '') {
                                    summary = textAbstract(fullSum, charLimit);
                                }

                                if (buffer["Author"]) {
                                    author = 'Created by: ' + buffer["Author"];
                                }

                                fullSum = fullSum.replace(regex, '<strong>$&</strong>');
                                summary = summary.replace(regex, '<strong>$&</strong>');
                                fullDesc = fullDesc.replace(regex, '<strong>$&</strong>');
                                desc = desc.replace(regex, '<strong>$&</strong>');

                                html += '<div class="sharepoint-item">'
                                               + '<a href="' + getFilePath(buffer) + '" target="_blank">'
                                               + '<div class="sharepoint-title">'
                                               + '<div class="sharepoint-title-icon '+getFileLogo(buffer['FileType'])+'"></div>'
                                               + title
                                               + '</div>'
                                               + '<div class="sharepoint-desc">'
                                               + desc
                                               + '</div>'
                                               + '<div class="sharepoint-full-desc">'
                                               + fullDesc
                                               + '</div>'
                                               + '<div class="sharepoint-summary">'
                                               + summary
                                               + '</div>'
                                               + '<div class="sharepoint-full-summary">'
                                               + fullSum
                                               + '</div>'
                                               + '<div class="sharepoint-author">'
                                               + author
                                               + '</div>'
                                               + '</a>'
                                               + '</div>';
                            });
                            
                            $('.search-box-container.sharepoint-box .search-box-content').addClass('animated fast fadeOutFull');
                            $('.search-box-container.sharepoint-box .search-box-content').one('animationend', function () {
                                $('.search-box-container.sharepoint-box .search-box-content').removeClass('animated fast fadeOutFull');
                                $('.search-box-container.sharepoint-box .search-box-content').html(html);
                                $('.search-box-container.sharepoint-box .search-box-content').addClass('animated fast fadeInFull');


                                if (lastPage > 1) {
                                    //need to generate paging, there is a next page
                                    if (page == 1) {
                                        generatePaging($('.search-box-container.sharepoint-box .search-box-content'), page, 'first', 'custom-sharepoint');
                                    }
                                        //middle page
                                    else if (page > 1 && page < lastPage) {
                                        generatePaging($('.search-box-container.sharepoint-box .search-box-content'), page, 'middle', 'custom-sharepoint');
                                    }
                                        //last page
                                    else if (page == lastPage) {
                                        generatePaging($('.search-box-container.sharepoint-box .search-box-content'), page, 'last', 'custom-sharepoint');
                                    }
                                }


                                if (heightAdjust) {
                                    //adjust search box height
                                    searchBoxHeightHandler();
                                }
                            });

                        }
                        else {
                            $('.search-box-container.sharepoint-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                            </ul>                        </div>');

                            if (heightAdjust) {
                                //adjust search box height
                                searchBoxHeightHandler();
                            }
                        }

                    },
                    error: function (err) {
                        console.log(err);
                        $('.search-box-container.sharepoint-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                       </ul>                        </div>');
                    }
                });
            });
        }
        else{
            $('.search-box-container.sharepoint-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                              </ul>                        </div>');
        }
    }
    catch (err) {
        console.log("Exception: " + err.message);
        $('.search-box-container.sharepoint-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                     </ul>                        </div>');
    }
}

function getOnedriveSearch(searchString, page, heightAdjust) {
    try {
        if (searchString != undefined && searchString != null && searchString != '') {

            //local search box modal
            $('.search-box-container.onedrive-box .search-box-content').append('<div class="search-modal"></div>');
            var numPerPage = 10;
            var charLimit = 90;
            var tenantUrl = window.location.protocol + '//' + window.location.host.replace(/(-[\w]*)/, '');
            var hostUrl = _spPageContextInfo.siteAbsoluteUrl.replace(/(-[\w]*)/, '');
            var scriptbase = tenantUrl + "/_layouts/15/";
            getScriptCache(scriptbase + "SP.RequestExecutor.js", function () {
                //SP.SOD.executeFunc('sp.requestexecutor.js', 'SP.RequestExecutor', function () {

                var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;

                var executor = new SP.RequestExecutor(appWebUrl);

                //path to host web
                //var path = ' Path:' + tenantUrl;

                executor.executeAsync({
                    url: appWebUrl + '/_api/search/query?' + "querytext='" + searchString + ' Path:' + personalUrl + " IsDocument:true'&startrow=" + (page - 1) * 10,
                    //url: appWebUrl + "/_api/SP.AppContextSite(@target)/search/query?@target='" + hostUrl + "'&querytext='" + searchstring + "'",
                    method: "GET",
                    headers: { "accept": "application/json;odata=verbose" },
                    success: function (data) {
                        data = JSON.parse(data.body);

                        var lastPage = Math.ceil((data.d.query.PrimaryQueryResult.RelevantResults.TotalRows) / numPerPage);

                        var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;

                        var html = '';

                        if (results.length > 0) {

                            $(results).each(function (index) {

                                var buffer = {};
                                var dataKeys = this.Cells.results;

                                $(dataKeys).each(function (ind) {
                                    buffer[this["Key"]] = this["Value"];
                                });

                                var fullSum = '';
                                var title = '';
                                var fullDesc = '';
                                var desc = '';
                                var summary = '';

                                var regex = new RegExp(searchString, 'gi');
                                var sumRegex = new RegExp(/(<c0>)|(<\/c0>)|(<ddd\/>)/);

                                //highlights summary

                                fullSum = buffer["HitHighlightedSummary"].replace(sumRegex, '');


                                if (buffer["Title"]) {
                                    title = buffer["Title"].replace(regex, '<strong>$&</strong>');
                                }

                                if (buffer["Description"]) {
                                    fullDesc = buffer["Description"];
                                }

                                if (fullDesc != '') {
                                    desc = textAbstract(fullDesc, charLimit);
                                }

                                if (fullSum != '') {
                                    summary = textAbstract(fullSum, charLimit);
                                }


                                fullSum = fullSum.replace(regex, '<strong>$&</strong>');
                                summary = summary.replace(regex, '<strong>$&</strong>');
                                fullDesc = fullDesc.replace(regex, '<strong>$&</strong>');
                                desc = desc.replace(regex, '<strong>$&</strong>');

                                html += '<div class="sharepoint-item">'
                                               + '<a href="' + getFilePath(buffer) + '" target="_blank">'
                                               + '<div class="sharepoint-title">'
                                               + '<div class="sharepoint-title-icon ' + getFileLogo(buffer['FileType']) + '"></div>'
                                               + title
                                               + '</div>'
                                               + '<div class="sharepoint-desc">'
                                               + desc
                                               + '</div>'
                                               + '<div class="sharepoint-full-desc">'
                                               + fullDesc
                                               + '</div>'
                                               + '<div class="sharepoint-summary">'
                                               + summary
                                               + '</div>'
                                               + '<div class="sharepoint-full-summary">'
                                               + fullSum
                                               + '</div>'
                                               + '</a>'
                                               + '</div>';
                            });

                            $('.search-box-container.onedrive-box .search-box-content').addClass('animated fast fadeOutFull');
                            $('.search-box-container.onedrive-box .search-box-content').one('animationend', function () {
                                $('.search-box-container.onedrive-box .search-box-content').removeClass('animated fast fadeOutFull');
                                $('.search-box-container.onedrive-box .search-box-content').html(html);
                                $('.search-box-container.onedrive-box .search-box-content').addClass('animated fast fadeInFull');

                                if (lastPage > 1) {
                                    //need to generate paging, there is a next page
                                    if (page == 1) {
                                        generatePaging($('.search-box-container.onedrive-box .search-box-content'), page, 'first', 'custom-onedrive');
                                    }
                                        //middle page
                                    else if (page > 1 && page < lastPage) {
                                        generatePaging($('.search-box-container.onedrive-box .search-box-content'), page, 'middle', 'custom-onedrive');
                                    }
                                        //last page
                                    else if (page == lastPage) {
                                        generatePaging($('.search-box-container.onedrive-box .search-box-content'), page, 'last', 'custom-onedrive');
                                    }
                                }


                                if (heightAdjust) {
                                    //adjust search box height
                                    searchBoxHeightHandler();
                                }
                            });

                        }
                        else {
                            $('.search-box-container.onedrive-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li></ul>                        </div>');

                            if (heightAdjust) {
                                //adjust search box height
                                searchBoxHeightHandler();
                            }
                        }

                    },
                    error: function (err) {
                        console.log(err);
                        $('.search-box-container.onedrive-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                              </ul>                        </div>');
                    }
                });
            });
        }
        else {
            $('.search-box-container.onedrive-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>      </ul>                        </div>');
        }
    }
    catch (err) {
        console.log("Exception: " + err.message);
        $('.search-box-container.onedrive-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>           </ul>                        </div>');
    }
}


/*helper function to generate page
function generatePageQuery(size, page) {
    //return '$top=' + size + '&$skip=' + (page - 1) * size;
    return (page - 1) * size;   //'&$skip=' + 
}
*/

function generatePaging(element,page,position,customClass) {
    if (position == 'first') {
        var html = '<ul id="Paging" class="ms-srch-Paging '+customClass+'">'
	     + '<li id="PagingSelf"><a id="SelfLink">'+page+'</a>'
	     + '</li>'
	     + '<li id="PagingImageLink">'
	     + '<a id="PageLinkNext" href="javascript:;" class="ms-commandLink ms-promlink-button ms-promlink-button-enabled ms-verticalAlignMiddle" title="Move to next page">'
	     + '<span class="page-button">'
	     + '>'
	     + '</a>'
         + '</li>'
	     + '</ul>';
        element.append(html);
    }
    else if (position == 'middle') {
        var html = '<ul id="Paging" class="ms-srch-Paging ' + customClass + '">'
	    + '<li id="PagingImageLink">'
        + '<a id="PageLinkPrev" href="javascript:;" class="ms-commandLink ms-promlink-button ms-promlink-button-enabled ms-verticalAlignMiddle" title="Move to previous page">'
        + '<span class="page-button">'
	    + '<'
	    + '</span>'
        + '</a>'
        + '</li>'
        + '<li id="PagingSelf"><a id="SelfLink">' + page + '</a>'
        + '</li>'
        + '<li id="PagingImageLink">'
        + '<a id="PageLinkNext" href="javascript:;" class="ms-commandLink ms-promlink-button ms-promlink-button-enabled ms-verticalAlignMiddle" title="Move to next page">'
        + '<span class="page-button">'
	    + '>'
	    + '</span>'
        + '</a>'
        + '</li>'
	    + '</ul>';
        element.append(html);
    }
    else if (position == 'last') {
        var html = '<ul id="Paging" class="ms-srch-Paging ' + customClass + '">'
	    + '<li id="PagingImageLink">'
        + '<a id="PageLinkPrev" href="javascript:;" class="ms-commandLink ms-promlink-button ms-promlink-button-enabled ms-verticalAlignMiddle" title="Move to previous page">'
        + '<span class="page-button">'
	    + '<'
	    + '</span>'
        + '</a>'
        + '</li>'
        + '<li id="PagingSelf"><a id="SelfLink">' + page + '</a>'
        + '</li>'
	    + '</ul>';
        element.append(html);
    }

    //replaced qetQueryString() with 


    $('#Paging.' + customClass + ' #PageLinkNext').on('click', function () {
        if (customClass == 'custom-outlook') {
            getMail(userEmail, getHashString(), page + 1);
        }
        else if (customClass == 'custom-yammer') {
            var yammerToken = getCookie("yammer_access_token");
            if (yammerToken) {
                loadYammerSearch(yammerToken, getHashString(), page + 1);
            }
        }
        else if (customClass == 'custom-sharepoint') {
            getSPSearch(getHashString(), page + 1);
        }
        else if (customClass == 'custom-onedrive') {
            getOnedriveSearch(getHashString(), page + 1);
        }
    });

    $('#Paging.' + customClass + ' #PageLinkPrev').on('click', function () {
        if (customClass == 'custom-outlook') {
            getMail(userEmail, getHashString(), page - 1);
        }
        else if (customClass == 'custom-yammer') {
            var yammerToken = getCookie("yammer_access_token");
            if (yammerToken) {
                loadYammerSearch(yammerToken, getHashString(), page - 1);
            }
        }
        else if (customClass == 'custom-sharepoint') {
            getSPSearch(getHashString(), page - 1);
        }
        else if (customClass == 'custom-onedrive') {
            getOnedriveSearch(getHashString(), page - 1);
        }
    });
}


function loadYammerSearch(token,searchString, page, heightAdjust){
    if (searchString != "" && searchString != undefined && searchString != null) {
        try {

         //local search box modal
         $('.search-box-container.yammer-box .search-box-content').append('<div class="search-modal"></div>');

		//var token = getCookie("yammer_access_token");
		yam.platform.setAuthToken(token);
		var charLimit = 107;
		yam.platform.request({
			url: "https://api.yammer.com/api/v1/search.json",
			method: "GET",
			data: {
				search:searchString,
				page: page,
    			num_per_page: 8
			},
			success: function (obj) {
				
				var count = obj.count.messages;
				var lastPage=0;
				if(count>0){
					lastPage = Math.ceil(count/8);
				}
				
				var html = ''
				var msgs = obj.messages.messages;
				//console.log(msgs);
				var ref = obj.messages.references;
				var displayMsg = [];
				for (var m in msgs){
					var displayName = "";
					var thumb = "";
					var profileUrl = "";
					for(var user in ref){
						if(ref[user].id == msgs[m].sender_id){
							displayName = ref[user].full_name;
							thumb = ref[user].mugshot_url;
							profileUrl = ref[user].web_url;
							break;
						}
					}

					    var r = new RegExp('<', 'gi');
					    var s = new RegExp('>', 'gi');
                        
					    var cleanBody = msgs[m].body.plain.replace(r, '&lt;').replace(s, '&gt;');
					    var cleanFullBody = msgs[m].body.plain.replace(r, '&lt;').replace(s, '&gt;');
						
						var regex = RegExp(searchString,'gi');
						if (cleanBody.length >= (charLimit+3)) {
							
							cleanBody = textAbstract(cleanBody,charLimit);
							
						}
						
						cleanBody = cleanBody.replace(regex, '<strong>$&</strong>');

						cleanFullBody = cleanFullBody.replace(regex, '<strong>$&</strong>');
						
						var date = new Date(msgs[m].created_at);
						var createdDateString = getDateString(date,true);
												
						displayMsg.push({
						body:cleanBody,
						fullBody:cleanFullBody,
						senderName:displayName,
						thumb:thumb,
						link:msgs[m].web_url,
						profileUrl:profileUrl,
						createdDate:createdDateString
					});
				}
				if (displayMsg.length>0){
					displayMsg.forEach(function(a){
						html+= '<a class="yammer-search-container" href="'+a.link+'" target="_blank">'
						+ 	'<div class="yammer-search-content">'
						+		'<div class="yammer-search-sender">'
						+	'<img class="yammer-search-thumb-img" src="'+a.thumb+'" />' + a.senderName
						+		'</div>'
						+		'<div class="yammer-search-body">'
						+		a.body
						+		'</div>'
						+		'<div class="yammer-search-full-body">'
						+		a.fullBody
						+		'</div>'
						+		'<div class="yammer-search-created">'
						+		a.createdDate
						+		'</div>'
						+	'</div>'
						+	'</a>';
					});
					
					$('.search-box-container.yammer-box .search-box-content').addClass('animated fast fadeOutFull');
					$('.search-box-container.yammer-box .search-box-content').one('animationend',function(){
						$('.search-box-container.yammer-box .search-box-content').removeClass('animated fast fadeOutFull');
					    $('.search-box-container.yammer-box .search-box-content').html(html);
						// add paging
						if(lastPage > 1){
							if(page==1){
							    generatePaging($('.search-box-container.yammer-box .search-box-content'), page, 'first', 'custom-yammer');
							}
							else if(page==lastPage){
							    generatePaging($('.search-box-container.yammer-box .search-box-content'), page, 'last','custom-yammer');
							}
							else{
							    generatePaging($('.search-box-container.yammer-box .search-box-content'), page, 'middle', 'custom-yammer');
							}
						}

						$('.search-box-container.yammer-box .search-box-content').addClass('animated fast fadeInFull');

						if (heightAdjust) {
						    //adjust search box height
						    searchBoxHeightHandler();
						}
					});
					
				}
				else{
					//$('.search-box-container.yammer-box .search-box-content').css('padding-top','15px');
                    $('.search-box-container.yammer-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                   </ul>                        </div>');

                    if (heightAdjust) {
                        //adjust search box height
                        searchBoxHeightHandler();
                    }

				}

			},
			error: function (msg) {
			    console.log("Error");
			    $('.search-box-container.yammer-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                           </ul>                        </div>');
			}
		});
	} 
	catch (err) 
	{  
	    console.log("Exception: " + err.message);
	    $('.search-box-container.yammer-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                          </ul>                        </div>');
	}

    }
    else
    {
	    $('.search-box-container.yammer-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                                 </ul>                        </div>');
    }

}


/*
function getBusinessCard(token){
	try {
		//$('body').addClass('loading');	//add loading gif
		//$('.onenote-box').closest('.ms-webpart-chrome').append('<div class="modal-search"></div>');
		var endpointUrl = "https://www.onenote.com/api/beta/me/notes/pages?filter=contains(title,'Office Lens')";
				
		var params={};		
				
		$.ajax({
				url:endpointUrl,
				method:'GET',
				async:true,
				//data: JSON.stringify(params),
				contentType:'application/json',
				accepts:'json',
				headers:{
					"Authorization":"Bearer "+token
				},
			beforeSend: function (xhr) {
			    xhr.setRequestHeader("Authorization", "Bearer " + token); 			
			},
			
			success:function(msg){
				console.log(msg);	//msg.value[1].contentUrl
				//console.log(msg.value[1].links.oneNoteWebUrl.href);
				console.log(msg.value.length);
				extractBusinessCard(businessCard, token, msg.value, 0);
				
			},
			error:function(err){
				console.log(err);
				//hard refresh the onenote token
				oauth_onenote_init();
			}
		});
	} 
	catch (err) 
	{  
		console.log("Exception: " + err.message);
	}

}

function extractBusinessCard(array,token,arrayData,index){
	if(index == arrayData.length){
		console.log(businessCard);
		parseBusinessCard(getHashString(),businessCard);
		return;
	}
	else{
			$.ajax({ url: arrayData[index].contentUrl,
				headers:{
					"Authorization":"Bearer "+token
				},
				beforeSend: function (xhr) {
				    xhr.setRequestHeader("Authorization", "Bearer " + token); 			
				},
				success: function(data) {
					console.log(data);
					var dom = $.parseHTML(data); 
					//console.log(dom);
					
					$(dom).each(function(){
						if(this.tagName == 'DIV')
						{	
							var outerHTML = this.outerHTML;
							var data = this.textContent.trim();
							var webLink = arrayData[index].links.oneNoteWebUrl.href;
							businessCard.push({data:data,outerHTML:outerHTML,webLink:webLink});
							return false;
						}
					});
					extractBusinessCard(array, token, arrayData, index+1); 
				},
				error:function(error){
					console.log(error);	
				}
			});
	}
}

function parseBusinessCard(searchString, array){
	//if(searchString != "" && !SP.ScriptHelpers.isNullOrUndefined(searchString) && searchString != undefined && searchString!='undefined'){
	//	$('.search-box-container.onenote-box .search-box-content').html('');
		$(array).each(function(i,v){
			console.log(this);
			
			var text = this.data.split("\n");
			var buffer= [];
			$(text).each(function(index,value){
				text[index] = this.trim();
				//console.log(text[index]);
				if(text[index] != ""){
					buffer.push(text[index]);
				}
			});
			//console.log(buffer);
			
			bufferBusinessCard.push(buffer);
			filterBusinessCard(searchString,bufferBusinessCard);
			
			//parsing
		});
	//}
	//else{
//		$('.search-box-container.onenote-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                    <li>Try these <a href="javascript:HelpWindowKey("WSSEndUser_SearchTips")">tips for searching</a> </li>                                </ul>                        </div>');			
//	}
}

function filterBusinessCard(searchString,array){
console.log(bufferBusinessCard);
	$('.search-box-container.onenote-box .search-box-content').empty();
	$(array).each(function(index,value){
			var name = "";
			if(this[0] != 'PHONE'){
				name = this[0];
			}
			
			var jobTitle = "";
			if(this[1] != 'PHONE'){
				jobTitle = this[1];
			}
			var phone = "";
			var phoneLink = "";
			if(this[this.indexOf('PHONE')+1] != "FAX"){
				phone = this[this.indexOf('PHONE')+1];
				phoneLink = phone.split(' ').join('');
			}
			
			var fax = "";
			var faxLink = "";
			if(this[this.indexOf('FAX')+1] != "ADDRESS"){
				fax = this[this.indexOf('FAX')+1];
				faxLink = fax.split(' ').join('');
			}
			
			var address = "";
			var addressLink = "";
			if(this[this.indexOf('ADDRESS')+1] != "EMAIL"){
				address = this[this.indexOf('ADDRESS')+1];
				addressLink = "http://www.bing.com/maps/?where1="+encodeURIComponent(address);
			}
			
			if(	this[this.indexOf('ADDRESS')+2]!="EMAIL" && (this.indexOf('EMAIL')>(this.indexOf('ADDRESS')+2))){
				address += this[this.indexOf('ADDRESS')+2];
				addressLink = "http://www.bing.com/maps/?where1="+encodeURIComponent(address);
			}
			
			var email = "";
			if(this[this.indexOf('EMAIL')+1] != "AFFILIATION"){
				email = this[this.indexOf('EMAIL')+1];
			}
			
			var affil = "";
			if(this[this.indexOf('AFFILIATION')+1] != "Did we extract the business card right? Yes No Suggestion"){
				affil = this[this.indexOf('AFFILIATION')+1];
			}
			
			
			
			var html="";
			console.log(this.join(' ').toLowerCase().indexOf(searchString.toLowerCase()));
			if(this.join(' ').toLowerCase().indexOf(searchString.toLowerCase())>-1){
				html+='<div class="business-search-item show">';
				html+='<a class="name business-item" href="'+ businessCard[index].webLink +'" target="_blank">'
					+ '<img class="name icon" src="/sites/ProductivityDemo/SiteAssets/ContactIcon.png" />'+name
					+'</a>'
					+'<div class="job-title business-item">'
					+ '<img class="jon-title icon" src="/sites/ProductivityDemo/SiteAssets/jobtitleIcon.png" />'+ jobTitle
					+'</div>'
					+'<div class="affiliation business-item">'
					+ '<img class="affilitation icon small" src="/sites/ProductivityDemo/SiteAssets/AffilicationIcon.png" />'+ affil
					+'</div>'
					+'<a class="phone business-item" href="tel:'+phoneLink+'">'
					+ '<img class="phone icon small" src="/sites/ProductivityDemo/SiteAssets/PhoneIcon.png" />'+ phone
					+'</a>'
					+'<a class="email business-item" href="mailto:'+email+'">'
					+ '<img class="email icon small" src="/sites/ProductivityDemo/SiteAssets/MailIcon.png" />'+email
					+'</a>'
					+'<a class="address business-item" href='+addressLink+'" target="_blank">'
					+ '<img class="address icon small" src="/sites/ProductivityDemo/SiteAssets/locationIcon.png" />'+address
					+'</a>'
					+'</div>';
					
			$('.search-box-container.onenote-box .search-box-content').append(html);
			}
			else{
				$('.search-box-container.onenote-box .search-box-content').html('<div id="NoResult">                                <div class="ms-textLarge ms-srch-result-noResultsTitle">Nothing here matches your search</div>                                <div>Suggestions</div>                                <ul>                                    <li>Make sure all words are spelled correctly</li>                                    <li>Try different search terms</li>                                    <li>Try more general search terms</li>                                    <li>Try fewer search terms</li>                                    <li>Try these <a href="javascript:HelpWindowKey("WSSEndUser_SearchTips")">tips for searching</a> </li>                                </ul>                        </div>');
			}
			
			//$('.onenote-box').closest('.ms-webpart-chrome').find('.modal-search').remove();
	});
}
*/


//_spBodyOnLoadFunctionNames.push("loadConfigSearch");