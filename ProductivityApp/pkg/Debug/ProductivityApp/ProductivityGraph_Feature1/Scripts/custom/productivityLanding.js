﻿/* Javascript for Landing page dynamic contents */
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

//global variable to store last refresh date time
//initialize on page load
var refreshTimeTATM = getUtcISOString();
var refreshTimeTABM = getUtcISOString();

//StatusCode 0 --> success, 1 --> error


$(document).ready(function () {

    //Add Modernizr test
    Modernizr.addTest('isios', function () {
        return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false
    });

    $('#selectnews').on('click', function () {
        setTimeout(function () {
            //popUpHTML
            var passedObj = { data: rssFeedUrl, type:'select' };
            loadPopUp(generateNewsPopUpHTML, newsPopUpButtonHandler, passedObj);
        }, 0);
    });

    $('#rssconfig').on('click', function () {
        setTimeout(function () {
            //popUpHTML
            var passedObj = { data: rssFeedUrl, type: 'config' };
            loadPopUp(generateNewsPopUpHTML, newsPopUpButtonHandler, passedObj);
            $('.mfp-content').css('width', '80%');
        }, 0);
    });

    //assign refresh handler
    $('#myCalendarHeading').on('click', function () {
        $('#myCalendarDetails').html('<div class="panel-loading"></div>');
        loadScheduleFeed(userEmail, true);
    });

    $('#myFlaggedEmailsHeading').on('click', function () {
        $('#myFlaggedEmailsDetails').html('<div class="panel-loading"></div>');
        loadFlaggedMessages(userEmail, []);
    });

    /*
    $('#myTasksHeading').on('click', function () {
        $('#myTasksDetails').html('<div class="panel-loading"></div>');
        loadTATMFeed(userEmail, true);
    });

    $('#assigneeTasksHeading').on('click', function () {
        $('#assigneeTasksDetails').html('<div class="panel-loading"></div>');
        loadTABMFeed(userEmail, true);
    });
    */

    $('#myYammerHeading').on('click', function () {
        $('#myYammerDetails').html('<div class="panel-loading"></div>');
        var yammerToken = getCookie('yammer_access_token');
        if (yammerToken) {
            loadYammerMessages(yammerToken, true);
        }
    });

    /*
    $('#myFollowedSitesHeading').on('click', function () {
        $('#myFollowedSitesDetails').html('<div class="panel-loading"></div>');
        loadFollowedSites();
    });
    */

    $('#myRecentDocsHeading').on('click', function () {
        $('#myRecentDocsDetails').html('<div class="panel-loading"></div>');
        loadRecentDocs();
    });

    $('#myRSSHeading').on('click', function () {
        $('#myRSSDetails').html('<div class="panel-loading"></div>');
        loadRSSFeed(userFeedSelection);
    });

});

function productivityInit() {

    applyPanelSettings({
        panelSelector: '#myYammerPanel',
        settings: webPartConfig.showYammerPanel,
        callback: function () {
            var yammerToken = getCookie("yammer_access_token");
            if (yammerToken) {
                loadYammerMessages(yammerToken);
            }
                //open yammer login popup
            else {
                //hide yammer panel
                $('#myYammerPanel').attr('style', 'max-height:0px;margin:0px!important;');
            }
        },
        falseCallback: function () {
            $('#yamButtonContainer').attr('style', 'display:none');
            $('#myYammerPanel').css('display', 'none');
        }
    });

    console.log(webPartConfig);

    //loadProductivityRate(userEmail);

    /*
    applyPanelSettings({
        panelSelector: '#myTasksPanel',
        settings: webPartConfig.showMyTaskPanel,
        callback: function () {
            loadTATMFeed(userEmail);
        }
    });

    applyPanelSettings({
        panelSelector: '#assigneeTasksPanel',
        settings: webPartConfig.showTheirTaskPanel,
        callback: function () {
            loadTABMFeed(userEmail);
        }
    });
    */

    applyPanelSettings({
        panelSelector: '#myCalendarPanel',
        settings: webPartConfig.showSchedulePanel,
        callback: function () {
            loadScheduleFeed(userEmail);
        }
    });

    applyPanelSettings({
        panelSelector: '#myRSSPanel',
        settings: webPartConfig.showNewsPanel,
        callback: function () {
            $('#selectnews').css('display', 'block');
            loadRSSFeed(userFeedSelection);
        }
    });

    
    applyPanelSettings({
        panelSelector: '#myFlaggedEmailsPanel',
        settings: webPartConfig.showFlaggedEmailPanel,
        callback: function () {
            loadFlaggedMessages(userEmail, []);
        }
    });
    

    /*
    applyPanelSettings({
        panelSelector: '#myFollowedSitesPanel',
        settings: webPartConfig.showFollowedSitePanel,
        callback: function () {
            loadFollowedSites();
        }
    });
    */

    applyPanelSettings({
        panelSelector: '#myRecentDocsPanel',
        settings: webPartConfig.showRecentDocPanel,
        callback: function () {
            loadRecentDocs();
        }
    });

    $('#sp13custom-bodyContainer').show();
        
        /*
        var timerCheck = setInterval(function () {
            applyPanelSettings({
                panelSelector: '#myTasksPanel',
                settings: webPartConfig.showMyTaskPanel,
                callback: function () {
                    checkTATMFeed(userEmail, refreshTimeTATM);
                }
            });

            applyPanelSettings({
                panelSelector: '#assigneeTasksPanel',
                settings: webPartConfig.showTheirTaskPanel,
                callback: function () {
                    checkTABMFeed(userEmail, refreshTimeTABM);
                }
            });
        }, 10000);
        */

        //loadOtherPanel();


        globalOverlayHandler();

        //searchResultHandler();

        postHeight();
        checkTotalHeight();
}

//panel helper
function refreshPanel(id, callback, disableEffect) {
    if (!id instanceof jQuery) {
        id = $(id);
    }

    if (disableEffect) {

        if (id.hasClass('slick-slider')) {
            id.slick('unslick');
        }

        id.html('');

        if (typeof callback === 'function') {
            callback();
        }
    }
    else {
        id.removeClass('animated custom fadeInFull');
        id.addClass('animated custom fadeOutFull');

        //add fade in animation
        id.one('animationend', function () {

            if (id.hasClass('slick-slider')) {
                id.slick('unslick');
            }

            id.removeClass('animated custom fadeOutFull');
            id.html('');
            id.addClass('animated custom fadeInFull');

            if (typeof callback === 'function') {
                callback();
            }

        });
    }
}

//assign opentip handler
function hoverTooltip() {
    $('#myCalendarHeading').on('hover', function () {
        var myOpentip = new Opentip("#myCalendarHeading", "Schedule panel will show all your upcoming schedules in the following 7 days. Click on the heading to refresh the panel.");
    });
}

function checkTotalHeight() {
    var windowWidthChecker = $(window).width();
    $(window).on('resize', function () {
        //only post message on width change
        if ($(this).width() != windowWidthChecker) {

            postHeight();

            windowWidthChecker = $(this).width();
        }
    });
}

/*add task function helper*/
function addAddTaskHandler() {
    $('#addTask').on('click', function () {
        setTimeout(function () {
            //popUpHTML
            loadPopUp(generateTaskPopUpHTML, initAddTaskPopUp, { type: "addTask" });
        }, 0);
    });
}

function initAddTaskPopUp() {
    //set sender
    $('.popup-content-body.assignedBy').html(currentUserName);

    //email check
    /*
    $('.popup-content-body.assignedToEmail > input').on('change', function () {
        ensureUserByEmail($(this).val(), function (a) {
            $('.popup-content-body.assignedToName > input').val(a);
            $('.popup-content-body.assignedToEmail .error-message').remove();

        }, function () {
            popUpAddError('assignedToEmail', 'User Email cannot be found. Please input a valid user email.');
        });
    });
    */

    //task name
    $('.popup-content-body.taskName > input').on('change', function () {
        if ($(this).val().trim() == "") {
            popUpAddError('taskName', 'Please input a task name.');
        }
        else {
            popUpClearError('taskName');
        }
    });

    //add auto complete
    addAutoComplete()

    //datepicker
    var selector = $('.popup-content-body.dueDate > input.datepicker').datepicker({
        minDate: new Date(),
        dateFormat: 'yy/mm/dd',
        onSelect: function (selectedDate, inst) {
            var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            selector.not(this).datepicker("option", option, date);
        }
    });

    //add assignee click handler
    $('.popup-content-body.assignedToName .addAssignee').on('click', function () {
        var newRow = $('<tr class="assignedToNameRow"><td class="popup-content-header"></td>'
          + '<td class="popup-content-body assignedToName ui-front"><input type="text" />'
          + '<div class="removeAssignee rowButton">-</div>'
          + '</td>'
          + '</tr>');

        $('.assignedToNameRow').last().after(newRow);

        //add autocomplete
        addAutoComplete();

        //add remove assignee handler
        newRow.find('.removeAssignee').on('click', function (event) {
            event.stopPropagation();
            $(this).closest('.assignedToNameRow').remove();
        });
    });


    //reset task form
    $('#addTaskPopUp .reset').on('click', function () {
        loadPopUp(generateTaskPopUpHTML, initAddTaskPopUp, { type: "addTask" }, true);
    });

    //cancel button
    $('.popup-button.cancel').on('click', function () {
        $.magnificPopup.close();
    });

    //submit task form
    $('#addTaskPopUp .submit').on('click', function () {
        if (!($(this).hasClass('disabled'))) {
            //parse multiple assigned to
            var parsedVal = parseAssignedTo(' - ');
            
            if (parsedVal.length > 0) {

                var status = true;
                var toRefresh = [];
                var asyncChecker = [];

                $(parsedVal).each(function () {
                    //validate
                    if (validateAddTaskPopUp(this.name)) {

                        var ddString = $('.popup-content.addTask .dueDate > input').val();
                        var dueDate = "";
                        if (ddString) {
                            dueDate = getUtcISOString(new Date(ddString));
                        }

                        var param = {
                            assignedBy: currentUserName,
                            assignedByEmailAddress: userEmail,
                            assignedTo: this.name,
                            assignedToEmailAddress: this.email,
                            taskName: $('.popup-content.addTask .taskName > input').val(),
                            description: $('.popup-content.addTask .taskDesc > textarea').val(),
                            originalDueDate: dueDate,
                            createdChannelID: 2,
                            tenantID: tenantId
                        };

                        if (this.email == userEmail) {
                            if (toRefresh.indexOf('tatm') == -1) {
                                toRefresh.push('tatm');
                            }
                        }
                        else {
                            if (toRefresh.indexOf('tabm') == -1) {
                                toRefresh.push('tabm');
                            }
                        }

                        createTask(param, toRefresh, asyncChecker, parsedVal.length);
                    }
                    else {
                        status = false;
                        return false;
                    }
                });

                if (status) {
                    $.magnificPopup.close();
                }
            }
            else {
                popUpAddError($('.popup-content.addTask .assignedToName').first(), 'User cannot be found. Please input a valid user.');
            }
        }
    });
}

function addAutoComplete() {
    //autocomplete on name, skip input field that already has auto complete
    $('.popup-content-body.assignedToName > input:not(.ui-autocomplete-input)').autocomplete({
        minLength: 1,
        source: siteUserList,
        open: function (event, ui) {
            //ios workaround, disable ios focus
            if (Modernizr.isios) {
                $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
            }
        },
        select: function (event, ui) {
            popUpClearError($(this).parent('.assignedToName'));
            
            //set name and email
            $(this).val(ui.item.label + ' - ' + ui.item.EMail);

            return false;
        },
        change: function (event, ui) {
            //check null value
            if ($(this).val().trim() == "") {
                popUpAddError($(this).parent('.assignedToName'), 'Please input a valid user name.');
                //addUserEmail($(this).parent('.assignedToName'), '');
            }
            else {
                popUpClearError($(this).parent('.assignedToName'));
                //user does not select from the suggestion
                if (!ui.item) {
                    var currentVal = $(this).val().trim();
                    var check = checkUserName(siteUserList, currentVal);
                    if (check) {

                        //set name and email --> name - email
                        $(this).val(check.label + ' - ' + check.EMail);
                    }
                    else {
                        //reset email user not found
                        popUpAddError($(this).parent('.assignedToName'), 'User cannot be found. Please input a valid user.');
                    }
                }
            }
        },
        messages: {
            noResults: '',
            results: function () { }
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        var re = new RegExp($.trim(this.term.toLowerCase()), 'gi');
        var t = item.label;
        if (item.label) {
            t = item.label.replace(re, "<span style='font-weight:600;color:#444;'>$&</span>");
            return $('<li>' + t + '</li>').appendTo(ul);
        }
    };
}

function checkUserName(arr, val) {
    var result = arr.filter(function (obj) {
        if (obj.label) {
            return obj.label.toLowerCase() === val.toLowerCase();
        }
    });

    if (result.length > 0) {
        return result[0];
    }
}

function addUserEmail(el, email) {
    if (el.children('.assignedToEmail').length > 0) {
        el.children('.assignedToEmail').html(email);
    }
    else {
        el.append('<span class="assignedToEmail">' + email + '</span>');
    }
}

function parseAssignedTo(separator) {
    //input in array first to check dupes
    var assignedToBuffer = [];
    //var nameBuffer = [];
    var emailBuffer = [];

    $('.popup-content.addTask .assignedToName').each(function (index) {
        if (!!($(this).find('input').val().trim()) && !!($(this).find('.error-message').length == 0)) {
            
            var value = $(this).find('input').val();

            var name = value.split(separator)[0].trim();
            //var email = $(this).find('.assignedToEmail').html();
            var email = value.split(separator)[1].trim();

            //name allow dupes, check email
            if (!(emailBuffer.indexOf(email) > -1)) {
                //nameBuffer.push(name);
                emailBuffer.push(email);
                assignedToBuffer.push({ name: name, email: email });
            }
        }
    });

    return assignedToBuffer;
}

function validateAddTaskPopUp(parsedName) {
    
    var nameError = (parsedName == '') || ($('.popup-content.addTask .assignedToName .error-message').length > 0);
    var taskNameError = $('.popup-content.addTask .taskName > input').val().trim() == "";
    

    if (parsedName == '') {
        popUpAddError($('.popup-content.addTask .assignedToName').first(), 'User cannot be found. Please input a valid user.');
    }


    if (taskNameError) {
        popUpAddError('taskName', 'Please input a task name.');
    }
    else {
        popUpClearError('taskName');
    }

    if (nameError || taskNameError) {
        return false;
    }

    return true;
}

function popUpAddError(cls, msg) {
    //string input
    if (typeof cls === 'string') {
        if ($('.popup-content-body.' + cls + ' .error-message').length == 0) {
            $('.popup-content-body.' + cls).append('<div class="error-message">' + msg + '</div>');
        }
        else {
            $('.popup-content-body.' + cls + ' .error-message').html(msg);
        }
    }
        //element input
    else if (typeof cls === 'object') {
        //handler for non jquery element
        if (!(cls instanceof jQuery)) {
            cls = $(cls);
        }
        if (cls.find('.error-message').length == 0) {
            cls.append('<div class="error-message">' + msg + '</div>');
        }
        else {
            cls.find('.error-message').html(msg);
        }
    }

    //disable submit button
    if ($('.popup-button.submit').length > 0) {
        $('.popup-button.submit').addClass('disabled');
    }

    //or save button
    if ($('.popup-button.save').length > 0) {
        $('.popup-button.save').addClass('disabled');
    }
}

function popUpClearError(cls) {
    if (typeof cls === 'string') {
        if ($('.popup-content-body.' + cls + ' .error-message').length > 0) {
            $('.popup-content-body.' + cls + ' .error-message').remove();
        }
    }
    else if (typeof cls === 'object') {
        if (!(cls instanceof jQuery)) {
            cls = $(cls);
        }
        if (cls.find('.error-message').length > 0) {
            cls.find('.error-message').remove();
        }
    }
    //enable submit button
    //if ($('.popup-content.addTask .error-message').length == 0) {
    if ($('.popup-content .error-message').length == 0) {
        if ($('.popup-button.submit').length > 0) {
            $('.popup-button.submit').removeClass('disabled');
        }

        if ($('.popup-button.save').length > 0) {
            $('.popup-button.save').removeClass('disabled');
        }
    }
}
/**/

//20161012 backup
//recent docs panel
/*
function loadRecentDocs() {
    try {
        var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;
        var endpointUrl = appWebUrl + '/_api/search/query?' + "querytext='Path:" + personalUrl + " IsDocument:true'&selectproperties='ModifiedBy%2cTitle%2cLastModifiedTime%2cFileType%2cPath%2cServerRedirectedURL%2cServerRedirectedEmbedURL'&rowlimit=9&sortlist='LastModifiedTime:descending'";

        var titleLimit = 47;
        var bodyLimit = 60;

        var day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $.ajax({
            url: endpointUrl,
            method: 'GET',
            dataType: 'json',
            async: true,
            success: function (json) {
                var results = json.PrimaryQueryResult.RelevantResults.Table.Rows;
                if (results.length > 0) {
                    //reset
                    var html = "";
                    $("#myRecentDocsDetails").removeClass('animated custom fadeInFull');
                    $("#myRecentDocsDetails").addClass('animated custom fadeOutFull');

                    $('#myRecentDocsDetails').one('animationend', function () {
                        if ($('#myRecentDocsDetails').hasClass('slick-slider')) {
                            $('#myRecentDocsDetails').slick('unslick');
                        }

                        $('#myRecentDocsDetails').removeClass('animated custom fadeOutFull');
                        $('#myRecentDocsDetails').html('');
                        $('#myRecentDocsDetails').addClass('animated custom fadeInFull');

                        $(results).each(function (index) {
                            var buffer = {};
                            var dataKeys = this.Cells;
                            $(dataKeys).each(function (ind) {
                                buffer[this["Key"]] = this["Value"];
                            });

                            var modifiedBy = buffer["ModifiedBy"].split('\n');

                            //remove empty
                            for (var i = 0; i < modifiedBy.length; i++) {
                                if (modifiedBy[i] == '') {
                                    modifiedBy.splice(i, 1);
                                }
                            }

                            var title = textAbstract(buffer["Title"], titleLimit).escape();

                            var modifiedDate = new Date(buffer["LastModifiedTime"]);

                            var modified = "Modified on " + day[modifiedDate.getDay()] + ', ' + getDateString(modifiedDate, true) + " by " + textAbstract(modifiedBy.join(', '),bodyLimit);

                            html += '<div class="document-item">'
                                    + '<a href="' + getFilePath(buffer) + '" target="_blank">'
                                    //+ '<a href="' + buffer["Path"] + '" target="_blank">'
                                    + '<div class="document-thumb">'
                                    + getFileLogoHTML(buffer["FileType"])
                                    + '</div>'
                                    + '<div class="document-content">'
                                    + '<div class="document-title">'
                                    + title
                                    + '</div>'
                                    + '<div class="document-modified">'
                                    + modified
                                    + '</div>'
                                    + '</div>'
                                    + '</a>'
                                    + '</div>';

                        });

                        $('#myRecentDocsDetails').html(html);

                        //slick init
                        $("#myRecentDocsDetails").slick({
                            dots: true,
                            arrows: false,
                            infinite: false,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            responsive: [{
                                breakpoint: 1023,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2
                                }
                            }, {
                                breakpoint: 769,
                                settings: {
                                    dots: false,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            }]
                        });
                    });
                }
                else {
                    $('#myRecentDocsDetails').html('<div class="empty-msg">You do not have any recent documents.</div>');
                }

            },
            error: function (err) {
                console.log('error: ' + err.message);
                $('#myRecentDocsDetails').html('<div class="empty-msg">Data request failed.</div>');
            }
        });
    }
    catch (err) {
        $('#myRecentDocsDetails').html('<div class="empty-msg">Data request failed.</div>');
        console.log(err.message);
    }
}
*/

function loadRecentDocs() {
    try {
        var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;
        var endpointUrl = appWebUrl + '/_api/search/query?' + "querytext='Path:" + personalUrl + " IsDocument:true'&selectproperties='ModifiedBy%2cTitle%2cLastModifiedTime%2cFileType%2cPath%2cServerRedirectedURL%2cServerRedirectedEmbedURL'&rowlimit=9&sortlist='LastModifiedTime:descending'";

        var titleLimit = 47;
        var bodyLimit = 60;

        var day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $.ajax({
            url: endpointUrl,
            method: 'GET',
            dataType: 'json',
            async: true,
            success: function (json) {
                var results = json.PrimaryQueryResult.RelevantResults.Table.Rows;
                if (results.length > 0) {
                    //reset
                    var html = "";
                    $("#myRecentDocsDetails").removeClass('animated custom fadeInFull');
                    $("#myRecentDocsDetails").addClass('animated custom fadeOutFull');

                    $('#myRecentDocsDetails').one('animationend', function () {
                        if ($('#myRecentDocsDetails').hasClass('slick-slider')) {
                            $('#myRecentDocsDetails').slick('unslick');
                        }

                        $('#myRecentDocsDetails').removeClass('animated custom fadeOutFull');
                        $('#myRecentDocsDetails').html('');
                        $('#myRecentDocsDetails').addClass('animated custom fadeInFull');

                        $(results).each(function (index) {
                            var buffer = {};
                            var dataKeys = this.Cells;
                            $(dataKeys).each(function (ind) {
                                buffer[this["Key"]] = this["Value"];
                            });

                            var modifiedBy = buffer["ModifiedBy"].split('\n');

                            //remove empty
                            for (var i = 0; i < modifiedBy.length; i++) {
                                if (modifiedBy[i] == '') {
                                    modifiedBy.splice(i, 1);
                                }
                            }

                            var title = textAbstract(buffer["Title"], titleLimit).escape();

                            var modifiedDate = new Date(buffer["LastModifiedTime"]);

                            var modified = "Modified on " + day[modifiedDate.getDay()] + ', ' + getDateString(modifiedDate, true) + " by " + textAbstract(modifiedBy.join(', '), bodyLimit);

                            html += '<div class="document-item">'
                                    + '<a href="' + getFilePath(buffer) + '" target="_blank">'
                                    //+ '<a href="' + buffer["Path"] + '" target="_blank">'
                                    + '<div class="document-thumb">'
                                    + getFileLogoHTML(buffer["FileType"])
                                    + '</div>'
                                    + '<div class="document-content">'
                                    + '<div class="document-title">'
                                    + title
                                    + '</div>'
                                    + '<div class="document-modified">'
                                    + modified
                                    + '</div>'
                                    + '</div>'
                                    + '</a>'
                                    + '</div>';

                        });

                        $('#myRecentDocsDetails').html(html);

                        //slick init
                        $("#myRecentDocsDetails").slick({
                            dots: true,
                            arrows: false,
                            infinite: false,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            responsive: [{
                                breakpoint: 1023,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2
                                }
                            }, {
                                breakpoint: 769,
                                settings: {
                                    dots: false,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            }]
                        });
                    });
                }
                else {
                    $('#myRecentDocsDetails').html('<div class="empty-msg">You do not have any recent documents.</div>');
                }

            },
            error: function (err) {
                console.log('error: ' + err.message);
                $('#myRecentDocsDetails').html('<div class="empty-msg">Data request failed.</div>');
            }
        });
    }
    catch (err) {
        $('#myRecentDocsDetails').html('<div class="empty-msg">Data request failed.</div>');
        console.log(err.message);
    }
}

//flagged messages panel
function loadFlaggedMessages(email, arrBuffer) {
    try {

        //check connection settings
        if(checkConnectionSettings('outlook')){


            var accessToken = getAccessToken();

            //get top 10 latest flagged email
            var endpointUrl = graphEndpointBeta + "me/messages?$top=10&$filter=flag/flagStatus eq 'flagged'";


            /*
            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                },
                success: function (json) {

                    var nextLink = json['@odata.nextLink'];
                    var data = json.value;

                    if (data.length > 0) {
                        //grab the flagged messages and add to array buffer
                        var filterFlagged = data.filter(function (a) { return a.flag.flagStatus == 'flagged'; });
                        filterFlagged.forEach(function (mail) {
                            arrBuffer.push(mail);
                        });


                        //go to next link to process
                        if (nextLink) {
                            loadFlaggedMessages(email, arrBuffer, nextLink);
                        }
                        else {
                            refreshPanel($("#myFlaggedEmailsDetails"), function () {
                                if (arrBuffer.length > 0) {
                                    loadFlaggedEmailData(arrBuffer);
                                }
                                else {
                                    $('#myFlaggedEmailsDetails').html('<div class="empty-msg">You do not have any flagged mail.</div>');
                                }
                            });
                        }

                    }
                    else {
                        $('#myFlaggedEmailsDetails').html('<div class="empty-msg">You do not have any flagged mail.</div>');
                    }

                },
                error: function (err) {
                    console.log('error: ' + err.statusText);
                    $('#myFlaggedEmailsDetails').html('<div class="empty-msg">Data request failed.</div>');
                }
            });
            */

            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                },
                success: function (json) {

                    var data = json.value;

                    if (data.length > 0) {

                            refreshPanel($("#myFlaggedEmailsDetails"), function () {
                                loadFlaggedEmailData(data);
                            });
                    }
                    else {
                        $('#myFlaggedEmailsDetails').html('<div class="empty-msg">You do not have any flagged mail.</div>');
                    }

                },
                error: function (err) {
                    console.log('error: ' + err.statusText);
                    $('#myFlaggedEmailsDetails').html('<div class="empty-msg">Data request failed.</div>');
                }
            });
        }
        //load sample data + links
        else {
            
            refreshPanel($("#myFlaggedEmailsDetails"), function () {
                noCredentialFlaggedEmail();
            });
            
        }
    }
    catch (err) {
        //no credentials
        if (err.message == 'EG0004') {
            $('#myFlaggedEmailsDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
        }
        else {
            $('#myFlaggedEmailsDetails').html('<div class="empty-msg">Data request failed.</div>');
        }
        console.log("error: " + err.message);
    }
}

function noCredentialResize() {
    $('window').on('resize', function () {
        $('.panel-modal.firstload').css('z-index', 1000)
    });
}

function loadFlaggedEmailData(data){
    //var maxFlagged = 10;
    var numFlagged = 0;
    var html = "";
    //var titleLimit = 47
    //var charLimit = 55;

    $(data).each(function (index) {
        if (this.flag.flagStatus == 'flagged') {
            //var tempBody = textAbstract(this.Body, charLimit).escape();
            //var tempSubject = textAbstract(this.Subject, titleLimit).escape();

            var tempBody = this.bodyPreview.escape();
            var tempSubject = this.subject.escape();

            html += '<div class="message-item">'
                    + '<a href="' + this.webLink + '" target="_blank">'
                    + '<div class="message-thumb">'
                    + getFileLogoHTML('outlook')
                    + '</div>'
                    + '<div class="message-content">'
                    + '<div class="message-subject">'
                    + tempSubject
                    + '</div>'
                    + '<div class="message-sender">'
                    + 'Sent by ' + this.sender.emailAddress.name
                    + '</div>'
                    + '<div class="message-bodypreview">'
                    + tempBody
                    + '</div>'
                    + '</div>'
                    + '</a>'
                    + '</div>';
        }
    });

    $('#myFlaggedEmailsDetails').html(html);

    //slick init
    $("#myFlaggedEmailsDetails").slick({
        slide: '.message-item',
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 769,
            settings: {
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });

    //truncate dotddotdot
    $(".message-subject, .message-sender, .message-bodypreview").dotdotdot({
        watch: "window"
    });
}

function noCredentialFlaggedEmail() {

    /*
    var sampleData = [
    {
        Received: "2016-05-18T03:04:06Z",
        MailId: "abcde",
        Subject: "Mail Subject",
        SenderName: "John",
        SenderEmail: "john@celgo.net",
        Body: "This is a sample message body",
        ToRecipients: {
            "bob@celgo.net": "Bob Ross"
        },
        CcRecipients: {
            "jack@celgo.net": "Jack Doe",
            "jane@celgo.net": "Jane Doe"
        },
        WebLink: "",
        Flagged: true
    },
    {
        Received: "2016-05-18T03:04:06Z",
        MailId: "abcde",
        Subject: "Mail Subject",
        SenderName: "John",
        SenderEmail: "john@celgo.net",
        Body: "This is a sample message body",
        ToRecipients: {
            "bob@celgo.net": "Bob Ross"
        },
        CcRecipients: {
            "jack@celgo.net": "Jack Doe",
            "jane@celgo.net": "Jane Doe"
        },
        WebLink: "",
        Flagged: true
    },
    {
        Received: "2016-05-18T03:04:06Z",
        MailId: "abcde",
        Subject: "Mail Subject",
        SenderName: "John",
        SenderEmail: "john@celgo.net",
        Body: "This is a sample message body",
        ToRecipients: {
            "bob@celgo.net": "Bob Ross"
        },
        CcRecipients: {
            "jack@celgo.net": "Jack Doe",
            "jane@celgo.net": "Jane Doe"
        },
        WebLink: "",
        Flagged: true
    }];


    loadFlaggedEmailData(sampleData);

    $("#myFlaggedEmailsDetails .slick-track").css('opacity', '0.7');
    */

    //add overlay with 2 buttons
    $("#myFlaggedEmailsDetails").append("<div class='panel-modal-firstload'></div>");
    $("#myFlaggedEmailsDetails").append("<div class='panel-button-container'><div class='configure panel-button popup-button'>Activate</div><div class='minimize panel-button popup-button'>Minimize</div></div>");

    //set button handler

    $("#myFlaggedEmailsDetails .configure.panel-button").on('click', function () {
        loadDetailedConfig('outlook');
    });

    function maximizePanel(){
        $('#myFlaggedEmailsPanel').removeClass('minimized').animate({ height: 130 }, 400);
        $('#myFlaggedEmailsPanel').off('click', maximizePanel);

        $('#myFlaggedEmailsHeading').on('click', function () {
            $('#myFlaggedEmailsDetails').html('<div class="panel-loading"></div>');
            loadFlaggedMessages(userEmail, []);
        });
    }

    $("#myFlaggedEmailsDetails .minimize.panel-button").on('click', function () {
        var panel = $(this).closest('.panel');

        //remove click handler
        $('#myFlaggedEmailsHeading').off('click');

        panel.animate({ height: 30 }, 400, 'swing', function () {
            panel.addClass('minimized');
            panel.on('click', maximizePanel);
        });
    });
}



function refreshYammer() {
    $('#s4-workspace').addClass('animated fadeOut');
    $('#s4-workspace').one('animationend', function () {
        $('#s4-workspace').removeClass('animated fadeOut');
        $('#yamButtonContainer').attr('style', null);
        $('#myYammerPanel').attr('style', null);
        var yammerToken = getCookie("yammer_access_token");
        if (yammerToken) {
            loadYammerMessages(yammerToken);
        }
       
        $('#s4-workspace').addClass('animated fadeInFull');
        $('#s4-workspace').one('animationend', function () {
            postHeight();
        });

    });
}


function searchResultHandler() {
    $('#searchInputButton').on('click', function () {
        var searchString = $('#searchInputArea').val().trim();

        if (searchString != '') {
            window.location.href = "SearchResult.aspx?appPage=" + webPartConfig.appPage + "#k=" + searchString;
        }
    });

    $('#searchInputArea').keypress(function(e){
            //enter pressed
            if (event.keyCode == 13) {
                $('#searchInputButton').trigger('click');
            }
        });
}


function loadProductivityRate(userEmail) {

    var endpointUrl = endpoint + "/api/tasks/today/rate?userEmail=" + userEmail + "&tenantId=" + tenantId;

    $.ajax({
        url: endpointUrl,
        method: 'GET',
        async: true,
        success: function (xhr) {

            if (xhr instanceof Array) {
                if (xhr[0] != -1) {
                    var taskCount = parseInt(xhr[0]);
                    var outTask = parseInt(xhr[1]);

                    var hour = new Date().getHours();
                    var freeHour = 0;

                    if (hour < 9) {
                        hour = 9;
                    }

                    if (hour >= 13) {
                        freeHour = 18 - hour;
                    } else {
                        freeHour = 18 - hour - 1;
                    }

                    if (freeHour < 0) {
                        freeHour = 0;
                    }

                    var freeHourHTML = '';
                    if (freeHour <= 8) {
                        freeHourHTML = '<div class="free-hour-value full">' + freeHour + '</div>';
                    }
                    if (freeHour <= 6) {
                        freeHourHTML = '<div class="free-hour-value _75">' + freeHour + '</div>';
                    }
                    if (freeHour <= 4) {
                        freeHourHTML = '<div class="free-hour-value _50">' + freeHour + '</div>';
                    }
                    if (freeHour <= 2) {
                        freeHourHTML = '<div class="free-hour-value _25">' + freeHour + '</div>';
                    }
                    if (freeHour == 0) {
                        freeHourHTML = '<div class="free-hour-value _0">' + freeHour + '</div>';
                    }

                    var rate = Math.round(((taskCount - outTask) / taskCount) * 100, 0);

                    var img = '';
                    if (rate >= 80) {
                        img = '<img src="../Content/img/happy.png" />';
                    } else if (rate < 80 && rate >= 40) {
                        img = '<img src="../Content/img/working.png" />';
                    } else if (rate < 40) {
                        img = '<img src="../Content/img/busy.png" />';
                    } else {
                        img = '<img src="../Content/img/happy.png" />';
                    }


                    //var taskRate = rate + '% Task Completed';
                    var taskRate = (taskCount - outTask) + '/' + taskCount + ' Task Completed';

                    if (taskCount == 0) {
                        taskRate = 'No Task For Today';
                    }

                    var hourDesc = 'Hours Left';
                    if (freeHour <= 1) {

                        hourDesc = 'Hour Left';
                    }

                    var html = '<div class="rate-container">'
                        + '<div id="addTask" class="header-content">' + '<div class="addTask-icon">+</div>' + '<div class="addTask-value">Add Task</div>' + '</div>'
                        + '<div class="free-hour-container header-content">' + freeHourHTML + '<div class="free-hour-description">' + hourDesc + '</div>' + '</div>'
                        + '<div class="task-rate-container header-content">' + '<div class="task-rate-value">' + img + '</div>' + '<div class="task-rate-description">' + taskRate + '</div>' + '</div>' + '</div>';


                    //reset
                    if ($('.sp13custom-Search.col.six .rate-container').length > 0) {
                        $('.sp13custom-Search.col.six .rate-container').addClass('animated custom fadeOutFull');
                        $('.sp13custom-Search.col.six .rate-container').one('animationend', function () {
                            $('.sp13custom-Search.col.six .rate-container').remove();
                            $('.sp13custom-Search.col.six').prepend(html);
                            $('.sp13custom-Search.col.six .rate-container').addClass('animated fadeInFull custom');

                            addAddTaskHandler();

                            applyProdStatusOverride();

                        });
                    } else {
                        $('.sp13custom-Search.col.six').prepend(html);
                        $('.sp13custom-Search.col.six .rate-container').addClass('animated fadeInFull custom');

                        addAddTaskHandler();

                        applyProdStatusOverride()
                    }

                }
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

//global popup handler
//changed obj into a wrapper --> {data:{},type:...}
//2 callback func, htmlGen --> can be a func to gen html popup, can be a jquery obj, can be a string 
//popUpButtonHandler --> create popup button handler, obj --> passed obj
function loadPopUp(htmlGen, popUpButtonHandler, obj, reload) {
    try {
        /*
        if (!reload) {
            $('body').addClass('loading');
        }
        */

        var popUpHTML;

        //gen htmlpopup
        if (typeof htmlGen === 'function') {
            if(obj){
                popUpHTML = htmlGen(obj.data, obj.type);
            }
            else{
                popUpHTML = htmlGen();
            }
        }
        else {
            popUpHTML = htmlGen;
        }
    
        if ($('.mfp-content').length > 0 && reload) {
            //some fade out fade in
            $('.popup-container').addClass('animated custom fadeOutFull');

            $('.popup-container').one('animationend', function () {
                $('.mfp-content').html(popUpHTML);
                $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');
                $('.popup-container').addClass('animated custom fadeInFull');
                if (popUpButtonHandler && typeof (popUpButtonHandler) === "function") {
                    if (obj) {
                        popUpButtonHandler(obj.data, obj.type);
                    }
                    else {
                        popUpButtonHandler();
                    }
                }
            });
        }
        else {

            $.magnificPopup.open({
                items: {
                    src: popUpHTML,
                    type: 'inline'
                },
                closeOnBgClick: false,
                //the close button should be default
                closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close">x</button>',
                callbacks: {
                    open: function () {
                        //add event handlers on the button, if there is a handler
                        if (popUpButtonHandler && typeof (popUpButtonHandler) === "function") {
                            if (obj) {
                                popUpButtonHandler(obj.data, obj.type);
                            }
                            else {
                                popUpButtonHandler();
                            }
                        }
                    }
                }
            });
        }
    }
    catch (err) {
        console.log("Exception: " + err.message);
    }
}


function generateNewsPopUpHTML(arr, type) {

    var popUpHTML = '';

    if (type == 'config') {
        popUpHTML = '<div class="popup-container" id="rssFeedPopUp">'
              + '<div class="popup-title task">RSS Feed Config</div>'
              + '<table class="popup-content rssFeed">'
              + '<tr class="rssFeedChannel header">'
              + '<td class="rssFeedHeader active">Default</td>'
              + '<td class="rssFeedHeader title">Feeds Title</td>'
              + '<td class="rssFeedHeader url">Feeds URL</td>'
              + '<td class="rssFeedHeader url">Description</td>'
              + '</tr>';

        if (arr) {
            arr.forEach(function (a) {
                var checked = "";
                if (parseBool(a.active)) {
                    checked = "checked";
                }

                popUpHTML += '<tr class="rssFeedChannel item">'
                + '<td class="rssFeedItem active" id="' + getRadioID(a.title) + '">'
                + '<div class="mobileTitle">Default</div>'
                + '<input type="radio" ' + checked + '>'
                + '</td>'
                + '<td class="rssFeedItem title">'
                + '<div class="mobileTitle">Title</div>'
                + '<input type="text" value="' + a.title + '" />'
                + '</td>'
                + '<td class="rssFeedItem url">'
                + '<div class="mobileTitle">URL</div>'
                + '<input type="text" value="' + a.url + '" />'
                + '</td>'
                + '<td class="rssFeedItem desc">'
                + '<div class="mobileTitle">Desc</div>'
                + '<input type="text" value="' + a.desc + '" />'
                + '</td>'
                //delete
                + '<td class="deleteRow">'
                + '<div class="deleteRow-button">X</div>'
                + '</td>'
                + '</tr>';
            });
        }

        popUpHTML += '</table>';

        popUpHTML += '<div class="popup-button-container">'
                    + '<div class="popup-button save">Save Config</div>'
                    + '<div class="popup-button add">Add Feed</div>'
                    + '<div class="popup-button cancel">Cancel</div>'
                    + '</div>' + '</div>';
    }
    else if (type == 'select') {
        popUpHTML = '<div class="popup-container" id="rssFeedPopUp">'
              + '<div class="popup-title task">Select News Feed</div>'
              + '<table class="popup-content rssFeed">'
              + '<tr class="rssFeedChannel header">'
              + '<td class="rssFeedHeader select active">Selected</td>'
              + '<td class="rssFeedHeader select title">Feeds Title</td>'
              + '<td class="rssFeedHeader select url">Description</td>'
              + '</tr>';

        rssFeedUrl.forEach(function (a) {
            popUpHTML += '<tr class="rssFeedChannel item">'
                      + '<td class="rssFeedItem select active" id="' + getRadioID(a.title) + '">'
                      + '<div class="mobileTitle">Selected</div>';

            if (!!userFeedSelection && !!userFeedSelection.title) {
                    if (userFeedSelection.title == a.title) {
                        popUpHTML += '<input type="radio" checked>';
                    }
                    else {
                        popUpHTML += '<input type="radio">';
                    }
            }
            else {
                popUpHTML += '<input type="radio">';
            }

            popUpHTML += '</td>'
               + '<td class="rssFeedItem select title">'
               + '<div class="mobileTitle">Title</div>'
               + '<div class="feedItemText">' + a.title + '</div>'
               + '</td>'
               + '<td class="rssFeedItem select desc">'
               + '<div class="mobileTitle">Desc</div>'
               + '<div class="feedItemText">' + a.desc + '</div>'
               + '</td></tr>';
        });

        popUpHTML += '</table>';
        
        popUpHTML += '<div class="popup-button-container">'
                    + '<div class="popup-button ok">Ok</div>'
                    + '<div class="popup-button cancel">Cancel</div>'
                    + '</div>' + '</div>';
    }

    return popUpHTML;
}

function getRadioID(str) {
    return str.trim().split(/\s+/).join('');
}

function newsPopUpButtonHandler(obj, type) {
    if (type == 'config') {
        //add radio button handler
        radioButtonHandler($('.rssFeedItem.active input'),'.rssFeedItem.active');

        //add input mini validation
        rssConfigInputValidation($('.rssFeedItem.title input, .rssFeedItem.url input'));
        
        //assign auto ID on the radio button
        autoAssignID($('.rssFeedItem.title input'));

        //add delete row click handler
        deleteRSSConfigHandler($('.rssFeedChannel.item .deleteRow-button'));

        //add max input validation
        maxValueValidation($('.rssFeedItem.desc input'), 50);

        //add feeds
        $('.popup-button.add').on('click', function () {

            var popUpHTML = '<tr class="rssFeedChannel item">'
                + '<td class="rssFeedItem active">'
                + '<div class="mobileTitle">Default</div>'
                + '<input type="radio">'
                + '</td>'
                + '<td class="rssFeedItem title">'
                + '<div class="mobileTitle">Title</div>'
                + '<input type="text" value="" />'
                + '</td>'
                + '<td class="rssFeedItem url">'
                + '<div class="mobileTitle">URL</div>'
                + '<input type="text" value="" />'
                + '</td>'
                + '<td class="rssFeedItem desc">'
                + '<div class="mobileTitle">Desc</div>'
                + '<input type="text" value="" />'
                + '</td>'
                //delete
                + '<td class="deleteRow">'
                + '<div class="deleteRow-button">X</div>'
                + '</td>'
                + '</tr>';

            var popUpEl = $(popUpHTML);

            $('.popup-content.rssFeed>tbody').append(popUpEl);

            //add handler
            radioButtonHandler(popUpEl.find('.rssFeedItem.active input'), '.rssFeedItem.active');
            rssConfigInputValidation(popUpEl.find('.rssFeedItem.title input, .rssFeedItem.url input'));
            autoAssignID(popUpEl.find('.rssFeedItem.title input'));
            deleteRSSConfigHandler(popUpEl.find('.deleteRow .deleteRow-button'));
            maxValueValidation(popUpEl.find('.rssFeedItem.desc input'), 50);

        });

        //for admin to save edit the feed selection
        $('.popup-button.save').on('click', function () {
            if (!($(this).hasClass('disabled'))) {
                //refresh rssFeedUrl
                tempFeedUrl = [];
                var status = true;

                $('.rssFeed .rssFeedChannel.item').each(function (index) {
                    var active = $(this).find('.active input')[0].checked;
                    var title = $(this).find('.title input').val().trim();
                    var url = $(this).find('.url input').val().trim();
                    var desc = $(this).find('.desc input').val().trim();

                    //validation
                    if (checkIsNull(title)) {
                        popUpAddError($(this).find('.title'), 'Please input a title');
                        status = false;
                        return false;
                    }

                    if (checkIsNull(url)) {
                        popUpAddError($(this).find('.url'), 'Please input a url');
                        status = false;
                        return false;
                    }

                    if (desc.length > 50) {
                        popUpAddError($(this).find('.desc'), 'Input exceeds 50 characters');
                        status = false;
                        return false;
                    }


                    tempFeedUrl.push({
                        title: title,
                        url: url,
                        active: active,
                        desc: desc
                    });

                });

                if (status) {
                    $.magnificPopup.close();
                    rssFeedUrl = tempFeedUrl;
                    updateRSSFeedConfig();
                }
            }
        });

    }
    else if (type == 'select') {

        //add radio button handler
        radioButtonHandler($('.rssFeedItem.active input'), '.rssFeedItem.active');

        $('.popup-button.ok').on('click', function () {
            //get selection
            var selectedFeedTitle = $('.rssFeedItem.active input:checked').closest('.rssFeedItem').siblings('.rssFeedItem.title').find('.feedItemText').text();

            updateRSSFeedPreference(selectedFeedTitle);

            userFeedSelection = getRSSFeedByTitle(rssFeedUrl, selectedFeedTitle);

            loadRSSFeed(userFeedSelection);

            $.magnificPopup.close();
        });
    }

    $('.popup-button.cancel').on('click', function () {
        $.magnificPopup.close();
    });
}


/*rss Config event helper*/
function radioButtonHandler(el, selector) {
    
    if (!(el instanceof jQuery)) {
        el = $(el);
    }

    
    el.on('click', function () {
        var id = $(this).closest('.active').attr('ID');

        //disable all radio button first
        $(selector + ' input').prop('checked', false);

        //check the correct one
        $(selector + '#' + id + ' input').prop('checked', true);
    });
}

function autoAssignID(el) {
    if (!(el instanceof jQuery)) {
        el = $(el);
    }

    el.on('change', function () {
        //just to remove whitespace
        var id = getRadioID($(this).val());

        //auto assign ID
        $(this).closest(".rssFeedItem").siblings('.active').attr("ID", id);
    });
}

function rssConfigInputValidation(el) {
    if (!(el instanceof jQuery)) {
        el = $(el);
    }

    el.on('change', function () {
        if ($(this).val().trim() == "") {
            popUpAddError($(this).parent('.rssFeedItem'), 'Please input a value.');
        }
        else {
            popUpClearError($(this).parent('.rssFeedItem'));
        }
    });
}

function maxValueValidation(el, max) {
    if (!(el instanceof jQuery)) {
        el = $(el);
    }

    el.on('change', function () {
        if ($(this).val().trim().length > max) {
            popUpAddError($(this).parent('.rssFeedItem'), 'Input exceeds 50 characters.');
        }
        else {
            popUpClearError($(this).parent('.rssFeedItem'));
        }
    });
}

function deleteRSSConfigHandler(el) {
    if (!(el instanceof jQuery)) {
        el = $(el);
    }

    el.on('click', function (e) {
        e.stopPropagation();
        $(this).closest('.rssFeedChannel.item').remove();
        if ($('#rssFeedPopUp').find('.error-message').length == 0) {
            if ($('.popup-button.save').length > 0) {
                $('.popup-button.save').removeClass('disabled');
            }
        }
    });
}
/**/


/*task helper function*/
//generate task popupHTML
function generateTaskPopUpHTML(obj,type) {

    var popUpHTML = '';

    //general task popup
    if (type.toLowerCase() == 'tatm' || type.toLowerCase() == 'tabm') {

        var extension = obj.plusDay + ' day';
        if (obj.plusDay > 1) {
            extension = obj.plusDay + ' days';
        }

        //add link for url
        obj.description = convertUrlIntoLink(obj.description);

        popUpHTML = '<div class="popup-container">' + '<div class="popup-title task">' + 'TASK' //obj.category
                + '</div>' + '<div class="popup-subject">' + obj.subject
                + '</div>' + '<table class="popup-content">' + '<tr>' + '<td class="popup-content-header">Assigned By:</td>'
                //+	'<td class="popup-content-body">'+msg.Sender.EmailAddress.Name+'</td>'
                + '<td class="popup-content-body">' + obj.assignedBy + '</td>' + '</tr>'
                + '<tr>' + '<td class="popup-content-header">Assigned To:</td>' + '<td class="popup-content-body">' + obj.assignedTo + '</td>' + '</tr>'
                + '<tr>' + '<td class="popup-content-header">Task Description:</td>'
                //BodyPreview the plain text version
                + '<td class="popup-content-body">' + obj.description + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Start Date:</td>' + '<td class="popup-content-body">' + obj.createdDate + '</td>' + '</tr>'
                + '<tr>' + '<td class="popup-content-header">Due Date:</td>' + '<td class="popup-content-body">' + obj.dueDate + '</td>' + '</tr>';

        //delayed
        if (obj.status.toLowerCase() == 'delayed' || obj.plusDay > 0)
        {
            popUpHTML += '<tr>' + '<td class="popup-content-header">Original Due Date:</td>' + '<td class="popup-content-body">' + obj.originalDueDate + '</td>' + '</tr>'
                + '<tr>' + '<td class="popup-content-header">Extension:</td>'
                //BodyPreview the plain text version
                + '<td class="popup-content-body">' + extension + '</td>' + '</tr>';
        }


        popUpHTML += '</table>';

        popUpHTML += '<div class="popup-button-container">'
        /*
        if (obj.channel.toLowerCase == "form") {
            popUpHTML += '<a class="popup-button" href="' + obj.form + '" target="_blank">View More</a>';
        }
        */

        if (obj.channel.toLowerCase != "form") {
            if (type == 'tatm') {
                if (obj.plusDay < 3) {
                    popUpHTML += '<div class="popup-button plusOne">+1 Day</div>';
                }
            }

            if (!checkIsNull(yammerID)) {
                //check if message id and thread id is specified
                if (!checkIsNull(obj.messageId) && !checkIsNull(obj.threadId)) {
                    if (getCookie("yammer_access_token")) {
                        popUpHTML += '<div class="popup-button more">View Yammer Conversation</div>';
                    }
                    else {
                        popUpHTML += '<div class="popup-button loginYam">View Yammer Conversation</div>';
                    }
                }
                    //start yammer converstation
                else {
                    if (getCookie("yammer_access_token")) {
                        popUpHTML += '<div class="popup-button start">Start Yammer Conversation</div>';
                    }
                    else {
                        popUpHTML += '<div class="popup-button loginYamStart">Start Yammer Conversation</div>';
                    }
                }
            }
        }

        popUpHTML += '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';
    }
    //add task popup
    else if(type.toLowerCase() == 'addtask'){
        popUpHTML = '<div class="popup-container" id="addTaskPopUp">'
          + '<div class="popup-title task">Add Task</div>'
          + '<table class="popup-content addTask">'
          + '<tr>'
          + '<td class="popup-content-header">Assigned By</td>'
          + '<td class="popup-content-body assignedBy"></td>'
          + '</tr>'
          + '<tr class="assignedToNameRow">'
          + '<td class="popup-content-header">Assigned To</td>'
          + '<td class="popup-content-body assignedToName ui-front"><input type="text" />'
          //+ '<span class="assignedToEmail"></span>'
          + '<div class="addAssignee rowButton">+</div>'
          + '</td>'
          + '</tr>'
          + '<tr>'
          + '<td class="popup-content-header">Task Name</td>'
          + '<td class="popup-content-body taskName"><input type="text" /></td>'
          + '</tr>'
          + '<tr>'
          + '<td class="popup-content-header">Task Description</td>'
          + '<td class="popup-content-body taskDesc"><textarea class="task-description-input" rows="4"></textarea></td>'
          + '</tr>'
          + '<tr>'
          + '<td class="popup-content-header">Due Date</td>'
          + '<td class="popup-content-body dueDate"><input type="text" class="datepicker"/></td>'
          + '</tr>'
          + '</table>';

        popUpHTML += '<div class="popup-button-container">'
                    + '<div class="popup-button submit">Add Task</div>'
                    //+ '<div class="popup-button reset">Reset Form</div>'
                    + '<div class="popup-button cancel">Cancel</div>'
                    + '</div>' + '</div>';
    }

    return popUpHTML;
}

//buton handler function for task popup
function taskButtonHandler(obj, type) {

        $('.popup-button.cancel').on('click', function () {
            $.magnificPopup.close();
        });

        $('.popup-button.more').on('click', function () {
            if (obj.channel.toLowerCase() != 'form') {
                $.magnificPopup.close();
                $('body').addClass('loading');
                var yammerToken = getCookie("yammer_access_token");
                if (yammerToken) {
                    loadYammerRelatedMessages({
                        token:yammerToken, 
                        messageID:obj.messageId, 
                        threadID: obj.threadId
                    });
                }
            }
        });

        $('.popup-button.start').on('click', function () {
            if (obj.channel.toLowerCase() != 'form') {
                $.magnificPopup.close();
                $('body').addClass('loading');
                var yammerToken = getCookie("yammer_access_token");
                if (yammerToken) {

                    var target_user_email = "";
                    var type = '';
                    if (obj.assignedByEmail == userEmail) {
                        target_user_email = obj.assignedToEmail;
                        if (obj.assignedToEmail == userEmail) {
                            type = 'tatm';
                        }
                        else {
                            type = 'tabm';
                        }
                    }
                    else {
                        target_user_email = obj.assignedByEmail;
                        type = 'tatm';
                    }

                    var passedObj = { 
                        data: {
                            taskID: obj.taskId,
                            token: yammerToken,
                            subject: obj.subject,
                            target_user_email: target_user_email,
                            reload: true,
                            type: type
                        },
                        type: 'start' 
                    };

                    loadPopUp(generateYammerPopUpHTML, yammerButtonHandler, passedObj);
                    $('body').removeClass('loading');

                }
            }
        });

        $('.popup-button.loginYam').on('click', function () {
            if (obj.channel.toLowerCase() != 'form') {
                $.magnificPopup.close();
                yam.login(function (response) {
                    console.log(response);
                    if (response.authResponse) {
                        setCookie("yammer_access_token", response.access_token.token);

                        refreshYammer();
                            loadYammerRelatedMessages({
                                token: response.access_token.token,
                                messageID: obj.messageId,
                                threadID: obj.threadId
                            });
                    }
                });
            }
        });

        $('.popup-button.loginYamStart').on('click', function () {
            if (obj.channel.toLowerCase() != 'form') {
                $.magnificPopup.close();
                yam.login(function (response) {
                    console.log(response);
                    if (response.authResponse) {
                        setCookie("yammer_access_token", response.access_token.token);

                        refreshYammer();

                        var target_user_email = "";
                        var type = '';
                        if (obj.assignedByEmail == userEmail) {
                            target_user_email = obj.assignedToEmail;
                            if (obj.assignedToEmail == userEmail) {
                                type = 'tatm';
                            }
                            else {
                                type = 'tabm';
                            }
                        }
                        else {
                            target_user_email = obj.assignedByEmail;
                            type = 'tatm';
                        }

                        var passedObj = {
                            data: {
                                taskID: obj.taskId,
                                token: response.access_token.token,
                                subject: obj.subject,
                                target_user_email: target_user_email,
                                reload: true,
                                type: type
                            },
                            type: 'start'
                        };

                        loadPopUp(generateYammerPopUpHTML, yammerButtonHandler, passedObj);
                    }
                });
            }
        });

        //null or undefined checker
        if (type) {
            if (type.toLowerCase() == "tatm") {
                $('.popup-button.plusOne').on('click', function () {
                    addOneDayTask(userEmail, obj.taskId);
                });
            }
        }
}
/*end task helper*/


/*schedule helper function*/
function generateScheduleHTML(obj, type) {
    //type --> schedule, reply
    
    var popUpHTML = '';

    if (type.toLowerCase() == 'reply') {
        popUpHTML = '<div class="popup-container ">' + '<div class="popup-subject">Quick Reply</div>';
        popUpHTML += '<div class="ms-Dropdown" tabindex="0"><i class="ms-Dropdown-caretDown ms-Icon ms-Icon--caretDown"></i><select class="ms-Dropdown-select"><option value="">Select a reply…</option><option value="I am going to be 15 minutes late">15 minutes late</option><option value="I am going to be 30 minutes late">30 minutes late</option><option value="I am going to be 45 minutes late">45 minutes late</option><option value="Do not bother waiting for me">Totally late</option></select></div>';
        popUpHTML += '<textarea placeholder="Type your message..." class="popup-reply-textarea">' + '</textarea>' + '<div class="popup-reply-error-status"></div>' + '<div class="popup-reply-button-container">' + '<div class="popup-button send">Send</div>' + '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';
    }
    else {
        var responseString = obj.response;
        // add response time 
        if (obj.response.toLowerCase() == "accepted") {
            responseString = obj.response + ' (' + obj.responseTime + ')';
        }

        popUpHTML = '<div class="popup-container">' + '<div class="popup-title schedule">' + 'Schedule' + '</div>';

        popUpHTML += '<div class="popup-subject">' + obj.subject + '</div>' + '<table class="popup-content">' + '<tr>' + '<td class="popup-content-header">Location:</td>' + '<td class="popup-content-body">' + obj.location + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Time:</td>' + '<td class="popup-content-body">' + obj.time + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Description:</td>' + '<td class="popup-content-body">' + obj.body + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Response:</td>' + '<td class="popup-content-body">' + responseString + '</td>' + '</tr>' + '</table>';
        //+	'</div>';

        if ((obj.response.toLowerCase() == "accepted") || (obj.response.toLowerCase() == "organizer") || (obj.isOrganizer == 'true')) {
            popUpHTML += '<div class="popup-button-container">' + '<div class="popup-button reply">Quick Reply</div>'
                //open owa button
                + '<div class="popup-button owa">Open Item</div>';
        }
        else {
            popUpHTML += '<div class="popup-button-container">' + '<div class="popup-button accept">Accept</div>' + '<div class="popup-button reply">Quick Reply</div>'
                //open owa button
                + '<div class="popup-button owa">Open Item</div>';
        }

        popUpHTML += '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';

    }
    
    return popUpHTML;
}


//schedule button handler
function scheduleButtonHandler(obj, type) {
    if (type.toLowerCase() == 'reply')
    {
        $('.popup-button.send').on('click', function () {

            var messageContent = $('.popup-reply-textarea').val();
            if (messageContent.trim() == "") {
                //don't do anything
                $('.popup-reply-error-status').html('Please input a message');
            } else {
                $('.popup-reply-error-status').html('');
                
                quickReplySchedule(messageContent, 'RE: ' + obj.subject, obj.organizerEmail);                
            }
        });

        $('.popup-button.cancel').on('click', function () {
            $.magnificPopup.close();
        });

        $('select.ms-Dropdown-select').on('change', function () {
            if ($(this).val() != '') {
                $('.popup-reply-textarea').val($(this).val());
            }
        });
    }
    else {
        $('.popup-button.accept').on('click', function () {
              acceptEvent(userEmail, obj.id);
            $.magnificPopup.close();
        });

        $('.popup-button.owa').on('click', function () {
            window.open(obj.webLink);
            $.magnificPopup.close();
        });

        $('.popup-button.cancel').on('click', function () {
            $.magnificPopup.close();
        });

        $('.popup-button.reply').on('click', function () {
            $('.mfp-container .mfp-content').attr('style', 'max-height: 300px;overflow:hidden;');

            $('.popup-container').addClass('animated custom fadeOutFull');
            $('.mfp-container .mfp-content').one('transitionend', function () {
                $('.mfp-container .mfp-content').attr('style', 'max-height: 300px;overflow:auto;');
                $('.mfp-container .mfp-content').html(generateScheduleHTML(obj,'reply'));
                $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');

                //change handler
                $('select.ms-Dropdown-select').on('change', function () {
                    if ($(this).val() != '') {
                        $('.popup-reply-textarea').val($(this).val());
                    }
                });

                $('.popup-button.send').on('click', function () {

                    var messageContent = $('.popup-reply-textarea').val();
                    if (messageContent.trim() == "") {
                        //don't do anything
                        $('.popup-reply-error-status').html('Please input a message');
                    } else {
                        $('.popup-reply-error-status').html('');
                        quickReplySchedule(messageContent, 'RE: ' + obj.subject, obj.organizerEmail);
                    }
                });

                $('.popup-button.cancel').on('click', function (event) {
                    event.stopPropagation();
                    $('.mfp-container .mfp-content').attr('style', 'overflow:hidden;');
                    $('.mfp-container .mfp-content').html(generateScheduleHTML(obj, type));
                    $('.popup-container').addClass('animated custom fadeInFull');
                    $('.popup-container').one('animationend', function () {
                        $('.mfp-container .mfp-content').attr('style', null);
                        $('.popup-container').removeClass('animated custom fadeInFull');
                    });
                    $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');
                    //popUpButtonHandler();
                    scheduleButtonHandler(obj, type);
                });
            });
        });
    }
}
/*end schedule helper*/

function loadScheduleFeed(userEmail, reload) {
    try {

        //check connection settings
        if (checkConnectionSettings('outlook')) {

            var accessToken = getAccessToken();

            var daysForward = 7;

            var startDate = new Date().toISOString();
            var endDate = new Date().addDays(daysForward).toISOString();

            var endpointUrl = graphEndpoint + "me/calendarView?startDateTime=" + startDate + "&enddatetime=" + endDate;
           
            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                headers:{
                    'Authorization': 'Bearer ' + accessToken   
                },
                success: function (json) {

                    console.log(json);
                    var data = json.value;
                    
                    if (data.length > 0) {

                        refreshPanel($("#myCalendarDetails"), function () {
                            loadScheduleData(data);
                            scheduleEventHandler();
                        });

                    }
                    else {
                        $("#myCalendarDetails").html('<div class="empty-msg">Seems like you do not have any meeting!</div>');
                    }

                    //reload != undefined
                    if (reload) {
                        $('body').removeClass('loading'); //remove loading gif
                    }

                },
                error: function (err) {
                    console.log('ajax error: ' + err.statusText);
                    $('#myCalendarDetails').html('<div class="empty-msg">Data request failed.</div>');
                    $('body').removeClass('loading');
                }
            });
        }
        else {
            refreshPanel($("#myCalendarDetails"), function () {
                noCredentialSchedule();
            });
        }
    }
    catch (err) {
        console.log("error: " + err.message);
        //no credentials
        if (err.message == 'EG0004') {
            $('#myCalendarDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
        }
            //general handler
        else {
            $('#myCalendarDetails').html('<div class="empty-msg">Data request failed.</div>');
        }
    }
}

//schedule data
function loadScheduleData(data) {

    var html = '';
    var locationLimit = 35;
    var titleLimit = 40;

    $(data).each(function (index) {
        var startDate = new Date(convertIntoISOString(this.start.dateTime));
        var endDate = new Date(convertIntoISOString(this.end.dateTime));
        var today = new Date();

        //just to compare date
        var todayDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        var checkStartDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
        var checkEndDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());


        if (endDate.getTime() >= today.getTime()) {
            var diffDays = 0;
            var timeDiff = 0;

            var startDay = ("0" + startDate.getDate()).slice(-2);
            var startMonth = getMonthString(startDate.getMonth());

            if (endDate >= today) {
                todayDate.setHours(0, 0, 0, 0);
                checkEndDate.setHours(0, 0, 0, 0);
                timeDiff = checkEndDate.getTime() - todayDate.getTime();
                diffDays = Math.round(timeDiff / (1000 * 3600 * 24));

                startDay = ("0" + endDate.getDate()).slice(-2);
                startMonth = getMonthString(endDate.getMonth());
            }
            else if (startDate <= today) {
                todayDate.setHours(0, 0, 0, 0);
                checkStartDate.setHours(0, 0, 0, 0);
                timeDiff = checkStartDate.getTime() - todayDate.getTime();
                diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
            }
            
            //schedule lasts multiple days
            var startTime = getShortDateString(startDate, true);
            var endTime = getShortDateString(endDate, true);

            var dateFull = getDateString(startDate, true) + ' - ' + getDateString(endDate, true);

            //schedule in the same day
            if (startDate.getDate() == endDate.getDate()) {
                startTime = formatAMPM(startDate);
                endTime = formatAMPM(endDate);
                dateFull = getDateString(startDate) + ' ' + startTime + ' - ' + endTime;
            };


            

            var date = "";
            if (diffDays == 0) {
                date = "Today";
            } else {
                if (diffDays == 1) {
                    date = diffDays + ' Day';
                } else {
                    date = diffDays + ' Days';
                }
            }

            var response = this.responseStatus.response;
            var responseDate = new Date(this.responseStatus.time);
            var responseTimeString = getDateString(responseDate, true);

            if (response == 'notResponded') {
                response = 'Not Responded';
            }

            var location = textAbstract(checkValue(this.location.displayName), locationLimit);
            var subject = textAbstract(checkValue(this.subject), titleLimit);
            var body = checkValue(this.bodyPreview).escape();

            if (!(body == 'N/A')) {
                body += '...';
            }

            subject = subject.escape();
            location = location.escape();

            html += '<div class="schedule-slider-item slider-item" data-mfp-src="' + this.webLink + '">' + '<div class="schedule-slider-date">';

            if (date == "Today") {
                html += '<div class="schedule-slider-date-when today">';
            } else {
                html += '<div class="schedule-slider-date-when">';
            }

            html += date + '</div>' + '<div class="schedule-slider-date-start">' + '<div class="schedule-slider-date-start-day">' + startDay + '</div>' + '<div class="schedule-slider-date-start-month">' + startMonth + '</div>' + '</div>' + '</div>' + '<div class="schedule-slider-content">' + '<div class="schedule-slider-content-category">' + startTime + ' - ' + endTime + '</div>' + '<div class="schedule-slider-content-subject">' + subject + '</div>' + '<div class="schedule-slider-content-location">' + location + '</div>' + '</div>' + '<div class="schedule-popup" style="display:none">' + '<div class="schedule-full-subject">' + checkValue(this.subject) + '</div>' + '<div class="schedule-full-location">' + checkValue(this.location.displayName) + '</div>' + '<div class="schedule-description">' + body + '</div>' + '<div class="schedule-organizer-email">' + this.organizer.emailAddress.address + '</div>' + '<div class="schedule-isOrganizer">' + this.isOrganizer.toString() + '</div>' + '<div class="schedule-full-date">' + dateFull + '</div>' + '<div class="schedule-response">' + response + '</div>' + '<div class="schedule-response-time">' + responseTimeString + '</div>' + '<div class="schedule-event-id">' + this.id + '</div>' + '</div>' + '</div>';
        }
    });

    $("#myCalendarDetails").html(html);

    //slick init
    $("#myCalendarDetails").slick({
        slide: '.schedule-slider-item',
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 769,
            settings: {
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });

    $("#myCalendarDetails").on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        //remove all other overlay							    
        var others = $('#myCalendarDetails .modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');

                $(this).siblings('.circle').addClass('animated slideOutUp'); //fadeOut

                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }

    });
}

//continue development here
function noCredentialSchedule() {
    var tdyDate = new Date();
    var startDate = new Date(tdyDate.setHours(10,0,0,0));
    var endDate = new Date(tdyDate.setHours(15,0,0,0));

    /*
    var sampleData = [{
        StartDate: startDate.toISOString(),
        EndDate: endDate.toISOString(),
        ScheduleId: "AAMkAGRhMjU5MjM3LWIwNzktNDVkYS1iMGRhLTM2YmUyZmI1NTViZABGAAAAAABP8V6Qc_A8Q6bkmXQ1ZxsfBwCGzd2azMw1Toaq80ua03EuAAAAAAENAACGzd2azMw1Toaq80ua03EuAADmd__aAAA=",
        Subject: "Meeting",
        Location: "Office, Meeting Room 1",
        Body: "Sample Schedule Body",
        OrganizerName: "John Doe",
        OrganizerEmail: "john@celgo.net",
        isOrganizer: false,
        ResponseStatus: "NotResponded",
        ResponseTime: "0001-01-01T00:00:00Z",
        Attendees: {
            "jane@celgo.net": "Jane Doe",
            "bob@celgo.net": "Bob Ross"
        },
        WebLink: ""
    },
    {
        StartDate: startDate.toISOString(),
        EndDate: endDate.toISOString(),
        ScheduleId: "AAMkAGRhMjU5MjM3LWIwNzktNDVkYS1iMGRhLTM2YmUyZmI1NTViZABGAAAAAABP8V6Qc_A8Q6bkmXQ1ZxsfBwCGzd2azMw1Toaq80ua03EuAAAAAAENAACGzd2azMw1Toaq80ua03EuAADmd__aAAA=",
        Subject: "Appointment",
        Location: "Office, Meeting Room 2",
        Body: "Sample Schedule Body",
        OrganizerName: "John Doe",
        OrganizerEmail: "john@celgo.net",
        isOrganizer: false,
        ResponseStatus: "NotResponded",
        ResponseTime: "0001-01-01T00:00:00Z",
        Attendees: {
            "jane@celgo.net": "Jane Doe",
            "bob@celgo.net": "Bob Ross"
        },
        WebLink: ""
    },
    {
        StartDate: startDate.toISOString(),
        EndDate: endDate.toISOString(),
        ScheduleId: "AAMkAGRhMjU5MjM3LWIwNzktNDVkYS1iMGRhLTM2YmUyZmI1NTViZABGAAAAAABP8V6Qc_A8Q6bkmXQ1ZxsfBwCGzd2azMw1Toaq80ua03EuAAAAAAENAACGzd2azMw1Toaq80ua03EuAADmd__aAAA=",
        Subject: "Meeting Appointment",
        Location: "Office, Meeting Room 3",
        Body: "Sample Schedule Body",
        OrganizerName: "John Doe",
        OrganizerEmail: "john@celgo.net",
        isOrganizer: false,
        ResponseStatus: "NotResponded",
        ResponseTime: "0001-01-01T00:00:00Z",
        Attendees: {
            "jane@celgo.net": "Jane Doe",
            "bob@celgo.net": "Bob Ross"
        },
        WebLink: ""
    }];

    loadScheduleData(sampleData);

    $("#myCalendarDetails .slick-track").css('opacity', '0.7');
    */

    //add overlay with 2 buttons
    $("#myCalendarDetails").append("<div class='panel-modal-firstload'></div>");
    $("#myCalendarDetails").append("<div class='panel-button-container'><div class='configure panel-button popup-button'>Activate</div><div class='minimize panel-button popup-button'>Minimize</div></div>");

    //set button handler

    $("#myCalendarDetails .configure.panel-button").on('click', function () {
        loadDetailedConfig('outlook');
    });

    function maximizePanel() {
        $('#myCalendarPanel').removeClass('minimized').animate({ height: 130 }, 400);
        $('#myCalendarPanel').off('click', maximizePanel);

        
        $('#myCalendarHeading').on('click', function () {
            $('#myCalendarDetails').html('<div class="panel-loading"></div>');
            loadScheduleFeed(userEmail);
        });
        
    }

    $("#myCalendarDetails .minimize.panel-button").on('click', function () {
        var panel = $(this).closest('.panel');

        //remove click handler
        $('#myCalendarHeading').off('click');

        panel.animate({ height: 30 }, 400, 'swing', function () {
            panel.addClass('minimized');
            panel.on('click', maximizePanel);
        });
    });
    
}

function scheduleEventHandler() {
    //event handler need to be moved from numevents if block below
    $('.schedule-slider-item').on('click', function (event) {

        //remove all other overlay
        var slickParent = $(event.target).closest('.slick-slider');


        var others = $('.modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-slider').not(slickParent).find('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }
        var parent = $(event.target).closest('.slider-item');
        var event_id = parent.find('.schedule-event-id').text();


        //change different overlay buttons here
        //for non-accept status: None,Not Responded					    
        var overlay = $('<div class="modal-overlay"></div><div class="circle circle-accept left">ACCEPT</div><div class="circle circle-view center">VIEW</div><div class="circle circle-reply right two-lines">QUICK<BR>REPLY</div>');

        //for accepted status: Organizer, Accepted
        if (($(this).find('.schedule-response').text() == "organizer") || ($(this).find('.schedule-response').text() == "accepted")) {
            overlay = $('<div class="modal-overlay"></div><div class="circle circle-view left two">VIEW</div><div class="circle circle-reply right two two-lines">QUICK<BR>REPLY</div>');
        }

        //check if organizer
        if (parent.find('.schedule-isOrganizer').text() == "true") {
            overlay = $('<div class="modal-overlay"></div><div class="circle circle-view left two">VIEW</div><div class="circle circle-reply right two two-lines">QUICK<BR>REPLY</div>');
        }


        overlay.addClass('animated fadeIn');
        $(this).closest('.slick-slider').find('.slick-dots').css('z-index', '-1');

        parent.css('cursor', 'auto');
        parent.append(overlay);

        $('.circle').addClass('animated slideInUp');

        //data object
        var obj = {
            subject: parent.find('.schedule-full-subject').html(),
            response: parent.find('.schedule-response').text(),
            responseTime: parent.find('.schedule-response-time').text(),
            location: parent.find('.schedule-full-location').html(),
            time: parent.find('.schedule-full-date').text(),
            body: parent.find('.schedule-description').html(),
            organizerEmail: parent.find('.schedule-organizer-email').text(),
            isOrganizer: parent.find('.schedule-isOrganizer').text(),
            id: parent.find('.schedule-event-id').text(),
            webLink: parent.attr('data-mfp-src')
        };

        overlay.on('click', function (event) {
            event.stopPropagation();
            if ($(event.target).hasClass('modal-overlay')) {
                $('.slick-dots').css('z-index', '1');
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            //view details
            if ($(event.target).hasClass('circle-view')) {

                var passedObj = { data: obj, type: 'schedule' };

                loadPopUp(generateScheduleHTML, scheduleButtonHandler, passedObj);



                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut')
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');

                });
            }

            if ($(event.target).hasClass('circle-accept')) {
                //var event_id = parent.find('.schedule-event-id').text();
                acceptEvent(userEmail, obj.id);

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            if ($(event.target).hasClass('circle-reply')) {

                var passedObj = { data: obj, type: 'reply' };

                loadPopUp(generateScheduleHTML, scheduleButtonHandler, passedObj);


                $('.mfp-container .mfp-content').attr('style', 'max-height: 300px;');

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }
        });
    });
}


/*schedule operation*/
function acceptEvent(email, event_id) {
    try {
        $('body').addClass('loading'); //add loading gif


        var accessToken = getAccessToken();

        var endpointUrl = graphEndpoint + "me/calendar/events/" + event_id + "/accept";
            
            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                headers:{
                    'Authorization': 'Bearer ' + accessToken
                },
                success: function (json) {

                    //reload panel
                    loadScheduleFeed(email, true);
                },
                error: function (err) {
                    console.log(err);
                    console.log('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    } catch (err) {
        console.log("Exception: " + err.responseJSON.message);
    }
}

//quick reply schedule
function quickReplySchedule(message, subject, recipient) {
    try {

        $.magnificPopup.close();
        $('body').addClass('loading'); //add loading gif

        var accessToken = getAccessToken();

        var endpointUrl = graphEndpoint + "me/sendMail";

        var passedData = {
            "Message":{
                "Subject": subject,
                "Body":{
                    "ContentType":"Text",
                    "Content": message 
                },
                "ToRecipients": [
                    {
                        "EmailAddress": {
                            "Address": recipient
                        }
                    }
                ]
            },
            "SaveToSentItems": true
        };

            $.ajax({
                url: endpointUrl,
                data:JSON.stringify(passedData),
                method: "POST",
                headers:{
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/json'
                },
                async: true,
                success: function (json) {
                    
                    $('body').removeClass('loading');
                },
                error: function (err) {
                    console.log('error: ' + err.message);
                    $('body').removeClass('loading');
                }
            });
    } catch (err) {
        console.log("Exception: " + err.responseJSON.message);
    }
}
/*end schedule operation*/


/*task check operation*/
function checkTaskFeed(userEmail, lastRun) {
    var endpointUrl = endpoint + "/api/tasks/check?userEmail=" + userEmail + "&lastRun=" + lastRun;

    $.ajax({
        url: endpointUrl,
        method: 'GET',
        async: true,
        success: function (xhr) {
            if (xhr > 0) {
                loadTABMFeed(userEmail);
                loadTATMFeed(userEmail);
                loadProductivityRate(userEmail);
            }

            //update time buffer
            refreshTime = (new Date(Date.now() - ((new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0, -1);

        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
}

function checkTATMFeed(userEmail,lastRun) {
    var endpointUrl = endpoint + "/api/tasks/check/tome?userEmail=" + userEmail + "&lastRun=" + lastRun;

    $.ajax({
        url: endpointUrl,
        method: 'GET',
        async: true,
        success: function (xhr) {
            if (xhr > 0) {
                loadTATMFeed(userEmail);
                loadProductivityRate(userEmail);
            }

        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
}

function checkTABMFeed(userEmail,lastRun) {
  
    var endpointUrl = endpoint + "/api/tasks/check/byme?userEmail=" + userEmail + "&lastRun=" + lastRun;

    $.ajax({
        url: endpointUrl,
        method: 'GET',
        async: true,
        success: function (xhr) {

            if (xhr > 0) {
                loadTABMFeed(userEmail);
                loadProductivityRate(userEmail);
            }

        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
}
/*end task check operation*/

/*get TATM, access token required*/
function loadTATMFeed(userEmail, reload) {
    try {

        if (checkConnectionSettings('task')) {

            var accessToken = getAccessToken();

            var encryption = "";
            if (!checkIsNull(encryptionKey)) {
                encryption = "&key=" + encodeURIComponent(encryptionKey);
            }

            var endpointUrl = endpoint + "/api/tasks/tome?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + encryption;

            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                success: function (json) {
                    var html = "";

                    if (json.StatusCode != 1) {

                        //set tatm checker
                        if (json.lastModifiedTime) {
                            refreshTimeTATM = convertIntoISOString(json.lastModifiedTime);
                        }
                        else {
                            refreshTimeTATM = getUtcISOString();
                        }


                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        var data = json.result;

                        if (data.length > 0) {

                            refreshPanel($("#myTasksDetails"), function () {
                                loadTATMData(data);
                                tatmEventHandler();
                            });

                        }
                        else {
                            $('#myTasksDetails').html('<div class="empty-msg">Seems like you have completed all task!</div>');
                        }
                    }
                        //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials (tenant ID, license key unverified)
                        if (json.error_code == 'EG0004') {
                            $('#myTasksDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
                        }
                            //other error
                        else {
                            $('#myTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
                        }
                        $('body').removeClass('loading');

                    }

                    //reload != undefined
                    if (reload) {
                        $('body').removeClass('loading'); //remove loading gif
                    }

                },
                error: function (err) {
                    console.log('error: ' + err.message);
                    $('#myTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
        }
        //first load, load sample
        else {
            refreshPanel($('#myTasksDetails'), function () {
                noCredentialTATM();
            });
        }
    }
    catch (ex) {

        console.log('error: ' + ex.message);

        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            $('#myTasksDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
        }
        //general handler
        else {
            $('#myTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
        }
    }
}

//load tatm data
function loadTATMData(data) {
    var charLimit = 65;
    var html = '';

    //parse data
    $(data).each(function (index) {

        //current date
        var d = new Date();

        var dueDate = new Date();

        //set due date
        if (!checkIsNull(this.dueDate)) {
            dueDate = new Date(convertIntoISOString(this.dueDate));
            //dueDate = convertUTCToLocale(new Date(this.dueDate));
        }
        else {
            dueDate = new Date(convertIntoISOString(this.originalDueDate));
            //dueDate = convertUTCToLocale(new Date(this.originalDueDate));
        }

        var createdDate = new Date(convertIntoISOString(this.createdDateTime));
        //var createdDate = convertUTCToLocale(new Date(this.createdDateTime));

        //set original due date
        var originalDue = new Date(convertIntoISOString(this.originalDueDate));
        //var originalDue = convertUTCToLocale(new Date(this.originalDueDate));

        var createdDateString = getDateString(createdDate);
        var dueDateString = getDateString(dueDate);
        var originalDueString = getDateString(originalDue);

        var dueDay = parseInt(dueDate.getDate());
        var dueMonth = getMonthString(dueDate.getMonth());

        //compare date diff
        dueDate.setHours(0, 0, 0, 0);
        d.setHours(0, 0, 0, 0);
        var dueDiff = (dueDate.getTime() - d.getTime());
        var due = Math.floor(dueDiff / (1000 * 3600 * 24));

        var status = this.taskStatus;

        var taskName = textAbstract(this.taskName, charLimit).escape();

        var category = "My Task";

        html += '<div class="task-slider-item slider-item">' + '<div class="task-slider-date">';

        if (due == 0) {
            due = "Today";
            html += '<div class="task-slider-date-due today">' + due + '</div>';
        }
        else if (due < 0) {
            due = "Overdue";
            html += '<div class="task-slider-date-due expired">' + due + '</div>';
        }
        else {
            if (due == 1) {
                due = due + " Day";
            }
            else {
                due = due + " Days";
            }

            html += '<div class="task-slider-date-due">' + due + '</div>';
        }

        //in case the task is assigned to yourself
        if (this.assignedToEmailAddress.toLowerCase() == userEmail.toLowerCase()) {
            html += '<div class="task-slider-date-start">' + '<div class="task-slider-date-start-day">' + dueDay + '</div>' + '<div class="task-slider-date-start-month">' + dueMonth + '</div>' + '</div>' + '</div>';
            html += '<div class="task-slider-content">' + '<div class="task-slider-content-category">' + category + '</div>' + '<div class="task-slider-content-subject">' + taskName + '</div>' + '</div>';

            //task status button
            if (this.channel.toLowerCase() != "form") {
                if (status == 'Delayed') {
                    html += '<div class="task-slider-status-button delayed">' + status + '</div>';
                }
                else {
                    html += '<div class="task-slider-status-button">' + status + '</div>';
                }
            }
            else if (this.channel.toLowerCase() == "form") {
                html += '<div class="task-slider-status-button">' + this.channel + '</div>';
            }
        }

        var description = '';
        if (this.description) {
            description = this.description.escape();
        }

        //hidden popup Data
        html += '<div class="popUpData" style="display:none">' + '<div class="description">' + description + '</div>' + '<div class="assignedBy">' + this.assignedBy + '</div>' + '<div class="assignedTo">' + this.assignedTo + '</div>' + '<div class="assignedByEmail">' + this.assignedByEmailAddress + '</div>' + '<div class="assignedToEmail">' + this.assignedToEmailAddress + '</div>' + '<div class="taskId">' + this.taskID + '</div>' + '<div class="createdDate">' + createdDateString + '</div>' + '<div class="dueDate">' + dueDateString + '</div>'
            /*
            +		'<div class="reminderDate">'
            +		reminderDateString
            +		'</div>'
            */
            + '<div class="status">' + status + '</div>'
            + '<div class="flexible">' + this.enablePlusDay + '</div>' + '<div class="messageId">' + this.yammerMessageID + '</div>' + '<div class="threadId">' + this.yammerThreadID + '</div>' + '<div class="plusDay">' + this.plusDay + '</div>' + '<div class="channel">' + this.channel + '</div>' + '<div class="originalDueDate">' + originalDueString + '</div>' + '</div>' + '</div>';

    });

    var htmlElement = $(html);
    $("#myTasksDetails").html(htmlElement);
    $("#myTasksDetails").slick({
        slide: '.task-slider-item',
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        draggable: false, //disable mouse dragging
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 769,
            settings: {
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });

    $("#myTasksDetails").on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        //remove all other overlay							    
        var others = $('#myTasksDetails .modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');

                $(this).siblings('.circle').addClass('animated slideOutUp');

                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.task-slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }

    });
}

function noCredentialTATM() {
    var tdyDate = new Date();
    var ytdDate = new Date(tdyDate.setDate(tdyDate.getDate() - 1));
    var tmrDate = new Date(tdyDate.setDate(tdyDate.getDate() + 1));

    var sampleData = [
        {
            "taskID": 0,
            "taskName": "Expired Task",
            "description": "Sample Expired Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": ytdDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": "sample@celgo.net",
            "assignedTo": "John Doe",
            "assignedToEmailAddress": userEmail,
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        },
        {
            "taskID": 0,
            "taskName": "Today Task",
            "description": "Sample Today Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": tdyDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": "sample@celgo.net",
            "assignedTo": "John Doe",
            "assignedToEmailAddress": userEmail,
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        },
        {
            "taskID": 0,
            "taskName": "Tomorrow Task",
            "description": "Sample Tomorrow Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": tmrDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": "sample@celgo.net",
            "assignedTo": "John Doe",
            "assignedToEmailAddress": userEmail,
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        }
    ];

    loadTATMData(sampleData);

    $("#myTasksDetails .slick-track").css('opacity', '0.7');

    //add overlay with 2 buttons
    $("#myTasksDetails").append("<div class='panel-modal-firstload'></div>");
    $("#myTasksDetails").append("<div class='panel-button-container'><div class='configure panel-button popup-button'>Activate</div><div class='minimize panel-button popup-button'>Minimize</div></div>");

    //set button handler

    $("#myTasksDetails .configure.panel-button").on('click', function () {
        loadDetailedConfig('task');
    });

    function maximizePanel() {
        $('#myTasksPanel').removeClass('minimized').animate({ height: 130 }, 400);
        $('#myTasksPanel').off('click', maximizePanel);

        $('#myTasksHeading').on('click', function () {
            $('#myTasksDetails').html('<div class="panel-loading"></div>');
            loadTATMFeed(userEmail);
        });
    }

    $("#myTasksDetails .minimize.panel-button").on('click', function () {
        var panel = $(this).closest('.panel');

        //remove click handler
        $('#myTasksHeading').off('click');

        panel.animate({ height: 30 }, 400, 'swing', function () {
            panel.addClass('minimized');
            panel.on('click', maximizePanel);
        });
    });
}

function tatmEventHandler() {
    $('#myTasksDetails .task-slider-item').on('click', function (event) {

        var slickParent = $(event.target).closest('.slick-slider');

        //remove all other overlay
        var others = $('.modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-slider').not(slickParent).find('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }

        //default				    
        var overlay = $('<div class="modal-overlay"></div><div class="circle circle-dismiss left">DISMISS</div><div class="circle circle-view center">VIEW</div><div class="circle circle-complete right">COMPLETE</div>');

        var parent = $(event.target).closest('.slider-item');
        var enablePlusDay = parseInt(parent.find('.enablePlusDay').text());
        var slideIndex = parent.index();


        overlay.addClass('animated fadeIn');

        $(this).closest('.slick-slider').find('.slick-dots').css('z-index', '-1');

        parent.css('cursor', 'auto');
        parent.append(overlay);
        $('.circle').addClass('animated slideInUp');

        var obj = {
            subject: parent.find('.task-slider-content-subject').html(),
            status: parent.find('.status').text(),
            assignedBy: parent.find('.assignedBy').text(),
            assignedTo: parent.find('.assignedTo').text(),
            assignedByEmail: parent.find('.assignedByEmail').text(),
            assignedToEmail: parent.find('.assignedToEmail').text(),
            enablePlusDay: enablePlusDay,
            createdDate: parent.find('.createdDate').text(),
            dueDate: parent.find('.dueDate').text(),
            description: parent.find('.description').html(),
            taskId: parseInt(parent.find('.taskId').text()),
            plusDay: parent.find('.plusDay').text(),
            reminderDate: parent.find('.reminderDate').text(),
            messageId: parent.find('.messageId').text(),
            threadId: parent.find('.threadId').text(),
            category: parent.find('.task-slider-content-category').text(),
            channel: parent.find('.channel').text(),
            originalDueDate: parent.find('.originalDueDate').text()
        };

        overlay.on('click', function (event) {
            event.stopPropagation();
            if ($(event.target).hasClass('modal-overlay')) {
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            if ($(event.target).hasClass('circle-view')) {

                var passedObj = { data: obj, type: 'tatm' };

                loadPopUp(generateTaskPopUpHTML, taskButtonHandler, passedObj)


                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut')
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            if ($(event.target).hasClass('circle-dismiss')) {

                $('body').addClass('loading');
                parent.addClass('animated slideOutUp');
                parent.one('animationend', function () {
                    dismissTask(userEmail, obj.taskId, slideIndex, parent, 'TATM');
                });

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }


            if ($(event.target).hasClass('circle-complete')) {
                $('body').addClass('loading');
                parent.addClass('animated slideOutUp');
                parent.one('animationend', function () {

                    //completeTask(obj.assignedToEmail, obj.timestamp, slideIndex, parent, $("#myTasksDetails"));
                    completeTask(userEmail, obj.taskId, slideIndex, parent, "TATM");
                });

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

        });
    });
}

/*get TABM, access token required*/
function loadTABMFeed(userEmail, reload) {
    try{

        if (checkConnectionSettings('task')) {

            var accessToken = getAccessToken();

            var encryption = "";
            if (!checkIsNull(encryptionKey)) {
                encryption = "&key=" + encodeURIComponent(encryptionKey);
            }

            var endpointUrl = endpoint + "/api/tasks/byme?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + encryption;

            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //set taBm checker
                        if (json.lastModifiedTime) {
                            refreshTimeTABM = convertIntoISOString(json.lastModifiedTime);
                        }
                        else {
                            refreshTimeTABM = getUtcISOString();
                        }

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        var data = json.result;

                        if (data.length > 0) {

                            refreshPanel($("#assigneeTasksDetails"), function () {
                                loadTABMData(data);
                                tabmEventHandler();
                            });

                        } else {
                            $('#assigneeTasksDetails').html('<div class="empty-msg">Seems like you do not have any pending task!</div>');
                        }
                    }
                        //web service exception
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials (tenant ID, license key unverified)
                        if (json.error_code == 'EG0004') {
                            $('#assigneeTasksDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
                        }
                            //general error
                        else {
                            $('#assigneeTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
                        }
                        $('body').removeClass('loading');
                    }

                    if (reload) {
                        $('body').removeClass('loading'); //remove loading gif
                    }

                },
                error: function (err) {
                    console.log('error: ' + err.message);
                    $('#assigneeTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
        }
        else {
            refreshPanel($("#assigneeTasksDetails"), function () {
                noCredentialTABM();
            });
        }
    }
    catch (ex) {
        console.log('error: ' + ex.message);
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            $('#assigneeTasksDetails').html('<div class="empty-msg">You are not authorized to access this feature.</div>');
        }
        //general handler
        else {
            $('#assigneeTasksDetails').html('<div class="empty-msg">Data request failed.</div>');
        }
    }
}

function loadTABMData(data) {

    var html = "";
    var charLimit = 65;

    //parse data
    $(data).each(function (index) {

        //current date
        var d = new Date();

        var dueDate = new Date();

        //set due date
        if (!checkIsNull(this.dueDate)) {
            dueDate = new Date(convertIntoISOString(this.dueDate));
        }
        else {
            dueDate = new Date(convertIntoISOString(this.originalDueDate));
        }

        var createdDate = new Date(convertIntoISOString(this.createdDateTime));

        //set original due date
        var originalDue = new Date(convertIntoISOString(this.originalDueDate));

        var createdDateString = getDateString(createdDate);
        var dueDateString = getDateString(dueDate);
        var originalDueString = getDateString(originalDue);

        var dueDay = parseInt(dueDate.getDate());
        var dueMonth = getMonthString(dueDate.getMonth());

        //compare date diff
        dueDate.setHours(0, 0, 0, 0);
        d.setHours(0, 0, 0, 0);
        var dueDiff = (dueDate.getTime() - d.getTime());
        var due = Math.floor(dueDiff / (1000 * 3600 * 24));

        var status = this.taskStatus;

        var taskName = textAbstract(this.taskName, charLimit).escape();

        html += '<div class="task-slider-item slider-item">' + '<div class="task-slider-date">';

        if (due == 0) {
            due = "Today";
            html += '<div class="task-slider-date-due today">' + due + '</div>';
        } else if (due < 0) {
            due = "Overdue";
            html += '<div class="task-slider-date-due expired">' + due + '</div>';
        } else {
            if (due == 1) {
                due = due + " Day";
            } else {
                due = due + " Days";
            }

            html += '<div class="task-slider-date-due">' + due + '</div>';
        }

        if (this.assignedByEmailAddress.toLowerCase() == userEmail.toLowerCase()) {

            var category = "Task for " + this.assignedTo;
            html += '<div class="task-slider-date-start">' + '<div class="task-slider-date-start-day">' + dueDay + '</div>' + '<div class="task-slider-date-start-month">' + dueMonth + '</div>' + '</div>' + '</div>' + '<div class="task-slider-content">' + '<div class="task-slider-content-category">' + category + '</div>' + '<div class="task-slider-content-subject">' + taskName + '</div>' + '</div>';

            if (status == 'Completed') {
                html += '<div class="task-slider-status-button completed">' + status + '</div>';
            }
            else if (status == 'Delayed') {
                html += '<div class="task-slider-status-button delayed">' + status + '</div>';
            }
            else {
                html += '<div class="task-slider-status-button">' + status + '</div>';
            }
        }

        var description = '';
        if (this.description) {
            description = this.description.escape();
        }

        //popup Data
        html += '<div class="popUpData" style="display:none">' + '<div class="description">' + description + '</div>' + '<div class="assignedBy">' + this.assignedBy + '</div>' + '<div class="assignedTo">' + this.assignedTo + '</div>' + '<div class="assignedByEmail">' + this.assignedByEmailAddress + '</div>' + '<div class="assignedToEmail">' + this.assignedToEmailAddress + '</div>' + '<div class="taskId">' + this.taskID + '</div>' + '<div class="createdDate">' + createdDateString + '</div>' + '<div class="dueDate">' + dueDateString + '</div>'
            /*
            +		'<div class="reminderDate">'
            +		reminderDateString
            +		'</div>'
            */
            + '<div class="status">' + status + '</div>'
            + '<div class="enablePlusDay">' + this.enablePlusDay + '</div>' + '<div class="messageId">' + this.yammerMessageID + '</div>' + '<div class="threadId">' + this.yammerThreadID + '</div>' + '<div class="plusDay">' + this.plusDay + '</div>' + '<div class="channel">' + this.channel + '</div>' + '<div class="originalDueDate">' + originalDueString + '</div>' + '</div>' + '</div>';
    });

    var htmlElement = $(html);
    $("#assigneeTasksDetails").append(htmlElement);

    $("#assigneeTasksDetails").slick({
        slide: '.task-slider-item',
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 769,
            settings: {
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]
    });


    $("#assigneeTasksDetails").on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        //remove all other overlay							    
        var others = $('#assigneeTasksDetails .modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');

                $(this).siblings('.circle').addClass('animated slideOutUp'); //fadeOut

                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.task-slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }

    });
}

function noCredentialTABM() {
    var tdyDate = new Date();
    var ytdDate = new Date(tdyDate.setDate(tdyDate.getDate() - 1));
    var tmrDate = new Date(tdyDate.setDate(tdyDate.getDate() + 1));

    var sampleData = [{
            "taskID": 0,
            "taskName": "Expired Task",
            "description": "Sample Expired Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": ytdDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": userEmail,
            "assignedTo": "John Doe",
            "assignedToEmailAddress": "sample@celgo.net",
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        },
        {
            "taskID": 0,
            "taskName": "Today Task",
            "description": "Sample Today Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": tdyDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": userEmail,
            "assignedTo": "Jane Doe",
            "assignedToEmailAddress": "sample@celgo.net",
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        },
        {
            "taskID": 0,
            "taskName": "Tomorrow Task",
            "description": "Sample Tomorrow Task",
            "dueDate": null,
            "taskStatusID": 1,
            "originalDueDate": tmrDate.toISOString(),
            "taskStatus": "In Progress",
            "createdChannelID": 1,
            "channel": "Email",
            "assignedBy": "sample",
            "assignedByEmailAddress": userEmail,
            "assignedTo": "Bob Ross",
            "assignedToEmailAddress": "sample@celgo.net",
            "assignmentTimestamp": "58AE4078-DFD8-4847-9E77-7F852644CECF",
            "createdDateTime": "2016-05-19T01:49:58.953",
            "modifiedDateTime": "2016-05-19T01:49:58.953",
            "enablePlusDay": true,
            "plusDay": 0,
            "ReminderDate": "2016-05-23T00:00:00",
            "yammerMessageID": "7123",
            "yammerThreadID": "7123",
            "encrypted": false,
            "remark": null,
            "sorting": 0,
            "enable": true,
            "createdBy": null,
            "modifiedBy": null,
            "tenantID": "celgo.net",
            "assignedToCompletedDateTime": null
        }];

    loadTABMData(sampleData);

    $("#assigneeTasksDetails .slick-track").css('opacity', '0.7');

    //add overlay with 2 buttons
    $("#assigneeTasksDetails").append("<div class='panel-modal-firstload'></div>");
    $("#assigneeTasksDetails").append("<div class='panel-button-container'><div class='configure panel-button popup-button'>Activate</div><div class='minimize panel-button popup-button'>Minimize</div></div>");

    //set button handler

    $("#assigneeTasksDetails .configure.panel-button").on('click', function () {
        loadDetailedConfig('task');
    });

    function maximizePanel() {
        $('#assigneeTasksPanel').removeClass('minimized').animate({ height: 130 }, 400);
        $('#assigneeTasksPanel').off('click', maximizePanel);

        $('#assigneeTasksHeading').on('click', function () {
            $('#assigneeTasksDetails').html('<div class="panel-loading"></div>');
            loadTABMFeed(userEmail);
        });
    }

    $("#assigneeTasksDetails .minimize.panel-button").on('click', function () {
        var panel = $(this).closest('.panel');

        //remove click handler
        $('#assigneeTasksHeading').off('click');

        panel.animate({ height: 30 }, 400, 'swing', function () {
            panel.addClass('minimized');
            panel.on('click', maximizePanel);
        });
    });
}

function tabmEventHandler() {
    $('#assigneeTasksDetails .task-slider-item').on('click', function (event) {

        var slickParent = $(event.target).closest('.slick-slider');

        //remove all other overlay
        var others = $('.modal-overlay');
        if (others.length > 0) {
            others.each(function (event) {
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-slider').not(slickParent).find('.slick-dots').css('z-index', '1');
                    $(this).siblings('.circle').remove();
                    $(this).closest('.slider-item').css('cursor', 'pointer');
                    $(this).remove();

                });
            });
        }

        //default				    
        var overlay = $('<div class="modal-overlay"></div><div class="circle circle-dismiss left two">DISMISS</div><div class="circle circle-view right two">VIEW</div>');

        var parent = $(event.target).closest('.slider-item');
        var enablePlusDay = parseInt(parent.find('.enablePlusDay').text());

        var slideIndex = parent.index();

        if (parent.find('.status').text() == "Completed") {
            overlay = $('<div class="modal-overlay"></div><div class="circle circle-return left">RETURN</div><div class="circle circle-view center">VIEW</div><div class="circle circle-confirm right">CONFIRM</div>');
        }


        overlay.addClass('animated fadeIn');

        $(this).closest('.slick-slider').find('.slick-dots').css('z-index', '-1');

        parent.css('cursor', 'auto');
        parent.append(overlay);
        $('.circle').addClass('animated slideInUp');

        var obj = {
            subject: parent.find('.task-slider-content-subject').html(),
            status: parent.find('.status').text(),
            assignedBy: parent.find('.assignedBy').text(),
            assignedTo: parent.find('.assignedTo').text(),
            assignedByEmail: parent.find('.assignedByEmail').text(),
            assignedToEmail: parent.find('.assignedToEmail').text(),
            enablePlusDay: enablePlusDay,
            createdDate: parent.find('.createdDate').text(),
            dueDate: parent.find('.dueDate').text(),
            description: parent.find('.description').html(),
            taskId: parseInt(parent.find('.taskId').text()),
            plusDay: parent.find('.plusDay').text(),
            reminderDate: parent.find('.reminderDate').text(),
            messageId: parent.find('.messageId').text(),
            threadId: parent.find('.threadId').text(),
            category: parent.find('.task-slider-content-category').text(),
            channel: parent.find('.channel').text(),
            originalDueDate: parent.find('.originalDueDate').text()
        };

        overlay.on('click', function (event) {
            event.stopPropagation();
            if ($(event.target).hasClass('modal-overlay')) {
                $(this).addClass('animated fadeOut');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            if ($(event.target).hasClass('circle-view')) {
                var passedObj = { data: obj, type: 'tabm' };
                //load popup
                loadPopUp(generateTaskPopUpHTML, taskButtonHandler, passedObj);

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut')
                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

            if ($(event.target).hasClass('circle-return')) {

                $('body').addClass('loading');
                parent.addClass('animated slideOutUp');
                parent.one('animationend', function () {
                    returnTask(userEmail, obj.taskId, slideIndex, parent, 'TABM');
                });

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }


            if ($(event.target).hasClass('circle-dismiss')) {

                $('body').addClass('loading');
                parent.addClass('animated slideOutUp');
                parent.one('animationend', function () {
                    dismissTask(userEmail, obj.taskId, slideIndex, parent, 'TABM');
                });

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }


            if ($(event.target).hasClass('circle-confirm')) {
                $('body').addClass('loading');
                parent.addClass('animated slideOutUp');
                parent.one('animationend', function () {

                    confirmTask(userEmail, obj.taskId, slideIndex, parent, "TABM");
                });

                $(this).addClass('animated slideOutUp');
                $(this).siblings('.circle').addClass('animated slideOutUp');
                $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $('.slick-dots').css('z-index', '1');
                    overlay.remove();
                    parent.css('cursor', 'pointer');
                });
            }

        });
    });
}

/*task operation, access token required*/
function completeTask(userEmail, taskId, slideIndex, element, type) {
    try {

        var accessToken = getAccessToken();
        
        var endpointUrl = endpoint + "/api/tasks/op/complete?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskId;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type == "TABM") {
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                        }

                        loadProductivityRate(userEmail);
                        $('body').removeClass('loading'); //remove loading gif
                    }
                    //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials (tenant ID, license key unverified)
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }

                        $('body').removeClass('loading');
                    }

                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}

function confirmTask(userEmail, taskId, slideIndex, element, type) {
    try {
        var accessToken = getAccessToken();
        
        var endpointUrl = endpoint + "/api/tasks/op/confirm?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskId;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type == "TABM") {
                            $('#assigneeTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                        }
                        $('body').removeClass('loading'); //remove loading gif
                    }
                    //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }

                        $('body').removeClass('loading');
                    }
                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}

function returnTask(userEmail, taskId, slideIndex, element, type) {
    try {

        var accessToken = getAccessToken();

        var endpointUrl = endpoint + "/api/tasks/op/return?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskId;
            
            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type == "TABM") {
                            $('#assigneeTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                        }
                        $('body').removeClass('loading'); //remove loading gif
                    }
                        //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                            //no credentials
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }

                        $('body').removeClass('loading');
                    }

                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading');
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}

function dismissTask(userEmail, taskId, slideIndex, element, type) {
    try {

        var accessToken = getAccessToken();

        var endpointUrl = endpoint + "/api/tasks/op/dismiss?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskId;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type == "TABM") {
                            $('#assigneeTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('slickRemove', slideIndex);
                            element.remove();
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                            loadProductivityRate(userEmail);
                        }

                        $('body').removeClass('loading'); //remove loading gif

                    }
                    //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                            //no credentials
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }

                        $('body').removeClass('loading');
                    }
                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}


function addOneDayTask(userEmail, taskId, type) {
    try {

        var accessToken = getAccessToken();

            var encryption = "";
            if (!checkIsNull(encryptionKey)) {
                encryption = "&key=" + encodeURIComponent(encryptionKey);
            }

            var endpointUrl = endpoint + "/api/tasks/op/plus?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskId + encryption;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {
 
                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type == "TABM") {
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                        }

                        var newData = json.result[0];

                        var createdDate = new Date(convertIntoISOString(newData.createdDateTime));
                        var createdDateString = getDateString(createdDate);

                        var dueDate = new Date(convertIntoISOString(newData.dueDate));
                        var dueDateString = getDateString(dueDate);

                        var originalDue = new Date(convertIntoISOString(newData.originalDueDate));
                        var originalDueString = getDateString(originalDue);

                        var subject = newData.taskName;
                        if (!checkIsNull(subject)) {
                            subject = subject.escape();
                        }

                        var desc = newData.description;
                        if (!checkIsNull(desc)) {
                            desc = desc.escape();
                        }


                        //repop object with new data
                        var obj = {
                            subject: subject,
                            status: newData.taskStatus,
                            assignedBy: newData.assignedBy,
                            assignedTo: newData.assignedTo,
                            assignedByEmail: newData.assignedByEmailAddress,
                            assignedToEmail: newData.assignedToEmailAddress,
                            enablePlusDay: parseInt(newData.enablePlusDay),
                            createdDate: createdDateString,
                            dueDate: dueDateString,
                            description: desc,
                            taskId: newData.taskID,
                            plusDay: newData.plusDay,
                            reminderDate: newData.reminderDate,
                            messageId: newData.yammerMessageID,
                            threadId: newData.yammerThreadID,
                            category: 'My Task',
                            channel: newData.channel,
                            originalDueDate: originalDueString
                        };

                        var passedObj = { data: obj, type: 'tatm' };

                        //reload popup
                        loadPopUp(generateTaskPopUpHTML, taskButtonHandler, passedObj, true)

                        //reload rate
                        loadProductivityRate(userEmail);

                    }
                    //web service exception
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }
                        $('body').removeClass('loading');
                    }
                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading');
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}

//create task
function createTask(obj, refreshPointer, checker, num) {
    try {
        $('body').addClass('loading'); //add loading gif

        var accessToken = getAccessToken();

            var encryption = "";
            if (!checkIsNull(encryptionKey)) {
                encryption = "&key=" + encodeURIComponent(encryptionKey);
            }

            var endpointUrl = endpoint + "/api/tasks/op/create?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + encryption;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                data: obj,
                success: function (json) {
                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        console.log('created');
                        checker.push('done');
                        if (checker.length == num) {
                            //refresh
                            refreshPointer.forEach(function (a) {
                                if (a == 'tatm') {
                                    loadTATMFeed(userEmail, true);
                                }

                                if (a == 'tabm') {
                                    loadTABMFeed(userEmail, true);
                                }

                                loadProductivityRate(userEmail);
                            });
                        }
                    }
                    //exception error in web service
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);

                        //no credentials
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }
                    }

                    $('body').removeClass('loading'); //remove loading gif
                },
                error: function (err) {
                    console.log('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    } catch (err) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}
/*end task operation*/


/*helper function for slider*/
//el,parent jquery element
function removeOtherOverlays(el,sliderParent) {
    var others = $('.modal-overlay');
    if (others.length > 0) {
        others.each(function (event) {
            el.addClass('animated fadeOut');
            el.siblings('.circle').addClass('animated slideOutUp');
            el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $('.slick-slider').not(sliderParent).find('.slick-dots').css('z-index', '1');
                el.siblings('.circle').remove();
                el.closest('.slider-item').css('cursor', 'pointer');
                el.remove();
            });
        });
    }
}

//e --> event
function removeCurrentOverlay(el, e, parent){
    e.stopPropagation();
    if ($(e.target).hasClass('modal-overlay')) {
        el.addClass('animated fadeOut');
        el.siblings('.circle').addClass('animated slideOutUp');
        el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $('.slick-dots').css('z-index', '1');
            el.remove();
            parent.css('cursor', 'pointer');
        });
    }
}

function overlayButtonClicked(el,overlay,parent){
    el.addClass('animated slideOutUp');
    el.siblings('.circle').addClass('animated slideOutUp');
    el.siblings('.modal-overlay').addClass('animated fadeOut')
    overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $('.slick-dots').css('z-index', '1');
        overlay.remove();
        parent.css('cursor', 'pointer');
    });
}
/*end helper function*/


function globalOverlayHandler() {
    //handler to close item overlay when clicked elsewhere out of the slider
    $('body').on('click', function (event) {
        if (!($(event.target).closest('.slider-item').length > 0)) {

            //remove all other overlay
            var others = $('.modal-overlay');
            if (others.length > 0) {
                others.each(function (event) {
                    $(this).addClass('animated fadeOut');

                    $(this).siblings('.circle').addClass('animated slideOutUp'); //fadeOut

                    $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                        $('.slick-dots').css('z-index', '1');
                        $(this).siblings('.circle').remove();
                        $(this).closest('.slider-item').css('cursor', 'pointer');
                        $(this).remove();

                    });
                });
            }
        }

        //add custom click to close popup, will close the popup
        //added handler to skip datepicker elements
        if (!($(event.target).closest('.mfp-content').length > 0) && !($(event.target).closest('#ui-datepicker-div').length > 0) && !($(event.target).closest('.ui-datepicker-next').length > 0) && !($(event.target).closest('.ui-datepicker-prev').length > 0)) {
            if ($.magnificPopup.instance.isOpen) {
                $.magnificPopup.close();
            }
        }

    });
}

//feed --> feedObj
function loadRSSFeed(feed) {
    var feedUrl = "";
    var defaultFeed = getDefaultRSSFeed(rssFeedUrl);

    if (typeof feed === 'object') {
        //default feed if no user preference found
        if (!feed.url) {
            if (defaultFeed) {
                feedUrl = defaultFeed.url;
            }
        }
        else {
            feedUrl = feed.url;
        }
    }
    else {
        //user preference not found

        //try to get default feed
        if (defaultFeed) {
            feedUrl = defaultFeed.url;
        }
    }

    if(!checkIsNull(feedUrl)){
        $.ajax({
            type: "GET",
            url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(feedUrl),
            dataType: 'json',
            error: function () {
                $("#myRSSDetails").html("<div class='empty-msg'>Unable to load news source. Incorrect path or invalid news source.</div>");
            },
            success: function (xml) {
                var template = "<div class='rssmsg slider-item' data-mfp-src='{0}'><div class='rssmsg-thumbnail'><img src='{1}' /></div><div class='rssmsg-title'>{3}<div class='rssmsg-body'>{5}</div></div></div>";
                //check
                if (xml.responseData.feed && xml.responseData.feed.entries) {
                    var outputHTML = '';
                    $("#myRSSDetails").empty();
                    $("#myRSSDetails").removeClass('animated custom fadeInFull');
                    $('#myRSSDetails').addClass('animated custom fadeOutFull');

                    //add fade in animation
                    $('#myRSSDetails').one('animationend', function () {

                        if ($("#myRSSDetails").hasClass('slick-slider')) {
                            $("#myRSSDetails").slick('unslick');
                        }
                        $("#myRSSDetails").removeClass('animated custom fadeOutFull');
                        $("#myRSSDetails").html('');
                        $("#myRSSDetails").addClass('animated custom fadeInFull');


                        values = xml.responseData.feed.entries;


                        var feedLimit = 10;
                        var feedCount = 0;
                        //var charLimit = 47;
                        var titleLimit = 30;

                        for (var f in values) {
                            var title = values[f]["title"];
                            var publishedDate = values[f]["publishedDate"];
                            var myPDate = new Date(publishedDate);
                            var contentSnippet = values[f]["contentSnippet"];
                            var content = values[f]["content"];
                            var link = values[f]["link"];
                            var thumbnail;
                            try {
                                thumbnail = values[f]["mediaGroups"][0]["contents"][0]["thumbnails"][0]["url"];
                            } catch (err) {
                                //default
                                thumbnail = "../Content/img/Rss-icon.png";
                            }

                        
                            var titleLim = titleLimit;

                            if (checkIsNull(contentSnippet)) {
                                titleLim = 100;
                            }
                        

                            if (contentSnippet.startsWith("&nbsp;")) {
                                contentSnippet = contentSnippet.substring(6);
                            }

                            /*
                            if (contentSnippet && contentSnippet.length >= (charLimit + 3)) {
                                contentSnippet = textAbstract(contentSnippet, charLimit).escape();
                            }
                            */

                        
                            if (title && title.length >= (titleLim + 3)) {
                                title = textAbstract(title, titleLim).escape();
                            }
                        

                            var temp = template;
                            temp = temp.replace("{0}", link);
                            temp = temp.replace("{1}", thumbnail);
                            temp = temp.replace("{3}", title);
                            temp = temp.replace("{5}", contentSnippet);
                            var tempElement = $(temp);
                            $("#myRSSDetails").append(tempElement);

                            feedCount++;

                            if (feedLimit <= feedCount) {
                                break;
                            }
                        }

                        $("#myRSSDetails").slick({
                            dots: true,
                            arrows: false,
                            infinite: false,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            responsive: [{
                                breakpoint: 1023,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2
                                }
                            }, {
                                breakpoint: 769,
                                settings: {
                                    dots: false,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            }]
                        });

                        //truncate dotdotdot
                        $('.rssmsg-title').dotdotdot({
                            watch: 'window'
                        });

                        $('.rssmsg.slider-item').on('click', function (event) {
                            event.preventDefault();

                            var href = $(this).attr('data-mfp-src');
                            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                            var left = ((width / 2) - (750 / 2)) + dualScreenLeft;
                            var top = ((height / 2) - (400 / 2)) + dualScreenTop;
                            var newWindow = window.open(href, 'Outlook', 'scrollbars=yes, width=750, height=400, top=' + top + ', left=' + left);

                            if (window.focus) {
                                newWindow.focus();
                            }
                        });
                    });
                }
                else {
                    $("#myRSSDetails").html("<div class='empty-msg'>Unable to load news source. Incorrect path or invalid news source.</div>");
                }
            }
        });
    }
    else{
        $("#myRSSDetails").html("<div class='empty-msg'>Unable to load news source. Incorrect path or invalid news source.</div>");
    }
}

//link to yammer inbox on yammer panel click
function loadYammerInbox(token) {
    try {
        //var token = getCookie("yammer_access_token");
        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/users/current.json",
            method: "GET",
            success: function (obj) {

                var network = obj['network_name'];
                $('#myYammerHeading').attr('href', 'https://www.yammer.com/' + network + '/#/inbox/index');
            },
            error: function (msg) {
                console.log("Error");
            }
        });
    } catch (err) {
        console.log("Exception: " + err.message);
    }
}

function loadYammerMessages(token, reload) {
    //var charLimit = 65; //107
    //var charLimitSecond = 30;
    //var charLimit1024 = 50;

    try {
        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/messages/inbox.json",
            method: "GET",
            data: {
                filter: 'inbox_unseen',
                threaded: 'extended',
                exclude_own_messages_from_unseen: 'true'
            },
            success: function (msg) {
                //<div class='yammsg-body-1024'>{extra}</div></div></div>
                var template = "<div class='yammsg slider-item' data-mfp-src='{6}'><div class='yammsg-first-message'><div class='yammsg-thumbnail'><img src='{1}' /></div><div class='yammsg-content'><div class='yammsg-sender'>{3}</div><div class='yammsg-body {single}'>{5}</div></div></div>" + "<div class='yammsg-second-message'><i class='fa fa-comment' aria-hidden='true'></i><div class='yammsg-content'><div class='yammsg-sender'>{10}</div><div class='yammsg-body'>{11}</div></div></div>" + "<div class='yammer-data' style='display:none'><div class='yammer-group-name'>{13}</div><div class='yammer-message-time'>{14}</div><div class='yammer-group-id'>{7}</div><div class='yammer-thread-id'>{8}</div><div class='yammer-topic-id'>{9}</div><div class='yammer-message-id'>{12}</div><div class='yammer-last-message-id'>{15}</div></div>";

                if (!SP.ScriptHelpers.isNullOrUndefined(msg)) {
                    var m;
                    var messages = [];


                    if (msg.messages.length > 0) {
                        //added
                        $("#myYammerDetails").removeClass('animated custom fadeInFull');
                        $('#myYammerDetails').addClass('animated custom fadeOutFull');

                        $('#myYammerDetails').one('animationend', function () {
                            //$('.mfp-content').html(popUpHTML);
                            if ($("#myYammerDetails").hasClass('slick-slider')) {
                                $("#myYammerDetails").slick('unslick');
                            }
                            $("#myYammerDetails").removeClass('animated custom fadeOutFull');
                            $("#myYammerDetails").html('');
                            $("#myYammerDetails").addClass('animated custom fadeInFull');

                            //console.log(msg);
                            for (m in msg.messages) {

                                messages.push({
                                    sender_id: msg.messages[m].sender_id,
                                    body: msg.messages[m].body.plain,
                                    web_url: msg.messages[m].web_url,
                                    sender_thumbnail: "",
                                    sender_displayname: "",
                                    sender_profileURL: "",
                                    group_id: msg.messages[m].group_id,
                                    topic_id: msg.messages[m].topic_id,
                                    thread_id: msg.messages[m].thread_id,
                                    message_id: msg.messages[m].id,
                                    time: msg.messages[m].created_at
                                });
                            }

                            var users = [];
                            var groups = [];
                            for (var r in msg.references) {
                                if (msg.references[r].type === "user") {
                                    users.push({
                                        user_id: msg.references[r].id,
                                        user_thumbnail: msg.references[r].mugshot_url,
                                        user_displayname: msg.references[r].full_name,
                                        user_profileURL: msg.references[r].web_url
                                    });
                                }
                                if (msg.references[r].type === "group") {
                                    groups.push({
                                        group_id: msg.references[r].id,
                                        group_name: msg.references[r].full_name
                                    });
                                }
                            }

                            var outputHTML = "";

                            var countMessages = 0;
                            for (var m2 in messages) {
                                for (var u2 in users) {
                                    if (messages[m2].sender_id == users[u2].user_id) {
                                        messages[m2].sender_thumbnail = users[u2].user_thumbnail;
                                        messages[m2].sender_displayname = users[u2].user_displayname;
                                        messages[m2].sender_profileURL = users[u2].user_profileURL;
                                        break;
                                    }
                                }

                                messages[m2].group_name = 'Private Message';    //default
                                for (var g in groups) {
                                    if (messages[m2].group_id == groups[g].group_id) {
                                        messages[m2].group_name = groups[g].group_name;
                                        break;
                                    }
                                }

                                /*
                                //if no reply add the char limit
                                if ((msg.threaded_extended[messages[m2].thread_id][0] == undefined) || (msg.threaded_extended[messages[m2].thread_id][0] == null) || (msg.threaded_extended[messages[m2].thread_id][0] == '')) {
                                    charLimit = 107;
                                    charLimit1024 = 107;
                                } else {
                                    charLimit = 65;
                                    charLimit1024 = 55;
                                }
                                */

                                var singleMsg = '';

                                if ((msg.threaded_extended[messages[m2].thread_id][0] == undefined) || (msg.threaded_extended[messages[m2].thread_id][0] == null) || (msg.threaded_extended[messages[m2].thread_id][0] == '')) {
                                    singleMsg = 'single';
                                }


                                /*
                                var firstMessage1024 = messages[m2].body;

                                if (messages[m2].body && messages[m2].body.length >= (charLimit + 3)) {
                                    messages[m2].body = textAbstract(messages[m2].body, charLimit).escape();
                                }

                                if (messages[m2].body && firstMessage1024.length >= (charLimit1024 + 3)) {
                                    firstMessage1024 = textAbstract(firstMessage1024, charLimit1024).escape();
                                }
                                */

                                var d = new Date(messages[m2].time);
                                var timeString = getDateString(d, true);
                                
                               

                                var lastMessage = msg.threaded_extended[messages[m2].thread_id][0];

                                var lastMessageId = messages[m2].message_id;

                                var replyUser = '';
                                var replyMessage = '';
                                

                                if (lastMessage != undefined || lastMessage != null) {
                                    replyMessage = lastMessage.body.plain;
                                    lastMessageId = lastMessage.id;
                                    for (u2 in users) {
                                        if (lastMessage.sender_id == users[u2].user_id) {
                                            replyUser = users[u2].user_displayname;
                                            break;
                                        }
                                    }
                                }


                                /*
                                if (replyMessage.length >= charLimitSecond) {
                                    replyMessage = textAbstract(replyMessage, charLimitSecond).escape();
                                }
                                */

                                temp = template;
                                temp = temp.replace("{0}", messages[m2].sender_profileURL);
                                temp = temp.replace("{1}", messages[m2].sender_thumbnail);
                                temp = temp.replace("{2}", messages[m2].sender_profileURL);
                                temp = temp.replace("{3}", messages[m2].sender_displayname);
                                temp = temp.replace("{4}", messages[m2].web_url);
                                temp = temp.replace("{5}", messages[m2].body);
                                //single msg
                                temp = temp.replace("{single}", singleMsg);

                                temp = temp.replace("{6}", messages[m2].web_url);
                                temp = temp.replace("{7}", messages[m2].group_id);
                                temp = temp.replace("{8}", messages[m2].thread_id);
                                temp = temp.replace("{9}", messages[m2].topic_id);
                                temp = temp.replace("{12}", messages[m2].message_id);
                                temp = temp.replace("{13}", messages[m2].group_name);
                                temp = temp.replace("{14}", timeString);
                                temp = temp.replace("{15}", lastMessageId);
                                //temp = temp.replace("{extra}", firstMessage1024);

                                if (replyMessage != "") {
                                    temp = temp.replace("{10}", replyUser);
                                    temp = temp.replace("{11}", replyMessage);
                                }

                                var tempElement = $(temp);


                                $("#myYammerDetails").append(tempElement);

                                if (replyMessage == "") {
                                    tempElement.find('.yammsg-first-message').attr('style', 'max-height:none');
                                    tempElement.find('.yammsg-second-message').attr('style', 'display:none');
                                }


                                countMessages++;
                            }

                            if (countMessages) {

                                $("#myYammerDetails").slick({
                                    slide: '.yammsg',
                                    dots: true,
                                    arrows: false,
                                    infinite: false,
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    responsive: [{
                                        breakpoint: 1023,
                                        settings: {
                                            slidesToShow: 2,
                                            slidesToScroll: 2
                                        }
                                    }, {
                                        breakpoint: 769,
                                        settings: {
                                            dots: false,
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                        }
                                    }

                                    ]
                                });

                                $(".yammsg-body").dotdotdot({
                                    watch:'window'
                                });

                                $('.slider-item.yammsg').on('click', function (event) {

                                    var slickParent = $(event.target).closest('.slick-slider');

                                    event.preventDefault();

                                    //remove all other overlay
                                    var others = $('.modal-overlay');
                                    if (others.length > 0) {
                                        others.each(function (event) {
                                            $(this).addClass('animated fadeOut');
                                            $(this).siblings('.circle').addClass('animated slideOutUp');
                                            $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-slider').not(slickParent).find('.slick-dots').css('z-index', '1');
                                                $(this).siblings('.circle').remove();
                                                $(this).closest('.slider-item').css('cursor', 'pointer');
                                                $(this).remove();

                                            });
                                        });
                                    }


                                    //change different overlay buttons here
			    
                                    var overlay = $('<div class="modal-overlay"></div><div class="circle circle-view left two">VIEW</div><div class="circle circle-read right two two-lines">MARK AS<BR>READ</div>');
                                    /*<div class="circle circle-reply"></div>*/

                                    overlay.addClass('animated fadeIn');
                                    var parent = $(event.target).closest('.slider-item');
                                    $(this).closest('.slick-slider').find('.slick-dots').css('z-index', '-1');
                                    parent.css('cursor', 'auto');
                                    parent.append(overlay);

                                    var last_message_id = parent.find('.yammer-last-message-id').text();
                                    var message_id = parent.find('.yammer-message-id').text();
                                    var thread_id = parent.find('.yammer-thread-id').text();
                                    var group_name = parent.find('.yammer-group-name').text();



                                    $('.circle').addClass('animated slideInUp');
                                    overlay.on('click', function (event) {
                                        event.stopPropagation();
                                        if ($(event.target).hasClass('modal-overlay')) {
                                            $(this).addClass('animated fadeOut');
                                            $(this).siblings('.circle').addClass('animated slideOutUp');
                                            overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-dots').css('z-index', '1');
                                                overlay.remove();
                                                parent.css('cursor', 'pointer');
                                            });
                                        }

                                        //view details
                                        if ($(event.target).hasClass('circle-view')) {

                                            var token = getCookie("yammer_access_token");
                                            if (token) {
                                                loadYammerRelatedMessages({
                                                    token:token, 
                                                    messageID:message_id, 
                                                    threadID: thread_id,
                                                    groupName:group_name
                                                });
                                                markAsRead(token, last_message_id, thread_id, false);
                                            } else {
                                                
                                            }

                                            var element = $(this).parent().find('.slider-item');

                                            $(this).addClass('animated slideOutUp');
                                            $(this).siblings('.circle').addClass('animated slideOutUp');
                                            $(this).siblings('.modal-overlay').addClass('animated fadeOut')
                                            overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-dots').css('z-index', '1');
                                                overlay.remove();
                                                parent.css('cursor', 'pointer');

                                            });

                                        }

                                        if ($(event.target).hasClass('circle-read')) {
                                            $('body').addClass('loading');
                                            var token = getCookie("yammer_access_token");
                                            if (token) {
                                                var slideIndex = parent.attr('data-slick-index');
                                                parent.addClass('animated slideOutUp');
                                                parent.one('animationend', function () {
                                                    //$('#myYammerDetails').slick('slickRemove', slideIndex);
                                                    //$('body').addClass('loading');
                                                    markAsRead(token, last_message_id, thread_id, slideIndex);
                                                });
                                            } else {
                                            }



                                            $(this).addClass('animated slideOutUp');
                                            $(this).siblings('.circle').addClass('animated slideOutUp');
                                            $(this).siblings('.modal-overlay').addClass('animated fadeOut');
                                            overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-dots').css('z-index', '1');
                                                overlay.remove();
                                                parent.css('cursor', 'pointer');
                                            });
                                        }


                                        if ($(event.target).hasClass('circle-reply')) {
                                            var replyHTML = '<div class="popup-container ">' + '<div class="popup-subject">Quick Reply</div>' + '<textarea placeholder="Type your message..." class="popup-reply-textarea">' + '</textarea>' + '<div class="popup-reply-error-status"></div>' + '<div class="popup-reply-button-container">' + '<div class="popup-button send">Send</div>' + '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';

                                            $.magnificPopup.open({
                                                items: {
                                                    src: replyHTML,
                                                    type: 'inline'
                                                },
                                                closeOnBgClick: false,
                                                closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close">x</button>',
                                                callbacks: {
                                                    open: function () {
                                                        //add event handlers on the button
                                                        $('.popup-button.send').on('click', function () {
                                                            var messageContent = $('.popup-reply-textarea').val();
                                                            if (messageContent.trim() == "") {
                                                                $('.popup-reply-error-status').html('Please input a message');
                                                            } else {
                                                                $('.popup-reply-error-status').html('');
                                                                var token = GetCookie("yammer_access_token");
                                                                if (token) {
                                                                    $.magnificPopup.close();
                                                                    $('body').addClass('loading');
                                                                    replyYammerMessage(token, messageContent, message_id, thread_id);
                                                                } else {
                                                                }

                                                            }
                                                        });
                                                        $('.popup-button.cancel').on('click', function () {
                                                            $.magnificPopup.close();
                                                        });
                                                    }
                                                }
                                            });


                                            $(this).addClass('animated slideOutUp');
                                            $(this).siblings('.circle').addClass('animated slideOutUp');
                                            $(this).siblings('.modal-overlay').addClass('animated fadeOut');

                                            overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-dots').css('z-index', '1');
                                                overlay.remove();
                                                parent.css('cursor', 'pointer');
                                            });
                                        }
                                    });
                                });


                                $("#myYammerDetails").on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                                    //remove all other overlay							    
                                    var others = $('#myYammerDetails .modal-overlay');
                                    if (others.length > 0) {
                                        others.each(function (event) {
                                            $(this).addClass('animated fadeOut');

                                            $(this).siblings('.circle').addClass('animated slideOutUp'); //fadeOut
                                            $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                                $('.slick-dots').css('z-index', '1');
                                                $(this).siblings('.circle').remove();
                                                $(this).closest('.slider-item').css('cursor', 'pointer');
                                                $(this).remove();

                                            });
                                        });
                                    }

                                });



                            }

                        });
                    } else {
                        $("#myYammerDetails").html("<div class='empty-msg'>You\'ve read everything in your Yammer Inbox.</div>");
                    }

                    if (reload != undefined) {
                        $('body').removeClass('loading'); //remove loading gif
                    }

                }
            },
            error: function (msg) {
                console.log(msg);
                $("#myYammerDetails").html("<div class='empty-msg'>Data request failed.</div>");
            }
        });
    } catch (err) {
        console.log("Exception: " + err.message);
        $("#myYammerDetails").html("<div class='empty-msg'>Data request failed.</div>");
    }
}

//token, target_user_email, content, callback
function startYammerConversation(obj) {
    try{
        //get user id by email
        yam.platform.setAuthToken(obj.token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/users/by_email.json?email="+ obj.target_user_email,
            method: "GET",
            success: function (user) {
                var userId = user[0].id;

                //send private message
                yam.platform.request({
                    url: "https://api.yammer.com/api/v1/messages.json",
                    method: 'POST',
                    data: { 'body': obj.content, 'direct_to_id': userId },
                    success: function (msg) {
                        var messageId = msg.messages[0].id;
                        var threadId = msg.messages[0].thread_id;

                        obj.messageID = messageId;
                        obj.threadID = threadId;
                        loadYammerRelatedMessages(obj);


                        //save yammer message id and thread id in DB
                        updateYammerMessageAndThreadID(obj.taskID, messageId, threadId, obj.type);

                    },
                    error: function (err) {
                        console.log(error);
                        $('body').removeClass('loading');
                    }
                });

            },
            error: function (error) {
                console.log(error);
                $('body').removeClass('loading');
            }
        });

    } catch (ex) {
        console.log(ex.message);
    }

}

function updateYammerMessageAndThreadID(taskID, messageID, threadID, type) {
    try {

        var accessToken = getAccessToken();
        
        var endpointUrl = endpoint + "/api/tasks/op/updateYammer?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&userEmail=" + encodeURIComponent(userEmail) + "&tenantId=" + encodeURIComponent(tenantId) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&taskId=" + taskID + "&messageId=" + messageID + "&threadId=" + threadID;

            $.ajax({
                url: endpointUrl,
                method: 'POST',
                async: true,
                success: function (json) {

                    if (json.StatusCode != 1) {

                        //update access token
                        if (json.StatusCode == 2) {
                            setAccessToken(json);
                        }

                        if (type.toLowerCase() == "tabm") {
                            $('#assigneeTasksDetails').slick('unslick');
                            $('#assigneeTasksDetails').empty();
                            loadTABMFeed(userEmail, true);
                        } else {
                            $('#myTasksDetails').slick('unslick');
                            $('#myTasksDetails').empty();
                            loadTATMFeed(userEmail, true);
                       }
                    }
                        //web service exception error
                    else {
                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);
                        
                        //no credentials (tenant ID, license key unverified)
                        if (json.error_code == 'EG0004') {
                            alert('You do not have the required credentials to perform this operation');
                        }

                        $('body').removeClass('loading');
                    }

                },
                error: function (err) {
                    console.log(err);
                    alert('error: ' + err.message);
                    $('body').removeClass('loading'); //remove loading gif
                }
            });
    }
    catch (ex) {
        //no credentials (tenant ID, license key unverified)
        if (ex.message == 'EG0004') {
            alert('You do not have the required credentials to perform this operation');
        }
        console.log('error: ' + ex.message);
    }
}

function replyYammerMessage(token, message, message_id, thread_id, reload) {
    try {

        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/messages.json",
            method: "POST",
            data: {
                body: message,
                replied_to_id: message_id
            },
            success: function (msg) {
                console.log(msg);

                if (reload) {
                    $('.popup-yammer-textarea').html('');

                    //reload popup
                    loadYammerRelatedMessages({
                        token:token, 
                        messageID:message_id, 
                        threadID:thread_id, 
                        reload:true
                    });
                } else {
                    $('body').removeClass('loading');
                }

            },
            error: function (error) {
                console.log(error);
                $('body').removeClass('loading');
            }
        });
    } catch (ex) {
        console.log(ex.message);
    }
}


function markAsRead(token, message_id, thread_id, slideIndex) {
    try {

        yam.platform.setAuthToken(token);

        yam.platform.request({

            url: "https://api.yammer.com/api/v1/messages/last_seen_in_thread.json",
            method: "POST",
            data: {
                "message_id": message_id,
                "thread_id": thread_id
            },
            success: function (msg) {

                //this will reload yammer msg panel
                if (slideIndex) {
                    $('#myYammerDetails').slick('slickRemove', slideIndex);
                    $('#myYammerDetails').slick('unslick');
                    $('#myYammerDetails').empty();
                    loadYammerMessages(token, true);
                }

            },
            error: function (error) {
                console.log(error);
                $('body').removeClass('loading');
                
            }
        });

    } catch (ex) {
        console.log(ex.message);
    }
}


function markAsUnread(token, thread_id, loading) {
    try {
        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/messages/last_seen_in_thread.json",
            method: "POST",
            data: {
                "message_id": 0,
                "thread_id": thread_id
            },
            success: function (msg) {

                console.log(msg);
                if (loading) {
                    $('body').removeClass('loading');
                }
            },
            error: function (error) {
                console.log(error);
                if (loading) {
                    $('body').removeClass('loading');
                }
            }
        });

    } catch (ex) {
        console.log(ex.message);
    }
}

/*start yammer helper*/
function generateYammerPopUpHTML(obj, type) {
    //type --> start
    var popUpHTML = '';
    if (type == 'start') {
        popUpHTML = '<div class="popup-container ">' + '<div class="popup-title yammer">Yammer</div>';
        popUpHTML += '<textarea class="popup-reply-textarea">' + obj.subject + ' --- ' + '</textarea>' + '<div class="popup-reply-error-status"></div>' + '<div class="popup-reply-button-container">' + '<div class="popup-button send">Send</div>' + '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';
    }
    return popUpHTML;

}

//yammer button handler
function yammerButtonHandler(obj, type) {
    if (type == 'start') {
        $('.popup-button.send').on('click', function () {
            var messageContent = $('.popup-reply-textarea').val();
            if (messageContent.trim() == "") {
                //don't do anything
                $('.popup-reply-error-status').html('Please input a message');
            } else {
                $('.popup-reply-error-status').html('Sending...');
                $('.popup-reply-error-status').attr('style', 'color:#444;');
                //$('.popup-reply-error-status').html('');
                //token, target_user_email, content
                obj.content = messageContent;
                
                startYammerConversation(obj);
            }
        });

        $('.popup-button.cancel').on('click', function () {
            $.magnificPopup.close();
            $('body').removeClass('loading');
        });
    }
}

/*end yammer helper*/

function loadYammerRelatedMessages(obj) {
    try {
        if (!obj.reload) {
            $('body').addClass('loading');
        }
        yam.platform.setAuthToken(obj.token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/messages/in_thread/" + obj.threadID + ".json",
            method: "GET",
            success: function (msg) {
                var messages = msg.messages;
                var title = 'Yammer';
                if (obj.groupName) {
                    title = obj.groupName
                }
                var popUpHTML = '<div class="popup-container"><div class="popup-title yammer">'+title+'</div>';

                //messages.forEach(function(a){
                for (var j = messages.length - 1; j >= 0; j--) {
                    var thumb = '';
                    var sender = '';
                    //msg.references
                    for (var i = 0; i < msg.references.length; i++) {
                        if (msg.references[i].id == messages[j].sender_id) {
                            thumb = msg.references[i].mugshot_url;
                            sender = msg.references[i].full_name;
                            break;
                        }
                    }

                    var attachments = messages[j].attachments;
                    var attachmentLink = [];
                    for (var k = 0; k < attachments.length; k++) {
                        var download_url = attachments[k].download_url;
                        var web_url = attachments[k].web_url;

                        var url = download_url;

                        if (checkIsNull(url)) {
                            url = web_url;
                        }

                        var attachmentName = attachments[k].name;
                        if (!checkIsNull(attachmentName)) {
                            attachmentLink.push('<a class="popup-yammer-attachment" href="' + url + '" target="_blank">' + attachmentName + '</a>');
                        }
                    }

                    var timeString = getDateString(new Date(messages[j].created_at), true);

                    var body = messages[j].body.plain.escape();

                    //add the link url
                    body = convertUrlIntoLink(body);

                    popUpHTML += '<div class="popup-yammer">' + '<div class="popup-yammer-thumb">' + '<img class="popup-yammer-image" src="' + thumb + '" />' + '</div>' + '<div class="popup-yammer-content">' + '<div class="popup-yammer-sender">' + sender + '</div>' + '<div class="popup-yammer-body">' + body + '<div class="popup-yammer-attachment-container">';

                    $(attachmentLink).each(function () {
                        popUpHTML += this;
                    });

                    popUpHTML += '</div>' + '</div>' +'<div class="popup-yammer-date">'+ timeString +'</div>'+'</div>' + '</div>';

                }

                popUpHTML += '<textarea class="popup-yammer-textarea" placeholder="Type your message here..."></textarea>' + '<div class="popup-yammer-error-status"></div>' + '<div class="popup-button-container-yammer">' + '<div class="popup-button send">Send</div>' + '<div class="popup-button unread">Mark As Unread</div>' + '<div class="popup-button cancel">Cancel</div>' + '</div>' + '</div>';

                if ($('.mfp-content').length > 0 && obj.reload) {
                    //some fade out fade in
                    $('.popup-container').addClass('animated custom fadeOutFull');

                    $('.popup-container').one('animationend', function () {
                        $('.mfp-content').html(popUpHTML);
                        $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');
                        $('.popup-container').addClass('animated custom fadeInFull');
                        popUpButtonHandler();
                    });
                } else {
                    $.magnificPopup.open({
                        items: {
                            src: popUpHTML,
                            type: 'inline'
                        },
                        closeOnBgClick: false,
                        closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close">x</button>',
                        callbacks: {
                            open: function () {
                                //add event handlers on the button
                                popUpButtonHandler();
                            }
                        }
                    });
                }

                $('body').removeClass('loading');

                function popUpButtonHandler() {
                    $('.popup-button.unread').on('click', function () {
                        var token = GetCookie("yammer_access_token");
                        if (token) {
                            $.magnificPopup.close();
                            $('body').addClass('loading');
                            markAsUnread(token, obj.threadID, true);

                        } else {
                            oauth_yammer_init();
                        }
                    });

                    $('.popup-button.send').on('click', function () {
                        var messageContent = $('.popup-yammer-textarea').val();
                        if (messageContent.trim() == "") {
                            $('.popup-yammer-error-status').html('Please input a message');
                        } else {
                            $('.popup-yammer-error-status').html('Sending...');
                            $('.popup-yammer-error-status').attr('style', 'color:#444;');
                            replyYammerMessage(obj.token, messageContent, obj.messageID, obj.threadID, true);
                        }

                    });

                    $('.popup-button.cancel').on('click', function () {
                        $.magnificPopup.close();
                    });
                }

            },
            error: function (error) {
                $('body').removeClass('loading');
                console.log(error);
            }
        });

    } catch (ex) {
        $('body').removeClass('loading');
        console.log(ex.message);
    }
}


function loadFollowedSites() {
    getScriptCache(webPartConfig.SPHostUrl + "/_layouts/15/" + "SP.RequestExecutor.js", function () {

        //var hostweburl = decodeURIComponent(webPartConfig.SPHostUrl);
        var appweburl = _spPageContextInfo.webAbsoluteUrl; //decodeURIComponent(webPartConfig.SPAppWebUrl);
        
        var executor = new SP.RequestExecutor(appweburl);

        var endpoint = appweburl + '/_api/social.following';

        executor.executeAsync({
            //check only sites
            url: endpoint + '/my/followed(types=4)',
            method: 'GET',
            headers: {
                'Accept': 'application/json;odata=verbose'
            },
            success: followedContentRetrieved,
            error: requestFailed
        });
    });
}


// Parse the JSON data and iterate through the collection.
function followedContentRetrieved(data) {
    var stringData = JSON.stringify(data);
    var jsonObject = JSON.parse(stringData);
    

    var followedSites = JSON.parse(jsonObject.body).d.Followed;

    if (followedSites) {
        var sites = followedSites.results;

        loadFollowedSitesData(sites);
    }
}

function requestFailed(xhr, ajaxOptions, thrownError) {
    console.log(thrownError);
}

function loadFollowedSitesData(data) {
    var feedLimit = 10;
    var count = 0;

    if (data.length > 0) {
        //reset
        var html = "";
        $("#myFollowedSitesDetails").removeClass('animated custom fadeInFull');
        $("#myFollowedSitesDetails").addClass('animated custom fadeOutFull');

        $('#myFollowedSitesDetails').one('animationend', function () {
            if ($('#myFollowedSitesDetails').hasClass('slick-slider')) {
                $('#myFollowedSitesDetails').slick('unslick');
            }

            $('#myFollowedSitesDetails').removeClass('animated custom fadeOutFull');
            $('#myFollowedSitesDetails').html('');
            $('#myFollowedSitesDetails').addClass('animated custom fadeInFull');

            $(data).each(function (index) {
                
                var title = this.Name;
                var contentUrl = this.ContentUri;
                var url = this.Uri;

                count++;

                html += '<div class="site-item">'
                        + '<a href="' + url + '" target="_blank">'
                        + '<div class="site-thumb">'
                        + getFileLogoHTML('site')
                        + '</div>'
                        + '<div class="site-content">'
                        + '<div class="site-title">'
                        + title
                        + '</div>'
                        + '<div class="site-url">'
                        //+ contentUrl
                        + '</div>'
                        + '</div>'
                        + '</a>'
                        + '</div>';

                if (count == feedLimit) {
                    return false;
                }

            });

            $('#myFollowedSitesDetails').html(html);

            //slick init
            $("#myFollowedSitesDetails").slick({
                slide:'.site-item',
                dots: true,
                arrows: false,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [{
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }, {
                    breakpoint: 769,
                    settings: {
                        dots: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });
        });
    }
    else {
        $('#myFollowedSitesDetails').html('<div class="empty-msg">You do not have any followed site.</div>');
    }
}

//NGO data
/*
function getBannerContent(callback) {
        var formList = "BannerContent";
        var scriptbase = webPartConfig.SPHostUrl + "/_layouts/15/";
        $.getScript(scriptbase + "SP.RequestExecutor.js", loadForm);

        var appweburl = webPartConfig.SPAppWebUrl;
        var hostweburl = webPartConfig.SPHostUrl;

        function loadForm() {
            var clientContext = new SP.ClientContext(appweburl);
            var factory = new SP.ProxyWebRequestExecutorFactory(appweburl);
            clientContext.set_webRequestExecutorFactory(factory);

            var appContextSite = new SP.AppContextSite(clientContext, hostweburl);
            var oWeb = appContextSite.get_web();

            var list = oWeb.get_lists().getByTitle(formList);

            var camlQuery = new SP.CamlQuery();
            //camlQuery.set_viewXml("<View><ViewFields><FieldRef Name='ExpenseName' /><FieldRef Name='ExpenseDescription' /><FieldRef Name='ExpenseAmount' /><FieldRef Name='ExpenseType' /><FieldRef Name='isApproved' /><FieldRef Name='isRejected' /><FieldRef Name='isReimbursed' /><FieldRef Name='isDismissed' /><FieldRef Name='ExpenseDate' /><FieldRef Name='Approver' /><FieldRef Name='ApproverComment' /><FieldRef Name='Submitter' /><FieldRef Name='Administrator' /></ViewFields><Query><Where><And><Eq><FieldRef Name='Submitter'/><Value Type='Integer'><UserID /></Value></Eq><Eq><FieldRef Name='isDismissed'/><Value Type='Integer'>0</Value> </Eq></And></Where></Query></View>");
            camlQuery.set_viewXml("<View><ViewFields><FieldRef Name='Title' /><FieldRef Name='RightContent' /><FieldRef Name='LeftContent' /><FieldRef Name='CustomClass' /><FieldRef Name='ColorCode' /></ViewFields></View>");

            var listItems = list.getItems(camlQuery);
            clientContext.load(listItems);
            clientContext.executeQueryAsync(function (sender, args) {
                var listItemEnumerator = listItems.getEnumerator();
                var html = '';
                var count = 0;


                while (listItemEnumerator.moveNext()) {
                    count++;
                    var listItem = listItemEnumerator.get_current();

                    var id = listItem.get_id();

                    var title = listItem.get_item('Title');
                    var rightContent = listItem.get_item('RightContent');
                    //left content is an html content
                    var leftContent = listItem.get_item('LeftContent');
                    var customClass = listItem.get_item('CustomClass');
                    var colorCode = listItem.get_item('ColorCode');

                    var sliderHTML = '<div class="title-banner-container ' + customClass + '" style="background-color:' + colorCode + '">'
                                    + '<div class="title-banner-wrapper">'
                                    + '<div class="title-banner-left-container">'
                                    //+ leftContent
                                    + '<img class="img-banner" src="'+ leftContent +'">'
                                    + '</div>'
                                    + '<div class="title-banner-right-container">'
                                    + '<div class="title-banner-header">'
                                    + title
                                    + '</div>'
                                    + '<div class="title-banner-content">'
                                    + rightContent
                                    + '</div>'
                                    + '</div></div></div>';

                    //check if slider goes to current page, if yes skip
                    $('.banner-slider-container').append(sliderHTML);
                }

                if (count > 0) {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            },
            function (sender, args) {
                console.log('error: ' + args.get_message());
            });
        }
}

function runSlick() {
    //init slick
    $('.banner-slider-container').slick({
        slide: '.title-banner-container',
        autoplay: true,
        autoplaySpeed: 3500,
        pauseOnHover: false,
        dots: true,
        arrows: true,
        fade: true,
        //fade:false,
        prevArrow: $('.prev-button'),
        nextArrow: $('.next-button'),
        infinite: true,
        speed: 600,
        //speed:1000, 
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    });

    //pause slick when the arrow is clicked
    $('.slick-arrow, .slick-dots li').on('click', function () {
        $('.banner-slider-container').slick('slickPause');
    });

    //show the banner
    $('.banner-slider-container').removeClass('loading');

    //show 1st row banner
    //$('.celgo-PageLayout-1stRow').removeClass('loading');
}
*/
