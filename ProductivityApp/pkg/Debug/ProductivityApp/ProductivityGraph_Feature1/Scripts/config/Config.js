﻿var listName = "Productivity Config List";
var RSSFeedList = "RSS Feed Config";
var RSSFeedPreference = "RSS Feed Preference";
var firstLoadLoaded = false;

$(document).ready(function () {

    readSettings();
    
    getUserEmail(function () {
        
        readRSSFeed(function () {
            rssPreference(loadConfig);
        });
    });

});

function loadConfig() {

    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: listName,
        CAMLViewFields: "<ViewFields><FieldRef Name='ID' /><FieldRef Name='TenantID' /><FieldRef Name='Office365AppID' /><FieldRef Name='YammerAppID' /><FieldRef Name='EncryptionKey' /><FieldRef Name='loginError' /><FieldRef Name='retries' /></ViewFields>",
        CAMLRowLimit: 1,
        completefunc: function (xData, Status) {
            //no data
            if ($(xData.responseXML).SPFilterNode("z:row").length == 0) {
                //loadDetailedConfig();

                firstLoadHandler();
                firstLoadGreet();

                //set empty values
                yammerID = '';
                office365ID = '';
                tenantId = '';
                encryptionKey = '';
                retries = 0;
            }
            else {

                $(xData.responseXML).SPFilterNode("z:row").each(function () {
                    //store the config in the hidden element
                    configId = $(this).attr("ows_ID");
                    $("#tenantId").val($(this).attr("ows_TenantID"));
                    $("#office365AppId").val($(this).attr("ows_Office365AppID"));
                    $("#yammerAppId").val($(this).attr("ows_YammerAppID"));
                    $("#loginError").val($(this).attr("ows_loginError"));
                    $("#retries").val($(this).attr("ows_retries"));
                });

                //populate global variables 
                yammerID = $("#yammerAppId").val();
                office365ID = $("#office365AppId").val();
                tenantId = $("#tenantId").val();
                //encryptionKey = $("#encryptionKey").val();
                loginError = $("#loginError").val();
                //retries = $("#retries").val();

                if ($("#retries").val()) {
                    retries = parseInt($("#retries").val());
                }
                
                if (checkIsNull(office365ID) && checkIsNull(tenantId) && checkIsNull(yammerID)) {
                    firstLoadHandler();
                    firstLoadGreet();
                }
                else if (checkIsNull(office365ID) || checkIsNull(tenantId)) {
                    loadPage();
                }
                //in the normal flow, 1 retries is needed 
                else if (loginError && (retries > 1)) {
                    loadDetailedConfig('adal', function () {

                        tenantId = $('#tenantId').val();
                        office365ID = $('#office365AppId').val();
                        yammerID = $('#yammerAppId').val();

                        var passedObj = {
                            id: configId,
                            tenantId: tenantId,
                            office365AppId: office365ID,
                            yammerappid: yammerID,
                            loginError: ' ',
                            retries: 0
                        };

                        saveValueToList(passedObj, true, function () {
                            adalLogin(true, function (authContext) {
                                adalToken(authContext, function () {
                                    /*
                                    loadPage();
                                    $.magnificPopup.instance.close();
                                    */
                                    location.reload();
                                })
                            });
                        });

                    });
                }
                else {
                    adalLogin(false, function (authContext) {
                        adalToken(authContext, function () {
                            loadPage();
                        });
                    });
                }
            }

            
        }
    });

}




function adalLogin(popUp, callback) {
    window.adalConfig = {
        tenant: tenantId,
        clientId: office365ID,
        postLogoutRedirectUri: window.location.origin,
        endpoints: {
            graphApiUri: 'https://graph.microsoft.com'
        },
        popUp: popUp,
        cacheLocation: 'localStorage' // enable this for IE, as sessionStorage does not work for localhost.
    };

    var authContext = new AuthenticationContext(adalConfig);

    //Check For & Handle Redirect From AAD After Login
    var isCallback = authContext.isCallback(window.location.hash);
    authContext.handleWindowCallback();

    if (!isCallback) {
        // If not logged in force login
        var user = authContext.getCachedUser();
        if (user) {
            // Logged in already
            console.log('user logged in');

            //update config list to track error status
            var obj = {
                loginError: ' ',
                id: configId,
                retries: 0
            };

            saveValueToList(obj, true, function () {
                if (typeof callback == 'function') {
                    callback(authContext);
                }
            });
        }
        else {

            //20161012 moved counter for retries in the login process
            if (!inIframe()) {
                //update config list to track error status
                var obj = {
                    loginError: 'Unable to authenticate user.',
                    id: configId,
                    retries: retries + 1
                };

                saveValueToList(obj, true, function () {
                    authContext.login();
                });
            }
        }
    }
}

function adalToken(authContext, callback) {
   
    var isCallback = authContext.isCallback(window.location.hash);
    var errorMsg = authContext.getLoginError();

    if (isCallback && !errorMsg) {
        console.log(errorMsg);        
            window.location = authContext._getItem(authContext.CONSTANTS.STORAGE.LOGIN_REQUEST);
    }

    // Acquire token for Files resource.
    authContext.acquireToken(adalConfig.endpoints.graphApiUri, function (error, token) {

        // Handle ADAL Errors.
        if (error || !token) {

            console.log('ADAL error occurred: ' + error);

            return
        }
        else {
            if (!inIframe()) {
                if (typeof callback == 'function') {
                    //save token in cookies
                    setAccessToken(token);
                    callback();
                }
            }
        }

        /*
        // Handle ADAL Errors.
        if (error || !token || !user) {

            console.log('ADAL error occurred: ' + error);

            var loginError = 'Undefined error.';

            if (error) {
                loginError = error;
            }

            //adal js requires multiple attempt to get token and user
            //20161007 need to add counter for retries before prompting a popup
            if (!inIframe() && !user) {
                //update config list to track error status
                var obj = {
                    loginError: loginError,
                    id: configId,
                    retries: retries + 1
                };

                //if user login is required or token is still cached
                if (error == 'User login is required' || token) {
                    saveValueToList(obj);
                }
                else {
                    saveValueToList(obj, true);
                }
            }
            

            return
        }
        else {
            if (!inIframe()) {
                if (typeof callback == 'function') {
                    //save token in cookies
                    setAccessToken(token);
                    callback();
                }
            }
        }
        */
    });
}

//check if in iFrame
function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function loadPage() {
    if ($('#_HomePage').length > 0) {
        //call back for the main page script
        owaInit(productivityInit);
    }
    else if ($('#_TaskPage').length > 0) {
        //call back for the task page script
        owaInit(taskInit);
    }
    else if ($('#_SearchPage').length > 0) {
        //call back for the search page script
        owaInit(searchResultInit);
    }
}

function checkConnectionSettings(type) {
    var status = false;

    //for yammer, task, and outlook
    if (type == 'yammer') {
        if (checkIsNull(yammerID)) {
            status = false;
        }
        else {
            status = true;
        }
    }
    else if (type == 'task') {
        if (checkIsNull(licenseKey) || checkIsNull(tenantId)) {
            status = false;
        }
        else {
            status = true;
        }
    }
    else if (type == 'outlook') {
        if (checkIsNull(office365ID) || checkIsNull(tenantId)) {
            status= false;
        }
        else {
            status = true;
        }
    }

    return status;
}

function firstLoadHandler() {
    $('#greetpopup .greet.button.start').on('click', function () {
        $.magnificPopup.instance.close();
        loadHow();
    });

    $('#howpopup .greet.button.next').on('click', function () {
        $.magnificPopup.instance.close();
        loadFeatures();
    });

    //add event handlers
    $('#featurespopup .greet.button.back').on('click', function () {
        $.magnificPopup.instance.close();
        loadHow();
    });

    $('#featurespopup .greet.button.config').on('click', function () {
        $.magnificPopup.instance.close();
        loadDetailedConfig();
    });
}

function firstLoadGreet() {
    setTimeout(function () {
        var popUp = $.magnificPopup.open({
            items: {
                src: $('#greetpopup'),
                type: 'inline'
            },
            closeBtnInside: true,
            alignTop: false,
            closeOnBgClick: false,
            callbacks: {
                open: function () {
                    //add event handlers

                    $('.mfp-content').css('transition', 'none');
                    $('.mfp-content').css('max-height', 'inherit');
                    
                },
                close: function () {
                    if (!firstLoadLoaded) {
                        firstLoadLoaded = true;
                        loadPage();
                    }
                }
            }
        });

        

    }, 0);
}

/*content for first load popup*/
function loadHow() {
    setTimeout(function () {
        var howPopUp = $.magnificPopup.open({
            items: {
                src: $('#howpopup'),
                type: 'inline'
            },
            closeBtnInside: true,
            alignTop: false,
            closeOnBgClick: false,
            callbacks: {
                open: function () {

                    $('.mfp-content').css('transition', 'none');
                    $('.mfp-content').css('max-height', 'inherit');
                    //add event handlers
                    /*
                    $('.greet.button.back').on('click', function () {
                        firstLoadGreet();
                        howPopUp.close();
                    });
                    */

                    
                },
                close: function () {
                    if (!firstLoadLoaded) {
                        firstLoadLoaded = true;
                        loadPage();
                    }
                }
            }
        });


    }, 0);
}

/*content for features popup*/
function loadFeatures() {
    setTimeout(function () {
        var featuresPopUp = $.magnificPopup.open({
            items: {
                src: $('#featurespopup'),
                type: 'inline'
            },
            closeBtnInside: true,
            alignTop: false,
            closeOnBgClick: false,
            callbacks: {
                open: function () {

                    $('.mfp-content').css('transition', 'none');
                    $('.mfp-content').css('max-height', '100%');

                    //run slider
                    if ($(".features-container").hasClass('slick-slider')) {
                        $(".features-container").slick('unslick');
                    }

                    //enable slider for the features
                    $(".features-container").slick({
                        slide: '.feature-wrapper',
                        dots: true,
                        arrows: false,
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        adaptiveHeight: true
                    });

                    $('.features-container')[0].slick.refresh();

                },
                close: function () {
                    if (!firstLoadLoaded) {
                        firstLoadLoaded = true;
                        loadPage();
                    }
                }
            }
        });
    }, 0);
}

function loadDetailedConfig(type, callback) {

    setTimeout(function () {
        /*first load, show panel with registration links*/
        //first load, show welcome messages
        //4 panels, schedule, flagged email, my tasks, their tasks 

        //reset important bold
        $('.configcelltitle').attr('style', null);
        $('.configcelltitle span.important').attr('style', null);

        $.magnificPopup.open({
            items: {
                src: $('#configcontainer'),
                type: 'inline'
            },
            closeBtnInside: true,
            alignTop: false,
            closeOnBgClick: false,
            callbacks: {
                open: function () {
                    if (type == 'adal') {
                        //override save config handler
                        $('.mfp-content #configcontainer .popup-button').on('click', function () {
                            if (typeof callback == 'function') {
                                callback();
                            }
                        });
                    }
                    else {
                        $('.mfp-content #configcontainer .popup-button').on('click', saveConfig);
                    }
                }
            }
        });

        $('.mfp-content').css('max-height', '600px');
        $('.mfp-content').css('max-width', '900px');
        $('.mfp-content .welcome').show();
        $('#configcontainer .adal-error').hide();

        var classArr = [];

        if (type == 'outlook') {
            classArr = ['azureApp', 'tenantId', 'license'];
        }
        else if (type == 'task') {
            classArr = ['tenantId', 'license'];
        }
        else if (type == 'yammer') {
            classArr = ['yammerId'];
        }
        //20161007
        else if (type == 'adal') {
            classArr = ['adal-error'];

            $('.mfp-content').attr('style', null);
            $('.mfp-content .welcome').hide();
            $('#configcontainer .adal-error').show();
            $('#configcontainer .adal-error').html('<div>User sign in or authentication failed. Please ensure that the network tenant and client identifier is correct.</div>');

            if (loginError) {
                $('#configcontainer .adal-error').append('<div>Error message: ' + loginError +'</div>');
            }
        }

        classArr.forEach(function (a) {
            $('.mfp-content .configcelltitle.' + a).attr('style', 'font-weight:600; color:red;');
            $('.mfp-content .configcelltitle.' + a + ' span.important').attr('style', 'display:inline-block;');
        });

    }, 0);
}

//read query strings
function readSettings(arr) {
    //to differentiate query string in search page, cannot read settings in search page
    if (document.URL.split("?").length > 1) {

        //just in case there is hash
        var params = document.URL.split("?")[1].split('#')[0];
        params = params.split("&");

        for (var i = 0; i < params.length; i = i + 1) {
            var param = params[i].split("=");
            //1 param
            if (typeof arr === 'string') {
                if (arr === param[0]) {
                    webPartConfig[param[0]] = decodeURIComponent(param[1]);
                }
            }
                //multiple params in array
            else if (typeof arr === 'object' && arr instanceof Array) {
                if (arr.indexOf(param[0]) > -1) {
                    webPartConfig[param[0]] = decodeURIComponent(param[1]);
                }
            }
                //no param
            else {
                webPartConfig[param[0]] = decodeURIComponent(param[1]);
            }
        }
    }
}

function saveConfig() {
    var id = null;

    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: listName,
        CAMLViewFields: "<ViewFields><FieldRef Name='ID' /></ViewFields>",
        CAMLRowLimit: 1,
        completefunc: function (xData, Status) {
            $(xData.responseXML).SPFilterNode("z:row").each(function () {
                id = $(this).attr("ows_ID");
            });

            var tenantId = $('#tenantId').val();
            var office365AppId = $('#office365AppId').val();
            var yammerappid = $('#yammerAppId').val();

            var passedObj = {
                id: id,
                tenantId: tenantId,
                office365AppId: office365AppId,
                yammerappid: yammerappid,
                loginError: ' ',
                retries:0
            };

            saveValueToList(passedObj);
        }
    });
}

//20161007, change param to obj
function saveValueToList(obj, noReload, callback) {

    if (obj.id) {
        $().SPServices({
            operation: "UpdateListItems",
            async: true,
            batchCmd: "Update",
            listName: listName,
            ID: obj.id,
            valuepairs: generateValuePairs(configMapper, obj),
            completefunc: function (xData, Status) {
                if (noReload) {
                    if (typeof callback == 'function') {
                        callback();
                    }
                }
                else {
                    location.reload();
                }
            }
        });

    }
    else {
        $().SPServices({
            operation: "UpdateListItems",
            batchCmd: "New",
            listName: listName,
            valuepairs: generateValuePairs(configMapper, obj),
            completefunc: function (xData, Status) {
                if (noReload) {
                    if (typeof callback == 'function') {
                        callback();
                    }
                }
                else {
                    location.reload();
                }
            }
        });

    }
}

function generateValuePairs(mapper, obj) {
    var value = [];
    for (var key in mapper) {
        if (mapper.hasOwnProperty(key)) {
            if (mapper[key].type == 'number') {
                if (typeof obj[key] == 'number') {
                    value.push([mapper[key].internalName, obj[key]]);
                }
            }
            else {
                if (obj[key]) {
                    value.push([mapper[key].internalName, obj[key]]);
                }
            }
        }
    }
    return value;
}


function updateRSSFeedPreference(title) {
    var id = null;

    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: RSSFeedPreference,
        CAMLViewFields: "<ViewFields><FieldRef Name='UserEmail' /></ViewFields>",
        CAMLQuery: "<Query><Where><Eq><FieldRef Name='UserEmail'/><Value Type='Text'>" + userEmail + "</Value> </Eq> </Where></Query>",
        CAMLRowLimit: 1,
        completefunc: function (xData, Status) {
            $(xData.responseXML).SPFilterNode("z:row").each(function () {
                id = $(this).attr("ows_ID");
            });

            savePreferenceToList(id,title);
        }
    });
}

function savePreferenceToList(id,title) {
    if (id) {
        $().SPServices({
            operation: "UpdateListItems",
            async: true,
            batchCmd: "Update",
            listName: RSSFeedPreference,
            ID: id,
            valuepairs: [["UserEmail", userEmail], ["RSSFeedTitle", title]],
            completefunc: function (xData, Status) {

            }
        });

    }
    else {
        $().SPServices({
            operation: "UpdateListItems",
            batchCmd: "New",
            listName: RSSFeedPreference,
            valuepairs: [["UserEmail", userEmail], ["RSSFeedTitle", title]],
            completefunc: function (xData, Status) {
                
            }
        });

    }
}

function updateRSSFeedConfig() {
    //delete everything first
    if (rssFeedUrl.length > 0) {
        batchDeleteRSSFeedConfig(function () {
            batchInsertRSSFeedConfig(0);
        });
    }
    //reset config
    else {
        batchDeleteRSSFeedConfig();
    }
}

function batchDeleteRSSFeedConfig(callback) {
    $().SPServices.SPUpdateMultipleListItems({
        listName: RSSFeedList,
        CAMLQuery: "<Query></Query>",
        batchCmd: "Delete",
        completefunc: function (xData, Status) {
            if (typeof callback === 'function') {
                callback();
            }
        }
    });
}

function batchInsertRSSFeedConfig(i) {
    $().SPServices({
        operation: "UpdateListItems",
        batchCmd: "New",
        listName: RSSFeedList,
        valuepairs: [["Title", rssFeedUrl[i].title], ["RSSFeedTitle", rssFeedUrl[i].title], ["RSSFeedURL", rssFeedUrl[i].url], ["ShortDescription", rssFeedUrl[i].desc], ["isActive", rssFeedUrl[i].active]],
        completefunc: function (xData, Status) {
            if (rssFeedUrl.length > (i + 1)) {
                return batchInsertRSSFeedConfig(i + 1);
            }
        }
    });
}



//get user email
function getUserEmail(callback) {
    SP.SOD.executeOrDelayUntilScriptLoaded(loadUserData, 'SP.UserProfiles.js');
    function loadUserData() {

        var hostUrl = _spPageContextInfo.siteAbsoluteUrl.replace(/(-[\w]*)/, '');
        var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;

        context = new SP.ClientContext.get_current();
        var myweb = context.get_web();
        var currentUser = myweb.get_currentUser();
        context.load(currentUser);
        context.load(myweb, "EffectiveBasePermissions");
        //context.executeQueryAsync(Function.createDelegate(this, this.onSuccessMethod), Function.createDelegate(this, this.onFailureMethod));
        context.executeQueryAsync(function () {

            //show config button for user with the permission, site collection admin
            //cannot get site admin with current App Permission
            //&& currentUser.get_isSiteAdmin()
            if (myweb.get_effectiveBasePermissions().has(SP.PermissionKind.manageWeb)) {
                $("#popupconfig").css("display", "block");
                $("#rssconfig").css("display", "block");
            }

            $('body').append('<div id="hidden-email"></div>');

            userEmail = currentUser.get_email();
            $('#hidden-email').html(userEmail);

            //Get Instance of People Manager Class
            var peopleManager = new SP.UserProfiles.PeopleManager(context);

            //Get properties of the current user
            var userProfileProperties = peopleManager.getMyProperties();

            context.load(userProfileProperties);
            context.executeQueryAsync(function () {
                //for onedrive path
                $('body').append('<div id="personal-url" style="display:none;"></div>');

                personalUrl = userProfileProperties.get_personalUrl();
                $('#personal-url').html(personalUrl);

                if (typeof callback === 'function') {
                    callback();
                }

                var firstName = userProfileProperties.get_userProfileProperties()['FirstName'];
                var lastName = userProfileProperties.get_userProfileProperties()['LastName'];

                currentUserName = firstName + ' ' + lastName;

                var thumb = userProfileProperties.get_userProfileProperties()['PictureURL'];

                var thumbUrl = appWebUrl + "/_layouts/15/userphoto.aspx?size=M&url=" + thumb;

                //var htmlIcon = '<div class="custom-icon"><img class="custom-icon-img" src="' + thumbUrl + '"/><div class="custom-icon-name">' + firstName + '<span style="color:#e67300">@productivity</span></div></div>';
                var htmlIcon = '<div class="custom-icon"><img class="custom-icon-img" src="' + thumbUrl + '"/><span class="icon name">' + firstName + '</span><span class="icon productivity" style="color:#e67300">@productivity</span></div>';
                $('#DeltaSiteLogo .ms-siteicon-a').html(htmlIcon);

            }, function (sender, args) {
                console.log('error :' + args.get_message());
            });
        },
        function (sender, args) {
            console.log('error :' + args.get_message());
        });
    }
}

function readRSSFeed(callback) {
    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: RSSFeedList,
        CAMLViewFields: "<ViewFields><FieldRef Name='ID' /><FieldRef Name='RSSFeedTitle' /><FieldRef Name='RSSFeedURL' /><FieldRef Name='ShortDescription' /><FieldRef Name='isActive' /></ViewFields>",
        completefunc: function (xData, Status) {
            //no data
            if ($(xData.responseXML).SPFilterNode("z:row").length == 0) {
                //load default rss feed
                rssFeedUrl.push({
                    title: 'TechCrunch',
                    url: 'http://feeds.feedburner.com/TechCrunch/',
                    active: true,
                    desc: 'Latest tech news and information',
                    id: 1
                });
            }
            else{
                $(xData.responseXML).SPFilterNode("z:row").each(function () {
                        
                    //read all the stored feeds
                        rssFeedUrl.push({
                            title: parseIntoString($(this).attr("ows_RSSFeedTitle")),
                            url: parseIntoString($(this).attr("ows_RSSFeedURL")),
                            active: parseIntoString($(this).attr("ows_isActive")),
                            desc: parseIntoString($(this).attr("ows_ShortDescription")),
                            id: parseIntoString($(this).attr("ows_ID"))
                        });
                });
            }

            if (typeof callback === 'function') {
                callback();
            }
        }
    });
}

function rssPreference(callback) {
    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: RSSFeedPreference,
        CAMLViewFields: "<ViewFields><FieldRef Name='UserEmail' /><FieldRef Name='RSSFeedTitle' /></ViewFields>",
        CAMLQuery: "<Query><Where><Eq><FieldRef Name='UserEmail'/><Value Type='Text'>" + userEmail + "</Value> </Eq> </Where></Query>",
        CAMLRowLimit:1,
        completefunc: function (xData, Status) {
            //no data
            if ($(xData.responseXML).SPFilterNode("z:row").length == 0) {
                //load default rss feed from array
                
            }
            else {
                var feedTitle = "";
                $(xData.responseXML).SPFilterNode("z:row").each(function () {
                    feedTitle = $(this).attr('ows_RSSFeedTitle');
                });

                if(feedTitle !=""){
                    userFeedSelection = getRSSFeedByTitle(rssFeedUrl, feedTitle);

                }
                    
            }
            
            //rssReady = true;

            if (typeof callback === 'function') {
                callback();
            }
        }
    });
}

