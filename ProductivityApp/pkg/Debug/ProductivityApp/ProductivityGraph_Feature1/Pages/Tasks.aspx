﻿<%@ Page language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    
    <!-- Google Font API-->
     <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css' />
     <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>

    <link href="../Content/style/animate.css" rel="stylesheet" />
    <link href="../Content/style/leftmenu.css" rel="stylesheet" />
    <link href="../Content/style/main.css" rel="stylesheet" />
    <link href="../Content/style/sp13custom.css" rel="stylesheet" />
    <link href="../Content/style/sp13custom_header.css" rel="stylesheet" />
    <link href="../Content/style/sp13custom_task.css" rel="stylesheet" />

    <!-- script -->
    
    <script src="../Scripts/lib/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script src="../Scripts/lib/jquery.SPServices-2013.01.js"></script>
    <script src="../Scripts/lib/leftmenu.js"></script>
    <script src="../Scripts/lib/jquery.magnific-popup.min.js"></script>
    
    <!--yammer library-->
    <script type="text/javascript" src="https://c64.assets-yammer.com/assets/platform_js_sdk.js"></script>

    <!-- custom script for setting up app id -->
    <script src="../Scripts/config/Config.js"></script>

     <!-- custom script-->
    <script src="../Scripts/custom/main.js"></script>
    <script src="../Scripts/custom/task.js"></script>

</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    <!--Task Page identifier -->
    <div id="_TaskPage" style="display:none;"></div>
    
    <!-- config popup -->
    <div class="white-popup-block mfp-hide" id="configcontainer">
        <div class="configtable">
            <div class="configrow">
                <div class="configcelltitle">Tenant ID:</div>
                <div class="configcellinput">
                    <input type="text" id="tenantId" name="tenantId" placeholder="" size="100" />                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">Office 365 App ID:</div>
                <div class="configcellinput">
                    <input type="text" id="office365AppId" name="office365AppId" placeholder="" size="100" />                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">Yammer App ID:</div>
                <div class="configcellinput">
                    <input type="text" id="yammerAppId" name="yammerAppId" placeholder="" size="100" />
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">License Key:</div>
                <div class="configcellinput">
                    <input type="text" id="licenseKey" name="licenseKey" placeholder="" size="100" />
                </div>
            </div>
        </div>
        <div class="configbuttons">
        	<input type="button" onclick="saveConfig();" value="Save" />
        </div>        
    </div>
    <!-- end one time config -->

    <!-- main task body -->
            <!--Start Body-->
            <div id="sp13custom-bodyContainer">
                <!-- Start Header -->
                <div id="sp13custom-HeaderContainer" class="row ms-dialogHidden">
                    
                        <div class="sp13custom-Logo col six">
                            <!-- debug purpose --><input type="button" value="RefreshScript" id="debugButton"/>
                            
                                <div id="DeltaSiteLogo">
                                    <a class="ms-siteicon-a" href="Home.aspx">
                                        <!-- for dynamic custom icon -->
                                    </a>

                                </div>
                            
                        </div>
                        <div class="sp13custom-Search col six">
                            <!-- custom productivity rate will be prepended here -->
                            <div class="sp13custom-Search-Centre">
                                <div class="sp13custom-RightSearch">
                                    <div id="searchInputBox">
									    <!-- need to replace search box webpart with normal html-->						
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                
                <!-- End Header -->
                
                <!--Start Content-->
                <div id="sp13custom-contentContainer">
                    
                        <div id="DeltaPlaceHolderMain">

                            <div class="row">
                                <div class="col twelve customMainArea">
                                    <div class="contentwrapper">
									    <div class="innerpagecontent">
										    <div class="tasks-filter-container">
                                               <div class="tasks-filter-title">Task Search: </div>
                                               <input type="text" class="tasks-filter-box" placeholder="Search..."> 
                                               <input type="button" class="tasks-filter-button" value="SEARCH"> 
                                            </div>
                                            <div class="tasks-container">       
                                                <!-- tasks container for others will be loaded here --> 
                                            </div>
								        </div>
							        </div>
						        </div>				
					        </div>
				        </div>
			   
		     </div>						
		    </div>
    <!-- end task body -->


    <!-- left menu -->
        <div class="left_menu ms-dialogHidden" style="display:none">
            <a class="menu-link" href="#menu">
               <img src="../Content/RUI%20img/menu.png" />
            </a>
        </div>
        <div class="left_menu_content" style="display:none">
           <nav id="menu" role="navigation">
 	            <div class="owapanel">
	 	            <div id="messagespanel">
	 		            <div class="messagespanel-heading">
	 			            <span>FLAGGED ITEM</span>
	 		            </div>
	 		            <div id="messageslist">
	 		            </div>
	 	            </div>
	 	            
                    <!--
	 	            <div id="documentpanel">
	 		            <div class="documentpanel-heading">
	 			            <span>RECENT DOC</span>
	 		            </div>
	 		            <div id="documentlist">
	 		            </div>
	 	            </div>
                    -->
 	            </div>
         </nav>
    </div>
    <!-- end left menu -->
</asp:Content>
