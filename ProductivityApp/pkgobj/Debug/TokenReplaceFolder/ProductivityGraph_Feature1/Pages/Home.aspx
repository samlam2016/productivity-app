﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

    <WebPartPages:AllowFraming ID="AllowFraming" runat="server" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1, user-scalable=0" />

    <!-- Add your CSS styles to the following file -->
    <!-- Google Font API-->
     <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css' />
     <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
   
     <link href="../Content/style/font-awesome.min.css" rel="stylesheet" />
   
    <link href="../Content/style/Dropdown.css" rel="stylesheet" />
    <link href="../Content/style/magnific-popup.css" rel="stylesheet" />
    <link href="../Scripts/lib/slick/slick-theme.css" rel="stylesheet" />
    <link href="../Scripts/lib/slick/slick.css" rel="stylesheet" />
    <link href="../Content/style/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Content/style/animate.css" rel="stylesheet" />
    <!--<link href="../Content/style/leftmenu.css" rel="stylesheet" />-->
    <link href="../Content/style/main.css" rel="stylesheet" />
    <link href="../Content/style/sp13custom.css" rel="stylesheet" />
    <link href="../Content/style/opentip.css" rel="stylesheet" />

    <!-- script -->

    <script src="../Scripts/lib/modernizr.min.js"></script>
    <script src="../Scripts/lib/jquery-1.11.3.min.js"></script>

    <!-- ADAL library -->
    <script src="../Scripts/config/adal.js"></script>

    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="/_layouts/15/SP.UserProfiles.js"></script>
    <script src="../Scripts/lib/jquery.SPServices-2013.01.js"></script>
    <script src="../Scripts/lib/slick/slick.js"></script>
    <script src="../Scripts/lib/jquery.magnific-popup.min.js"></script>
    <script src="../Scripts/lib/jquery-ui.min.js"></script>

    <!-- dotdotdot lib -->
    <script src="../Scripts/lib/jquery.dotdotdot.min.js"></script>

    <!-- openTip -->
    <script src="../Scripts/lib/opentip-jquery.min.js"></script>
    
    <!-- global variable -->
    <script type="text/javascript" src="../Scripts/config/globalVar.js"></script>

    <!--yammer library-->
    <script type="text/javascript" src="https://c64.assets-yammer.com/assets/platform_js_sdk.js"></script>

    <!-- custom script for shared scripts in multiple pages -->
    <script src="../Scripts/custom/owa.js"></script>

    <!-- custom script for home page-->   
    <script src="../Scripts/custom/productivityLanding.js"></script>

     <!-- custom script for setting up app id -->
    <script src="../Scripts/config/Config.js"></script>

</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title

</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <!-- home page identifier -->
    <div id="_HomePage" style="display:none;"></div>

    <!-- config popup -->
    <div class="white-popup-block mfp-hide" id="configcontainer">
        <div class="configtitle">Connection Settings</div>
        
        <div class="welcome greeting">Welcome to <span>@Productivity</span> Dashboard!</div>
        <div class="welcome body">In order to use the full functionality of the Add-In, you will need to configure some connection settings below:</div>
        
        <div class="adal-error"></div>

        <div class="configtable">
            <div class="configrow">
                <div class="configcelltitle tenantId">Tenant ID<span class="important"> - Please input this setting</span><div class="welcome desc">(Your Office365 Tenant ID, e.g. celgo.net)</div></div>
                <div class="configcellinput">
                    <input type="text" id="tenantId" name="tenantId" placeholder="" size="100" />                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle azureApp">Azure AD App Client ID<span class="important"> - Please input this setting</span><div class="welcome desc">(Registered client ID of your Azure App for outlook connection, refer to <a href="https://azure.microsoft.com/en-us/documentation/articles/active-directory-integrating-applications/" target="_blank">this link</a> on how to register an application under your Active Directory. To configure the application permission and add credential certificate for our Add-In connection, please refer to <a href="https://celgosystems-public.sharepoint.com/Documents/AzureADGuide%20-%20%40Productivity%20Add-In.pdf" target="_blank">this guide</a>)</div></div>
                <div class="configcellinput">
                    <input type="text" id="office365AppId" name="office365AppId" placeholder="" size="100">                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle yammerId">Yammer App ID<span class="important"> - Please input this setting</span><div class="welcome desc">(Registered client ID in your network to establish yammer connection to the add-in, refer to <a href="https://developer.yammer.com/docs/app-registration" target="_blank">this link</a>. This is optional)</div></div>
                <div class="configcellinput">
                    <input type="text" id="yammerAppId" name="yammerAppId" placeholder="" size="100" />
                </div>
            </div>

            <input type="text" id="loginError" name="loginError" style="display:none;" />
            <input type="text" id="retries" name="retries" style="display:none;" />

            <!--
            <div class="configrow">
                <div class="configcelltitle encryption">Encryption Key<div class="welcome desc">(Optional encryption key to encrypt the tasks created from Productivity Add-In)</div></div>
                <div class="configcellinput">
                    <input type="text" id="encryptionKey" name="encryptionKey" placeholder="" size="100" />
                </div>
            </div>
            -->
        </div>
        <div class="configbuttons">
        	<input class="popup-button" type="button" value="Save" />
        </div>        
    </div>
    <!-- end one time config -->

    <!-- welcome popup -->
    <div class="white-popup-block mfp-hide first-load-popup" id="greetpopup">
        
        <div class="greet greeting">Welcome to <span>@Productivity</span> Dashboard!</div>
        <div class="greet body">@Productivity is a multi-platform app that combines your email, tasks, and reminders into a single workspace available from anywhere.</div>

        <div class="greet">Take a quick look at the features</div>
        
        <div class="greet-button-container">
            <div class="greet button start popup-button">Start</div>
        </div>
    </div>
    <!-->

    <!-- how does it work -->
    <div class="white-popup-block mfp-hide first-load-popup" id="howpopup">
       
        <div class="greet greeting">How does it work?</div>
        <div class="greet body">Take a few minutes to watch our short video.</div>
        <iframe id="videocontainer" frameborder="0" allowfullscreen="1" title="YouTube video player" src="https://www.youtube.com/embed/l2g1s7bp33g?controls=0&amp;showinfo=0&amp;rel=0&amp;loop=1&amp;autoplay=0&amp;enablejsapi=1&amp;widgetid=1"></iframe>
        
        <div class="greet-button-container">
            <div class="greet button next popup-button">Next</div>
        </div>
    </div>

    <!-- features -->
    <div class="white-popup-block mfp-hide first-load-popup" id="featurespopup">
       
        <div class="greet greeting">Overview</div>
        <div class="features-container">
            <!-- Panel Design-->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#e67300">Dashboard Panel</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Panel%20Details.png" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Features in our add-in are designed as dashboard panels for ease of use</li>
                        <li class="feature-list"> Clicking <span style="color:rgb(255,128,0);">the panel heading</span> will refresh the panel content.</li> 
                        <li class="feature-list"> <span style="color: rgb(0,128,64);">The panel body</span> shows the content in a slider display. Click the item to open a popup containing the detailed info and to perform actions.</li>
                    </ul>
                </div>
            </div>
            <!-- Schedule-->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#DE974B">Schedule</div>
                <!--<div class="feature-content">Showing your schedules in the next 7 days</div>-->
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide9.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing calendar events from Microsoft Outlook for the following 7 days.</li>
                    </ul>
                </div>
            </div>
            <!-- Flagged Email-->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#BD97AD">Flagged Email</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide2.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing flagged emails from Microsoft Outlook from the past 7 days.</li>
                    </ul>
                </div>
            </div>
            <!-- Tasks
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#BA9B53">Task</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide3.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing your personal tasks and tasks that you have assigned to others.</li> 
                    </ul>
                </div>
            </div>
            -->
            <!-- Yammer -->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#699976">Yammer</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide4.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing unread yammer messages and the last reply in a streamlined panel.</li> 
                    </ul>
                </div>
            </div>
            <!-- Recent Docs -->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#DE974B">Recent Docs</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide5.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing your latest modified OneDrive documents.</li> 
                    </ul>
                </div>
            </div>
            <!-- Followed Sites 
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#CDBA54">Followed Sites</div>
                <div class="feature-img">
                    <img src="../Content/Screencap/Slide6.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing sites that you followed in SharePoint.</li> 
                    </ul>
                </div>
            </div>
            -->
            <!-- News -->
            <div class="feature-wrapper">
                <div class="feature-title" style="color:#49769E">News</div>
                <div class="feature-img"><img src="../Content/Screencap/Slide7.PNG" /></div>
                <div class="feature-content">
                    <ul>
                        <li class="feature-list"> Showing news from your selected RSS Feed source.</li> 
                        <li class="feature-list"> User will be able to individually select the news source by clicking Select News button.</li> 
                        <li class="feature-list"> Admin will be able to add, edit, or delete news source by clicking RSS config button.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="greet-button-container">
            <div class="greet button back popup-button">Back</div>
            <div class="greet button config popup-button">Configure Settings</div>
        </div>
    </div>

    <!-- main home page body -->
        <!--Start Body-->
        <div id="sp13custom-bodyContainer" style="display:none;">
            <!-- Start Header -->
            <div id="sp13custom-HeaderContainer" class="row ms-dialogHidden">
                    <div class="sp13custom-Logo col six">
                        
                            <div id="DeltaSiteLogo">
                                <a class="ms-siteicon-a" href="Home.aspx">
                                    <!-- for dynamic custom icon -->
                                </a>  
                            </div>
                    </div>
                    <div class="sp13custom-Search col six">
                        <!-- custom productivity rate will be prepended here -->

                        <!--
                        <div class="sp13custom-Search-Centre">
                            <div class="sp13custom-RightSearch">
                                <div id="searchInputBox">
                                    <input type="text" id="searchInputArea" placeholder="Search..." />
                                    <img id="searchInputButton" src="../Content/RUI%20img/search_icon.png" />
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
            </div>
            <!-- End Header -->
     
            <!--Start Content-->
            <div id="sp13custom-contentContainer">
                    <div id="DeltaPlaceHolderMain">
                        <div class="row">
                            <div class="col twelve customMainArea">
                                <div class="contentwrapper">
                                    <div class="row">
                                        <div class="col twelve">

                            <!-- NGO Demo Banner 
                            <div class="banner-slider-container loading">
                               <div class="prev-button">​​​​<img class="prev-button-img" src="../Content/img/arrow_left.png" alt=""/></div>
                               <div class="next-button"><img class="next-button-img" src="../Content/img/arrow_right.png" alt=""/></div>
                               <div class="banner-modal"></div>
                            </div>
                            -->

                            <!-- yammer login button -->
                            <div id="yamButtonContainer">
                                <div class="yamButtonElement">
                                    <img class="yamButtonImageIcon" src="../Content/img/yammer-icon.png" />
                                    <div class="yamButtonTitle">VIEW YAMMER CONVERSATIONS</div>  
                                </div>
                                <div id="yamButton" class="yamButtonElement">LOGIN</div>
                                <div class="configureSettings yamButtonElement">Configure Yammer ID</div>
                            </div>

                            <!-- panels, all links need to be changed-->
                            <div id="myCalendarPanel" class="panel">
                                <a id="myCalendarHeading" class="panel-heading">
                                    <img class="headingLogo schedule" src="../Content/img/Schedule%20Icon.png" alt=""><div class="headingTitle">SC​HEDULE​</div></a>
                                <div id="myCalendarDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>
                            <div id="myFlaggedEmailsPanel" class="panel">
                                <a id="myFlaggedEmailsHeading" class="panel-heading">
                                    <img class="headingLogo task" src="../Content/img/FlaggedItem.png" alt=""><div class="headingTitle">FLAGGED EMAIL</div></a>
                                <div id="myFlaggedEmailsDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>

                            <!--
                            <div id="myTasksPanel" class="panel">
                                <a id="myTasksHeading" class="panel-heading">
                                    <img class="headingLogo task" src="../Content/img/Task%20Icon.png" alt=""><div class="headingTitle">MY TA​SK​</div></a>
                                <div id="myTasksDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>

                            <div id="assigneeTasksPanel" class="panel">
                                <a id="assigneeTasksHeading" class="panel-heading">
                                    <img class="headingLogo task" src="../Content/img/theirtask.png" alt=""><div class="headingTitle">THEIR TASK</div></a>
                                <div id="assigneeTasksDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>
                            -->

                            <div id="myYammerPanel" class="panel">
                                <a id="myYammerHeading" class="panel-heading">
                                    <img class="headingLogo yammer" src="../Content/img/Yammer%20Icon.png" alt=""><div class="headingTitle">YAMMER</div></a>
                                <div id="myYammerDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>
                            <div id="myRecentDocsPanel" class="panel">
                                <a id="myRecentDocsHeading" class="panel-heading"><img class="headingLogo schedule" src="../Content/img/book.png" alt="">
                                    <div class="headingTitle">RECENT DOC</div>
                                </a>
                                <div id="myRecentDocsDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>

                            <!--
                            <div id="myFollowedSitesPanel" class="panel">
                                <a id="myFollowedSitesHeading" class="panel-heading"><img class="headingLogo site" src="../Content/img/sharepoint-icon.png" alt="">
                                    <div class="headingTitle">FOLLOWED SITES</div>
                                </a>
                                <div id="myFollowedSitesDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>
                            -->

                            <div id="myRSSPanel" class="panel">
                                <a id="myRSSHeading" class="panel-heading"><img class="headingLogo news" src="../Content/img/News%20Icon.png" alt=""><div class="headingTitle">NEWS​</div></a>
                                <div id="myRSSDetails" class="panel-details"><div class="panel-loading"></div></div>
                            </div>
                            
                            <!-- end panel -->

                            <!-- add config button -->
                            <!-- config settings -->
                            <div id="popupconfig" class="configbutton"><div class="imageSettings"><img src="../Content/img/Setting.png" /></div><div class="textSettings">Connection</div></div>
                                            
                             <!-- rss setting -->
                            <div id="rssconfig" class="configbutton"><div class="imageSettings"><img src="../Content/img/Setting.png" /></div><div class="textSettings">RSS</div></div>

                            <!-- select news -->
                            <div id="selectnews" class="configbutton"><div class="imageSettings"><img src="../Content/img/News%20Icon.png" /></div><div class="textSettings">Select News</div></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--End Content-->
    <!-- end home page body -->

    
    <!-- left menu -->
    <!--
        <div class="left_menu ms-dialogHidden" style="display:none">
            <a class="menu-link" href="#menu">
               <img src="../Content/RUI%20img/menu.png" />
            </a>
        </div>
        <div class="left_menu_content" style="display:none">
           <nav id="menu" role="navigation">
 	            <div class="owapanel">
	 	            <div id="messagespanel">
	 		            <div class="messagespanel-heading">
	 			            <span>FLAGGED ITEM</span>
	 		            </div>
	 		            <div id="messageslist">
	 		            </div>
	 	            </div>
	 	
	 	            <div id="documentpanel">
	 		            <div class="documentpanel-heading">
	 			            <span>RECENT DOC</span>
	 		            </div>
	 		            <div id="documentlist">
	 		            </div>
	 	            </div>
 	            </div>
         </nav>
          <div class="local_modal"></div>
    </div>
    -->
    <!-- end left menu -->

    <!-- modal loading overlay -->
    <div class="modal"></div>

</asp:Content>
