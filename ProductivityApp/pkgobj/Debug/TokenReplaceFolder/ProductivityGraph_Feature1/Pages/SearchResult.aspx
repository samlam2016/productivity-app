﻿<%@ Page language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">

    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1, user-scalable=0" />

    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <!-- Add your CSS styles to the following file -->
    <!-- Google Font API-->
     <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css' />
     <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>

    <link href="../Content/style/Dropdown.css" rel="stylesheet" />
    <link href="../Content/style/magnific-popup.css" rel="stylesheet" />
    <link href="../Content/style/animate.css" rel="stylesheet" />
    <!--<link href="../Content/style/leftmenu.css" rel="stylesheet" />-->
    <link href="../Content/style/main.css" rel="stylesheet" />
    <link href="../Content/style/sp13custom.css" rel="stylesheet" />
    <!--<link href="../Content/style/sp13custom_header.css" rel="stylesheet" />-->
    <link href="../Content/style/sp13custom_search.css" rel="stylesheet" />

    <!-- script -->
    <script src="../Scripts/lib/modernizr.min.js"></script>
    <script src="../Scripts/lib/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="/_layouts/15/SP.UserProfiles.js"></script>
    <script src="../Scripts/lib/jquery.SPServices-2013.01.js"></script>
    <!--<script src="../Scripts/lib/leftmenu.js"></script>-->
    <script src="../Scripts/lib/jquery.magnific-popup.min.js"></script>
    
    <!--yammer library-->
    <script type="text/javascript" src="https://c64.assets-yammer.com/assets/platform_js_sdk.js"></script>

    <!-- custom script for shared scripts in multiple pages -->
    <script src="../Scripts/custom/owa.js"></script>

     <!-- custom script-->
    <!--<script src="../Scripts/custom/main.js"></script>-->
    <script src="../Scripts/custom/searchResult.js"></script>

    <!-- custom script for setting up app id -->
    <script src="../Scripts/config/Config.js"></script>

</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    <!-- search page identifier -->
    <div id="_SearchPage" style="display:none;"></div>

    <!-- config popup -->
    <div class="white-popup-block mfp-hide" id="configcontainer">
        <div class="configtitle">Connection Settings</div>
        <div class="configtable">
            <div class="configrow">
                <div class="configcelltitle">Tenant ID:</div>
                <div class="configcellinput">
                    <input type="text" id="tenantId" name="tenantId" placeholder="" size="100" />                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">Office 365 App ID:</div>
                <div class="configcellinput">
                    <input type="text" id="office365AppId" name="office365AppId" placeholder="" size="100" />                     
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">Yammer App ID:</div>
                <div class="configcellinput">
                    <input type="text" id="yammerAppId" name="yammerAppId" placeholder="" size="100" />
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">License Key:</div>
                <div class="configcellinput">
                    <input type="text" id="licenseKey" name="licenseKey" placeholder="" size="100" />
                </div>
            </div>
            <div class="configrow">
                <div class="configcelltitle">Encryption Key:</div>
                <div class="configcellinput">
                    <input type="text" id="encryptionKey" name="encryptionKey" placeholder="" size="100" />
                </div>
            </div>
        </div>
        <div class="configbuttons">
        	<input type="button" onclick="saveConfig();" value="Save" />
        </div>        
    </div>
    <!-- end one time config -->


    <!-- main result body -->
            <!--Start Body-->
            <div id="sp13custom-bodyContainer" style="display:none">
                <!-- Start Header -->
                <div id="sp13custom-HeaderContainer" class="row ms-dialogHidden">
                    
                        <div class="sp13custom-Logo col six">
                            <!-- debug purpose --><input type="button" value="RefreshScript" id="debugButton" style="display:none;"/>
                            
                                <div id="DeltaSiteLogo">
                                    <a class="ms-siteicon-a" href="Home.aspx">
                                        <!-- for dynamic custom icon -->
                                    </a>

                                </div>
                            
                        </div>
                        <div class="sp13custom-Search col six">
                            <!-- custom productivity rate will be prepended here -->
                            <div class="sp13custom-Search-Centre">
                                <div class="sp13custom-RightSearch">
                                    <div id="searchInputBox">
									    <!-- need to replace search box webpart with normal html-->						
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                
                <!-- End Header -->
                
                <!--Start Content-->
                <div id="sp13custom-contentContainer">
                    
                        <div id="DeltaPlaceHolderMain">

                            <div class="row">
                                <div class="col twelve customMainArea">
                                    <div class="contentwrapper">
									    <div class="innerpagecontent">
										    <div class="SearchResultContent col twelve">
											    <div class="SearchResultContent_Top">

                                                    <!-- custom search box -->
												    <div class="custom-search-box-container">
												       <input type="text" placeholder="Search..." class="custom-search-box"> 
												       <input type="button" class="custom-search-button" value="SEARCH">
												    </div>
																		
											    </div>
											    <div class="SearchResultContent_Bottom">
												    <!--refiner
                                                    <div class="SearchResultContent_Left">
													   
												    </div>
                                                    -->

											    <div class="SearchResultContent_Right">
												    <!-- search result boxes -->
                                                    <div class="search-box-container sharepoint-box">	
													    <div class="search-box-expand">SHAREPOINT 
														    <div class="expand-button expand">+</div>
													    </div>
													    <div class="search-box-content" style="padding-top: 15px;">
													    </div>
												    </div>
                                                    
												    <div class="search-box-container yammer-box">	
													    <div class="search-box-expand">YAMMER 
														    <div class="expand-button expand">+</div>
													    </div>
													    <div class="search-box-content" style="padding-top: 15px;">
													    <div id="yamButtonContainer"><div class="yamButtonElement"><img class="yamButtonImageIcon" src="../Content/img/yammer-icon.png" /><div class="yamButtonTitle">VIEW YAMMER SEARCHES</div></div><div id="yamButton" class="yamButtonElement">LOGIN</div></div>
                                                        </div>
												    </div>

												    <div class="search-box-container onedrive-box">	
													    <div class="search-box-expand">ONEDRIVE<!--& NOTE--> 
														    <div class="expand-button expand">+</div>
													    </div>
													    <div class="search-box-content" style="padding-top: 15px;">
													    </div>
												    </div>

												    <div class="search-box-container mail-box">	
													    <div class="search-box-expand">MAIL 
														    <div class="expand-button expand">+</div>
													    </div>
													    <div class="search-box-content" style="padding-top: 15px;">
													    </div>
												    </div>
									    </div>

									    </div>
								    </div>
								    </div>
							    </div>
						    </div>				
					    </div>
				    </div>
			   
		     </div>						
		    </div>
    <!-- end search body -->
</asp:Content>
