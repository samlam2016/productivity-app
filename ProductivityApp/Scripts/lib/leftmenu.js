﻿_spBodyOnLoadFunctionNames.push("loadleftmenu");


function loadleftmenu() {

	var InEditMode =(document.getElementById('MSOLayout_InDesignMode').value == 1) ? true : false;
	
	var IsDlg = (getQueryStringParameter("IsDlg") == "1") ? true : false;
	
	if (IsDlg)
	{
		$("#s4-bodyContainer").css("-ms-transform","none");
		$("#s4-bodyContainer").css("transform","none");
		$("#sp13custom-contentContainer").css("background-color","transparent");
		$("#sp13custom-contentContainer").css("border-top","0px solid transparent");
		
	}
	
	if (!InEditMode && !IsDlg){
		$(".left_menu_content").last().addClass( "loaded" );
		$('.left_menu_content').prependTo('body');
		$('.left_menu').prependTo('body');
		$('#s4-bodyContainer').last().addClass( "added" );
		
		$('.left_menu_content').css('display','block');
		$('.left_menu').css('display','block');
	
	
	
		var menu_height = $('.ms-belltown-anonShow').height();
		$('.left_menu').css('top',menu_height);
		$('.left_menu_content').css('top',menu_height);
		$( window ).resize(function() {
			menu_height = $('.ms-belltown-anonShow').height();
			$('.left_menu').css('top',menu_height);
			$('.left_menu_content').css('top',menu_height);
			
			/*
			if (menu_height < 50){
				
				$('#s4-bodyContainer').css('margin-left','50px');
				$('#s4-bodyContainer').css('width','auto');
				$('.left_menu_content').css('left','-100%');
				$('#s4-workspace').css('padding-left','0px');
			}else{
				$('#s4-bodyContainer').css('margin-left','0px');
				$('#s4-bodyContainer').css('width','auto');
				$('.left_menu_content').css('left','-100%');
				$('#s4-workspace').css('padding-left','0px');
			}
			*/
			
			var cur_width = $(window).width();
			/*if (cur_width > 1490){*/
			/*
			if (cur_width >= 1900){
				if ($(".menu-open")[0]){
				resize_search();
				}else{
					$("body").addClass("menu-open");
					resize_search();
				}
			}else{
			*/
				if ($(".menu-open")[0]){
					$("body").removeClass("menu-open");
					resize_search();
				}else{
					resize_search();
				}
				/*
			}
			*/
			
			/*
			if (cur_width < 768){
				var min_width = cur_width - 53;
				var min_width_px = min_width + "px";
				$("#sp13custom-bodyContainer").css("min-width",min_width_px + " !important");
			}
			*/
			
			if (cur_width < 1900){
				var min_width = cur_width - 53;
				var min_width_px = min_width + "px";
				if (min_width > 1490){
					min_width_px = "1490px";
				}
				$("#sp13custom-bodyContainer").css("min-width",min_width_px);
			}else{
				$("#sp13custom-bodyContainer").css("min-width","1490px");
			}
			
			
		});

		$('.left_menu').bind("click",function(e){
			//
			if ($(".menu-open")[0]){
				$("body").removeClass("menu-open");
				resize_search();
			}else{
				$("body").addClass("menu-open");
				resize_search();
			}
			e.preventDefault();
		});
		
		$('.left_menu_content.loaded').bind("click",function(e){
			if ($(".menu-open")[0]){
				$("body").removeClass("menu-open");
				resize_search();
			}else{
				$("body").addClass("menu-open");
				resize_search();
			}
		});
		
		//handler to close left menu
		$('body').on('click',function(e){
			if(!($(e.target).closest('.left_menu').length>0) && !($(e.target).closest('.left_menu_content').length>0)){
				if ($(".menu-open")[0]){
					e.preventDefault();
					$("body").removeClass("menu-open");
					resize_search();
				}			
			}
		});

		
		var cur_width = $(window).width();
		if (cur_width > 1490){
			$("body").addClass("menu-open");
			
		}
		if (cur_width < 768){
			var min_width = cur_width - 53;
			var min_width_px = min_width + "px";
			$("#sp13custom-bodyContainer").css("min-width",min_width_px);
		}
	
	
	}
	else
	{
		if($('.left_menu_content').length>0){
			$('.left_menu_content').css('display','none');
			$('.left_menu').css('display','none');
		}
	}
}

function resize_search(t){
	var resize_times = t;
	if ($(".sp13custom-Search")[0]){
		
		var search_div_width = $(".sp13custom-Search").width();
		var search_box_width = 0;
		var detect_width = $(window).width();
		var decrease_width = 155;
		if (detect_width <= 480){
			decrease_width = 60;
		}
		if ($(".menu-open")[0]){
			search_box_width = search_div_width - decrease_width;
		}else{
			search_box_width = search_div_width - decrease_width;
		}
		var search_box_width_px = search_box_width + "px";

		$(".ms-textSmall.ms-srch-sb-prompt.ms-helperText").css("width",search_box_width_px);
		
		//console.log($(".ms-textSmall.ms-srch-sb-prompt.ms-helperText").css("width"));
		
		
		if (t === undefined || t === null || t === 0) {
			setTimeout("resize_search(1)",250);
		}
		
		
	}
}

function getQueryStringParameter(paramToRetrieve) {
    try {
        var params = document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i++) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve)
                return singleParam[1];
        }
    }
    catch (err) {
        return "";
    }
}