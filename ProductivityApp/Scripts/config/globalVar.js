﻿/*Global Variables*/
var productCode = "P01";
var userEmail = ""; //global variable for user Email

var configId = -1;
var yammerID = "";
var office365ID = "";   //office 365 app registration
var licenseKey = "";
var tenantId = "";
var retries = 0;

var loginError = '';

var encryptionKey = ""; //for encryption
var personalUrl = ""; //for onedrive path
var currentUserName = "";
var siteUserList = [];
var webPartConfig = {}; //for webpart config
var rssFeedUrl = [];
var userFeedSelection = {};
var rssReady = false;
var endpoint = "https://celgoproductivityws.azurewebsites.net";
var graphEndpoint = "https://graph.microsoft.com/v1.0/";
var graphEndpointBeta = "https://graph.microsoft.com/beta/";

var configMapper = {
    "tenantId": {
        internalName: 'TenantID',
        type: 'text'
    },
    "office365AppId": {
        internalName: "Office365AppID",
        type: 'text'
    },
    "yammerappid": {
        internalName: "YammerAppID",
        type: 'text'
    },
    "loginError": {
        internalName: "loginError",
        type: 'text'
    },
    "retries": {
        internalName: "retries",
        type: 'number'
    }
}