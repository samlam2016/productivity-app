﻿
//global var
var yammerID = "";  
var office365ID = "";
var licenseKey = "";


function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');

	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length,c.length);
	}

	return '';
}

/*
function loadConfigTask() {

    $().SPServices({
        operation: "GetListItems",
        async: true,
        listName: "Productivity Config List",
        CAMLViewFields: "<ViewFields><FieldRef Name='Office365AppID' /><FieldRef Name='YammerAppID' /><FieldRef Name='LicenseKey' /></ViewFields>",
        CAMLRowLimit: 1,
        completefunc: function (xData, Status) {
            //no data
            if ($(xData.responseXML).SPFilterNode("z:row").length == 0) {
                window.location.href = _spPageContextInfo.webAbsoluteUrl + '/Pages/Home.aspx';
            }
            else {
                $(xData.responseXML).SPFilterNode("z:row").each(function () {
                    //store the config in the global var
                    office365ID = $(this).attr("ows_Office365AppID");
                    yammerID = $(this).attr("ows_YammerAppID");
                    licenseKey = $(this).attr("ows_LicenseKey");
                });
                
                taskInit();

            }

        }
    });


}
*/


function taskInit(yammerClientID, office365ClientID, licenseKey) {

        yammerID = yammerClientID;
        office365ID = office365ClientID;
        licenseKey = licenseKey

		var yammerToken = getCookie("yammer_access_token");
		if (yammerToken) {
		    loadCustomIcon(yammerToken);
		    loadOthersTaskData(yammerToken);
		}
		    //redirect to yammer login
		else {
		    window.location.href = _spPageContextInfo.webAbsoluteUrl + '/Pages/Yammer-Login.aspx' + "#k=" + yammerID;
		}

		filterHandler();
		//expandHandler();
}

//old function 
/*
function filterHandler(){
	$('.tasks-filter-button').on('click',function(){
		$('.tasks-content .tasks-content-empty').remove();
		$('.task-item').attr('style',null);
		$('.task-item').removeClass('filtered');
		if(!($('.tasks-filter-box').val() == '')){
			var queryString = $('.tasks-filter-box').val().trim().toLowerCase();
			$('.task-item').each(function(index){
				if($(this).find('.task-content-subject').html().toLowerCase().indexOf(queryString)==-1){
					$(this).css('display','none');
					$(this).addClass('filtered');
				}
			});
		}
		else{
			$('.tasks-content .tasks-content-empty').remove();
			$('.task-item').attr('style',null);
			$('.task-item').removeClass('filtered');
		}
		
		var tabmLength = $('.tasks-content.tabm').find('.task-item').not($('.filtered')).length;
		var tatmLength = $('.tasks-content.tatm').find('.task-item').not($('.filtered')).length;
		console.log(tabmLength);
		console.log(tatmLength);
		if(tabmLength == 0){
			$('.tasks-content.tabm').append('<div class="tasks-content-empty">No task found</div>');
		}
		
		if(tatmLength==0){
			$('.tasks-content.tatm').append('<div class="tasks-content-empty">No task found</div>');
		}
	});
}
*/

function filterHandler() {
    $('.tasks-filter-button').on('click', function () {
        $('.tasks-content .tasks-content-empty').remove();
        $('.task-content-row').attr('style', null);
        $('.task-content-row-header').attr('style', null);

        $('.task-content-row').removeClass('filtered');
        if (!($('.tasks-filter-box').val() == '')) {
            var queryString = $('.tasks-filter-box').val().trim().toLowerCase();
            $('.task-content-row').each(function (index) {
                //console.log($(this).text());
                if ($(this).text().toLowerCase().indexOf(queryString) == -1) {
                    $(this).css('display', 'none');
                    $(this).addClass('filtered');
                }
            });
        }
        else {
            $('.tasks-content .tasks-content-empty').remove();
            $('.task-content-row').attr('style', null);
            $('.task-content-row').removeClass('filtered');
        }

        $('.tasks-content').each(function () {
            var taskLength = $(this).find('.task-content-row').not($('.filtered')).length;
            console.log(taskLength);
            if (taskLength == 0) {
                $(this).find('.task-content-row-header').attr('style', 'display:none');
                $(this).append('<div class="tasks-content-empty">No task found</div>');
            }
        });
    });
}


function clickHandler() {
    $('.task-content-row').on('click', function () {
        var obj = {
            subject: $(this).find('.task-content-col.subject').text(),
            status: $(this).find('.task-content-col.status').text(),
            assignedBy: $(this).find('.assignedBy').text(),
            assignedTo: $(this).find('.assignedTo').text(),
            assignedByEmail: $(this).find('.assignedByEmail').text(),
            assignedToEmail: $(this).find('.assignedToEmail').text(),
            flexible: $(this).find('.flexible').text(),
            startDate: $(this).find('.task-content-col.created').text(),
            endDate: $(this).find('.task-content-col.due').text(),
            taskStatus: $(this).find('.taskStatusId').text(),
            description: $(this).find('.task-content-col.desc').text(),
            timestamp: $(this).find('.timestamp').text(),
            plusDay: $(this).find('.plusDay').text(),
            //reminderDate: $(this).find('.reminderDate').text(),
            messageId: $(this).find('.messageId').text(),
            threadId: $(this).find('.threadId').text(),
            //category: $(this).find('.task-slider-content-category').text(),
            form: $(this).find('.form').text(),
            type: $(this).find('.type').text(),
            assigneeDueDate: $(this).find('.assigneeDueDate').text(),
            currentDisplay: $(this).find('.task-content-col.status').text()
        };

        loadTabmPopUp(obj);

    });
}

function loadTabmPopUp(obj, reload) {
    try {

        if (!reload) {
            $('body').addClass('loading');
        }


        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

        var popUpHTML = "";

        var extension = obj.plusDay + ' day';
        if (obj.plusDay > 1) {
            extension = obj.plusDay + ' days';
        }


        popUpHTML = '<div class="popup-container">' + '<div class="popup-title task">' + 'TASK' //obj.category
            + '</div>' + '<div class="popup-subject">' + obj.subject //msg.Subject
            + '</div>' + '<table class="popup-content">' + '<tr>' + '<td class="popup-content-header">Assigned By:</td>'
            //+	'<td class="popup-content-body">'+msg.Sender.EmailAddress.Name+'</td>'
            + '<td class="popup-content-body">' + obj.assignedBy + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Assigned To:</td>' + '<td class="popup-content-body">' + obj.assignedTo + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Task Description:</td>'
            //BodyPreview the plain text version
            + '<td class="popup-content-body">' + obj.description + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Start Date:</td>' + '<td class="popup-content-body">' + obj.startDate + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Due Date:</td>' + '<td class="popup-content-body">' + obj.endDate + '</td>' + '</tr>';

        if (obj.status == 'Delayed') {
            popUpHTML += '<tr>' + '<td class="popup-content-header">Original Due:</td>' + '<td class="popup-content-body">' + obj.assigneeDueDate + '</td>' + '</tr>' + '<tr>' + '<td class="popup-content-header">Extension:</td>'
                //BodyPreview the plain text version
                + '<td class="popup-content-body">' + extension + '</td>' + '</tr>';
        }


        popUpHTML += '</table>';


        popUpHTML += '<div class="popup-button-container">'
				  + '<div class="popup-button more">View More</div>' + '</div>' + '</div>';


        //remove loading gif
        $('body').removeClass('loading');

        if ($('.mfp-content').length > 0 && reload) {
            //some fade out fade in
            $('.popup-container').addClass('animated custom fadeOutFull');

            $('.popup-container').one('animationend', function () {
                $('.mfp-content').html(popUpHTML);
                $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');
                $('.popup-container').addClass('animated custom fadeInFull');
                popUpButtonHandler();
            });
        } else {
            $.magnificPopup.open({
                items: {
                    src: popUpHTML,
                    type: 'inline'
                },
                closeOnBgClick: false,
                closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close">x</button>',
                callbacks: {
                    open: function () {
                        //add event handlers on the button
                        popUpButtonHandler();
                    }
                }
            });
        }

        function popUpButtonHandler() {

            $('.popup-button.more').on('click', function () {
                if (obj.form == null || obj.form == '' || obj.form == undefined || obj.form == -1 || obj.form == 'null') {
                    $.magnificPopup.close();
                    $('body').addClass('loading');
                    var yammerToken = getCookie("yammer_access_token");
                    if (yammerToken) {
                        loadYammerRelatedMessages(yammerToken, obj.messageId, obj.threadId);
                    } else {
                        oauth_yammer_init();
                    }
                } else {
                    //window.open=obj.form;
                    console.log('error: Form cannot be TABM');
                }
            });
        }
    } catch (err) {
        //remove loading gif
        $('body').removeClass('loading');
        console.log("Exception: " + err.message);
    }
}


function loadYammerRelatedMessages(token, messageID, threadID, reload) {
    try {
        if (!reload) {
            $('body').addClass('loading');
        }
        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/messages/in_thread/" + threadID + ".json",
            method: "GET",
            success: function (msg) {
                var messages = msg.messages;

                var popUpHTML = '<div class="popup-container"><div class="popup-title yammer">Yammer</div>';

                //messages.forEach(function(a){
                for (var j = messages.length - 1; j >= 0; j--) {
                    var thumb = '';
                    var sender = '';
                    //msg.references
                    for (var i = 0; i < msg.references.length; i++) {
                        if (msg.references[i].id == messages[j].sender_id) {
                            thumb = msg.references[i].mugshot_url;
                            sender = msg.references[i].full_name;
                            break;
                        }
                    }

                    var attachments = messages[j].attachments;
                    var attachmentLink = [];
                    for (var k = 0; k < attachments.length; k++) {
                        attachmentLink.push('<a class="popup-yammer-attachment" href="' + attachments[k].download_url + '">' + attachments[k].name + '</a>');
                    }

                    popUpHTML += '<div class="popup-yammer">' + '<div class="popup-yammer-thumb">' + '<img class="popup-yammer-image" src="' + thumb + '" />' + '</div>' + '<div class="popup-yammer-content">' + '<div class="popup-yammer-sender">' + sender + '</div>' + '<div class="popup-yammer-body">' + messages[j].body.plain + '<div class="popup-yammer-attachment-container">';

                    $(attachmentLink).each(function () {
                        popUpHTML += this;
                    });

                    popUpHTML += '</div>' + '</div>' + '</div>' + '</div>';

                }

                popUpHTML += '<textarea class="popup-yammer-textarea" placeholder="Type your message here..."></textarea>' + '<div class="popup-yammer-error-status"></div>' + '<div class="popup-button-container-yammer">' + '<div class="popup-button send">Send</div>' + '<div class="popup-button unread">Mark As Unread</div>' + '</div>' + '</div>';

                if ($('.mfp-content').length > 0 && reload) {
                    //some fade out fade in
                    $('.popup-container').addClass('animated custom fadeOutFull');

                    $('.popup-container').one('animationend', function () {
                        $('.mfp-content').html(popUpHTML);
                        $('.popup-container').append('<button title="Close (Esc)" type="button" class="mfp-close">x</button>');
                        $('.popup-container').addClass('animated custom fadeInFull');
                        popUpButtonHandler();
                    });
                } else {
                    $.magnificPopup.open({
                        items: {
                            src: popUpHTML,
                            type: 'inline'
                        },
                        closeOnBgClick: false,
                        closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close">x</button>',
                        callbacks: {
                            open: function () {
                                //add event handlers on the button
                                popUpButtonHandler();
                            }
                        }
                    });
                }

                $('body').removeClass('loading');

                function popUpButtonHandler() {
                    $('.popup-button.unread').on('click', function () {
                        var token = GetCookie("yammer_access_token");
                        if (token) {
                            $.magnificPopup.close();
                            $('body').addClass('loading');
                            markAsUnread(token, threadID, true);

                        } else {
                            oauth_yammer_init();
                        }
                    });

                    $('.popup-button.send').on('click', function () {
                        var messageContent = $('.popup-yammer-textarea').val();
                        if (messageContent.trim() == "") {
                            $('.popup-yammer-error-status').html('Please input a message');
                        } else {
                            $('.popup-yammer-error-status').html('Sending...');
                            $('.popup-yammer-error-status').attr('style', 'color:#444;');
                            replyYammerMessage(token, messageContent, messageID, threadID, true);
                        }

                    });
                }

            },
            error: function (error) {
                $('body').removeClass('loading');
                console.log(error);
            }
        });

    } catch (ex) {
        $('body').removeClass('loading');
        console.log(ex.message);
    }
}


function expandHandler() {
    $('.tasks-expand').on('click', function (event) {
        //var parentWebpart = $(this).closest('.tasks-container');
        var parentWebpart = $(this).closest('.tasks-wrapper');

        if ($(this).hasClass('expand')) {
            //hide all the other search boxes

            parentWebpart.find('.tasks-content').animate({ height: "toggle" }, 200, function () {
                parentWebpart.find('.tasks-expand-button').html('–');
                parentWebpart.find('.tasks-expand').toggleClass('expand');
            });
        }
        else {
            parentWebpart.find('.tasks-content').animate({ height: "toggle" }, 200, function () {
                parentWebpart.find('.tasks-expand-button').html('+');
                parentWebpart.find('.tasks-expand').toggleClass('expand');
            });
        }
    });
}


//moved from main.js, to load custom user site icon
function loadCustomIcon(token) {
    try {
        yam.platform.setAuthToken(token);

        yam.platform.request({
            url: "https://api.yammer.com/api/v1/users/current.json",
            method: "GET",

            success: function (obj) {
                var imgURL = obj['mugshot_url'];
                var name = obj['name'];	//full_name
                var email = obj['email'];
                var htmlIcon = '<div class="custom-icon"><img class="custom-icon-img" src="' + imgURL + '"/><div class="custom-icon-name">' + name + '<span style="color:#e67300">@productivity</span></div></div>';
                $('#DeltaSiteLogo .ms-siteicon-a').html(htmlIcon);

                //add a hidden field to put email
                $('body').append('<div id="hidden-email"></div>');
                $('#hidden-email').html(email);


            },
            error: function (msg) {
                console.log("Error");
            }
        });
    }
    catch (err) {
        console.log("Exception: " + err.message);
    }
}

function loadOthersTaskData(token) {
    //load hiearchy table from database
    //get the email addresses of the other people task from the table
    //use the email to grab the task data
    
    var otherEmails = ['tiffany@celgo.net', 'raymond@celgo.net', 'willie@celgo.net'];
    var headingArr = [];

    if (otherEmails.length > 0) {
        try {
            yam.platform.setAuthToken(token);

            yam.platform.request({
                url: "https://api.yammer.com/api/v1/users.json",
                method: "GET",
                success: function (users) {
                    console.log(users);
                    $(users).each(function (index) {
                        if (otherEmails.indexOf(this.email) > -1) {
                            headingArr.push({ mugshot_url: this.mugshot_url, full_name: this.full_name, name: this.first_name, email: this.email });
                        }
                    });


                    var timer = setInterval(function () {
                        if ($('#hidden-email').length > 0) {
                            userEmail = $('#hidden-email').text();
                            generateTaskData(headingArr, userEmail);
                            clearInterval(timer);
                        }
                    }, 200);

                },
                error: function (err) {

                }
            });
        }
        catch (ex) {
            console.log(ex.message);
        }
    }
    else {
        //no data or auth
    }

}


//generate html element for the tasks
function generateTaskData(arr, assigner) {
    try {

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
        for (var a = 0; a < arr.length; a++) {
            //arr.forEach(function (a) {
            //var endpointUrl = 'https://productivitydemows.azurewebsites.net/api/tasks/others';

            var endpointUrl = 'https://productivitydemows.azurewebsites.net/api/tasks/others?assigneeEmail=' + arr[a].email + '&assignerEmail=' + assigner;


            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: false,
                success: function (xhr) {
                    console.log(xhr);

                    //list like content in table format
                    var tableContent = '<table class="task-content-table">'
                                    + '<tr class="task-content-row-header">'
                                    + '<td class="task-content-col-header subject">'
                                    + 'Subject'
                                    + '</td>'
                                    + '<td class="task-content-col-header desc">'
                                    + 'Description'
                                    + '</td>'
                                    + '<td class="task-content-col-header status">'
                                    + 'Status'
                                    + '</td>'
                                    + '<td class="task-content-col-header due">'
                                    + 'Due Date'
                                    + '</td>'
                                    + '<td class="task-content-col-header created">'
                                    + 'Created Date'
                                    + '</td>'
                                    + '</tr>';
                    //+	'</table>';

                    if (xhr.TaskList.length > 0) {
                        $(xhr.TaskList).each(function (index) {
                            var dueDate = new Date(this.DueDate);
                            var dueDateString = months[dueDate.getMonth()] + " " + parseInt(dueDate.getUTCDate()) + ", " + dueDate.getFullYear();

                            var createdDate = new Date(this.Created);
                            var createdDateString = months[createdDate.getMonth()] + " " + parseInt(createdDate.getUTCDate()) + ", " + createdDate.getFullYear();

                            var assigneeDueDate = new Date(this.AssigneeDueDate);
                            var assigneeDueDateString = months[assigneeDueDate.getMonth()] + " " + parseInt(assigneeDueDate.getUTCDate()) + ", " + assigneeDueDate.getFullYear();


                            tableContent += '<tr class="task-content-row">'
                                    + '<td class="task-content-col subject">'
                                    + this.TaskName
                                    + '</td>'
                                    + '<td class="task-content-col desc">'
                                    + this.Description
                                    + '</td>'
                                    + '<td class="task-content-col status">'
                                    + this.CurrentDisplay
                                    + '</td>'
                                    + '<td class="task-content-col due">'
                                    + dueDateString
                                    + '</td>'
                                    + '<td class="task-content-col created">'
                                    + createdDateString
                                    + '</td>'
                                    + '<td class="hidden-data" style="display:none">'
                                    + '<div class="assigneeDue">'
                                    + assigneeDueDateString
                                    + '</div>'
                                    + '<div class="assignedBy">'
                                    + this.AssignedBy
                                    + '</div>'
                                    + '<div class="assignedTo">'
                                    + this.AssignedTo
                                    + '</div>'
                                    + '<div class="assignedByEmail">'
                                    + this.AssignedByEmailAddress
                                    + '</div>'
                                    + '<div class="assignedToEmail">'
                                    + this.AssignedToEmailAddress
                                    + '</div>'
                                    + '<div class="timestamp">'
                                    + this.AssignmentTimestamp
                                    + '</div>'
                                    + '<div class="taskStatusId">'
                                    + this.TaskStatusID
                                    + '</div>'
                                    + '<div class="threadId">'
                                    + this.ThreadID
                                    + '</div>'
                                    + '<div class="messageId">'
                                    + this.MessageID
                                    + '</div>'
                                    + '<div class="type">'
                                    + this.Type
                                    + '</div>'
                                    + '<div class="form">'
                                    + this.Form
                                    + '</div>'
                                    + '<div class="plusDay">'
                                    + this.PlusDay
                                    + '</div>'
                                    + '<div class="flexible">'
                                    + this.Flexible
                                    + '</div>'
                                    + '</td>'
                                    + '</tr>';
                        });

                        tableContent += '</table>';
                    }
                    else {
                        tableContent = '<div class="task-content-empty">No Assigned Task</div>';
                    }

                    var nth = 'odd';
                    if (a % 2 == 0) {
                        nth = 'even';
                    }

                    var title = arr[a].name + "'s Tasks";

                    var html = '<div class="tasks-wrapper">'
                            + '<div class="tasks-expand ' + nth + '">'
                            + '<img class="tasks-mugshot-image" src="' + arr[a].mugshot_url + '" />'
                            + '<div class="tasks-assignee-name">' + title + '</div>'
                            + '<div class="tasks-expand-button">–</div>'
                            + '</div>'
                            + '<div class="tasks-content">'
                            + tableContent
                            + '</div>'
                            + '</div>';

                    $('.tasks-container').append(html);

                },
                error: function (xhr) {
                    console.log(xhr);
                }
            });
            //});
        }

        //container expand/collapse handler
        expandHandler();

        //task clicked handler
        clickHandler();

    }
    catch (ex) {
        console.log(ex);
    }
}


//old function, load dummy data and gen task item similar to home page
function loadDummy() {
var data = [];
var data2 = [];
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
	/*
	for(var i =0;i<11;i++){
		dummy.push({due:"20151110", start:"20151103", category:"Assigned To: Peter Chow", subject:"Celgo: Test TABM", 
		assignedBy:'Peter Chan', assignedTo:'Peter Chow', 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",		
		status:"Task Assigned By Me"});
		
		dummy2.push({due:"20151110", start:"20151103", category:"Assigned By: Peter Chow", subject:"Celgo: Test TATM", 
		assignedBy:'Peter Chow', assignedTo:'Peter Chan', 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",		
		status:"Task Assigned To Me"});
	}
	
	dummy.push({due:"20151110", start:"20151103", category:"Assigned To: Peter Chow", subject:"Tissue Box", 
		assignedBy:'Peter Chan', assignedTo:'Peter Chow', 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",		
		status:"Task Assigned By Me"});
	
	dummy2.push({due:"20151110", start:"20151103", category:"Assigned By: Peter Chow", subject:"Tiffany Request", 
		assignedBy:'Peter Chow', assignedTo:'Peter Chan', 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",		
		status:"Task Assigned To Me"});	
	*/
	
	
	var html = "";
	var newTask = true;
	dummy.forEach(function(a){
	
	var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getUTCDate());
	var dueDate = new Date(a.due.substr(0,4), parseInt(a.due.substr(4,2))-1, a.due.substr(6,2));
	var due = parseInt((dueDate-today)/(1000*60*60*24));
	
	var startDate = months[parseInt(a.start.substr(4,2))-1] + " " + parseInt(a.start.substr(6,2)) +", "+a.start.substr(0,4);
	var endDate = months[parseInt(a.due.substr(4,2))-1] + " " + parseInt(a.due.substr(6,2)) +", "+a.due.substr(0,4);

	
	if(due==0){
		due = "Today";
	}
	else if(due < 0){
		due = "Expired";
	}
	else{
		due = due + " Days Left";
	}
	
	html += '<div class="task-item">'
	+		'<div class="task-item-wrapper">'
					+ '<div class="task-date">'
					+	'<div class="task-date-due">'
					+	due
					+	'</div>'
					+	'<div class="task-date-start">'
					+		'<div class="task-date-start-day">'
					+		a.start.substr(6,2)
					+		'</div>'
					+		'<div class="task-date-start-month">'
					+		months[parseInt(a.start.substr(4,2))-1]
					+		'</div>'		
					+	'</div>'
					+ '</div>'
					+	'<div class="task-content">'
					+	'<div class="task-content-category">'
					+	a.category
					+	'</div>'
					+	'<div class="task-content-subject">'
					+	a.subject
					+	'</div>'
					+	'</div>'
					//popup Data
					+	'<div class="popUpData" style="display:none">'
					+		'<div class="description">'
					+		a.description
					+		'</div>'
					+		'<div class="assignedBy">'
					+		a.assignedBy
					+		'</div>'
					+		'<div class="assignedTo">'
					+		a.assignedTo
					+		'</div>'
					+		'<div class="startDate">'
					+		startDate
					+		'</div>'
					+		'<div class="endDate">'
					+		endDate
					+		'</div>'
					+		'<div class="status">'
					+		a.status
					+		'</div>'
					+	'</div>'
					+	'</div>'
					+	'</div>';
	});
	
	$(".tasks-content.tabm").html(html);
	html='';
	
	dummy2.forEach(function(a){
	
	var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getUTCDate());
	var dueDate = new Date(a.due.substr(0,4), parseInt(a.due.substr(4,2))-1, a.due.substr(6,2));
	var due = parseInt((dueDate-today)/(1000*60*60*24));
	
	var startDate = months[parseInt(a.start.substr(4,2))-1] + " " + parseInt(a.start.substr(6,2)) +", "+a.start.substr(0,4);
	var endDate = months[parseInt(a.due.substr(4,2))-1] + " " + parseInt(a.due.substr(6,2)) +", "+a.due.substr(0,4);

	
	if(due==0){
		due = "Today";
	}
	else if(due < 0){
		due = "Expired";
	}
	else{
		due = due + " Days Left";
	}
	
	html += '<div class="task-item">'
	+		'<div class="task-item-wrapper">'
					+ '<div class="task-date">'
					+	'<div class="task-date-due">'
					+	due
					+	'</div>'
					+	'<div class="task-date-start">'
					+		'<div class="task-date-start-day">'
					+		a.start.substr(6,2)
					+		'</div>'
					+		'<div class="task-date-start-month">'
					+		months[parseInt(a.start.substr(4,2))-1]
					+		'</div>'		
					+	'</div>'
					+ '</div>'
					+	'<div class="task-content">'
					+	'<div class="task-content-category">'
					+	a.category
					+	'</div>'
					+	'<div class="task-content-subject">'
					+	a.subject
					+	'</div>'
					+	'</div>'
					//popup Data
					+	'<div class="popUpData" style="display:none">'
					+		'<div class="description">'
					+		a.description
					+		'</div>'
					+		'<div class="assignedBy">'
					+		a.assignedBy
					+		'</div>'
					+		'<div class="assignedTo">'
					+		a.assignedTo
					+		'</div>'
					+		'<div class="startDate">'
					+		startDate
					+		'</div>'
					+		'<div class="endDate">'
					+		endDate
					+		'</div>'
					+		'<div class="status">'
					+		a.status
					+		'</div>'
					+	'</div>'
					+	'</div>'
					+	'</div>';
	});
					
	 $(".tasks-content.tatm").html(html);																	
					
					$('.task-item').on('click', function (event) {
						//remove all other overlay
					    var others = $('.modal-overlay');
					    if(others.length>0){
							    others.each(function(event){
							    	$(this).addClass('animated fadeOut');
							    	$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
							    	$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
						    			
						    			$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').remove();
						    			$(this).closest('.task-item').css('cursor','pointer');
						    			$(this).remove();
						    			
						    		});				
							    });
							}

					    					    
					    var overlay = $('<div class="modal-overlay"></div><div class="circle-plus"></div><div class="circle-view"></div><div class="circle-complete"></div>');
					    
					    if($(this).find('.status').text() == "Task Assigned By Me"){
					    	overlay = $('<div class="modal-overlay"></div><div class="circle-return"></div><div class="circle-view"></div><div class="circle-confirm"></div>');
						} 
					    
						
						overlay.addClass('animated fadeIn');
					   	var parent = $(event.target).closest('.task-item');
					   	
					    parent.css('cursor','auto');
					    parent.append(overlay);
					    $('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideInUp');
			
										    
					    overlay.on('click',function(event){
					    	event.stopPropagation();
					    	if($(event.target).hasClass('modal-overlay')){
					    		$(this).addClass('animated fadeOut');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}
					    	
					    	if($(event.target).hasClass('circle-view')){
					    							
								var element = $(this).parent().find('.task-item-wrapper');
								
								
								var popUpHTML = "";
								popUpHTML += '<div class="popup-container">'
											+	'<div class="popup-title">'
											+	element.find('.status').text()
											+	'</div>'
											+	'<div class="popup-subject">'
											+	element.find('.task-slider-content-subject').text()
											+	'</div>'
											+	'<table class="popup-content">'
											+	'<tr>'
											+	'<td class="popup-content-header">Assigned By:</td>'
											+	'<td class="popup-content-body">'+element.find('.assignedBy').text()+'</td>'
											+	'</tr>'
											+	'<tr>'
											+	'<td class="popup-content-header">Assigned To:</td>'
											+	'<td class="popup-content-body">'+element.find('.assignedBy').text()+'</td>'
											+	'</tr>'
											+	'<tr>'
											+	'<td class="popup-content-header">Task Description:</td>'
											+	'<td class="popup-content-body">'+element.find('.description').text()+'</td>'
											+	'</tr>'
											+	'<tr>'
											+	'<td class="popup-content-header">Start Date:</td>'
											+	'<td class="popup-content-body">'+element.find('.startDate').text()+'</td>'
											+	'</tr>'
											+	'<tr>'
											+	'<td class="popup-content-header">End Date:</td>'
											+	'<td class="popup-content-body">'+element.find('.endDate').text()+'</td>'
											+	'</tr>'
											+	'</table>'
											+	'<div class="popup-button-container">'	
											+	'<input type="button" class="popup-button update" value="UPDATE" />'
											+	'<input type="button" class="popup-button plusOne" value="+1 DAY" />'
											+	'<input type="button" class="popup-button minusOne" value="-1 DAY" />'
											+	'<input type="button" class="popup-button cancel" value="CANCEL" />'
											+	'</div>'
											+	'</div>';
																	
											
							   	
							   	$.magnificPopup.open({
								  items: {
								    src: popUpHTML,
								    type: 'inline'
								  },
								  callbacks: {
								    open: function() {
								      //add event handlers on the button
								      $('.popup-button.update').on('click',function(){
								      	alert('update');
								      });
								      $('.popup-button.plusOne').on('click',function(){
								      	alert('plus');
								      });
								      $('.popup-button.minusOne').on('click',function(){
								      	alert('minus');
								      });
								      $('.popup-button.cancel').on('click',function(){
								      	$.magnificPopup.close();
								      });
								   	}
								  }
								});
								
								
								
								overlay.addClass('animated fadeOut');
							    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
							    		overlay.remove();
							    		parent.css('cursor','pointer');
							    });
							    

					    	}
					    	
					    	if($(event.target).hasClass('circle-plus')){
					    		console.log('plus 1');
	
					    		$(this).addClass('animated slideOutUp');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		$(this).siblings('.modal-overlay').addClass('animated fadeOut');
					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}
					    	
					    	if($(event.target).hasClass('circle-complete')){
					    		console.log('complete');
					    		$(this).addClass('animated slideOutUp');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		$(this).siblings('.modal-overlay').addClass('animated fadeOut');

					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}

					    	
					    	if($(event.target).hasClass('circle-minus')){
					    		console.log('minus 1');
					    		$(this).addClass('animated slideOutUp');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		$(this).siblings('.modal-overlay').addClass('animated fadeOut');

					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}
					    	if($(event.target).hasClass('circle-return')){
					    		console.log('minus 1');
					    		$(this).addClass('animated slideOutUp');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		$(this).siblings('.modal-overlay').addClass('animated fadeOut');

					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}

							if($(event.target).hasClass('circle-confirm')){
					    		console.log('minus 1');
					    		$(this).addClass('animated slideOutUp');
					    		$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp');
					    		$(this).siblings('.modal-overlay').addClass('animated fadeOut');

					    		overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					    		overlay.remove();
					    		parent.css('cursor','pointer');
					    		});
					    	}
					    	
					    });						    
					 });
					  
					  //handler to close overlay when clicked elsewhere
					    $('body').on('click',function(event){
							if(!$(event.target).closest('.task-item').length){
							
								//remove all other overlay
							    var others = $('.modal-overlay');
							    if(others.length>0){
								    others.each(function(event){
								    	$(this).addClass('animated fadeOut');
								    	
								    	$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').addClass('animated slideOutUp'); //fadeOut
								    	
							    		$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){

							    			$(this).siblings('.circle-plus, .circle-complete, .circle-view, .circle-confirm, .circle-return').remove();
							    			$(this).closest('.task-item').css('cursor','pointer');
							    			$(this).remove();
							    			
							    		});				
								    });
							    }
						    }
						});

}



//main function will be called after config
//_spBodyOnLoadFunctionNames.push("taskInit");