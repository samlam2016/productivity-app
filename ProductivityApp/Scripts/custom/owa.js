/* Helper Libraries */
/* codes that will be shared with multiple pages can be put here */


$(document).ready(function () {
    loadSiteUsers();

    //popup button click handler
    $('#popupconfig').on('click', function () {

           if (checkIsNull(office365ID) || checkIsNull(tenantId)) {
                loadDetailedConfig();
            }
            else {
                setTimeout(function () {
                    loadPopUp("#configcontainer", function () {
                        $('.mfp-content #configcontainer .popup-button').on('click', saveConfig);
                    });
                }, 0);
            }
    });

});

function owaInit(callback) {

    /*NGO Demo
    if ($('.banner-slider-container').length > 0) {
        //generate demo banner 
        getBannerContent(runSlick);
    }
    */

    applyAppCSSOverride();
    
    applyColorOverride();

    //try to create a sequence 
    //check token first, then yammer, then load the main func
            //function load(errorCode) {
                checkYammerStatus(callback);
            //}

            //token flow reverted back to always check the token before load content (2 requests)
            //refreshAccessToken(load,load);            
}

function convertUrlIntoLink(string) {
    var pattern = new RegExp(/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-{}]*[\w@?^=%&\/~+#-{}])?/gi);
    string = string.replace(pattern, '<a href="$&" target="_blank">$&</a>');
    return string;
}

function getScriptCache(url, callback) {
    jQuery.ajax({
        type: "GET",
        url: url,
        success: callback,
        dataType: 'script',
        cache:true
    });
}

function parseBool(str) {
    if (typeof str === 'string') {
        if (str == '0') {
            return false;
        }
        else if (str == '1') {
            return true;
        }
        else if (str.toLowerCase() == 'true') {
            return true;
        }
        else if (str.toLowerCase() == 'false') {
            return false;
        }
        else {
            return !!str;
        }
    }
    else if (typeof str === 'number') {
        if (str <= 0) {
            return false;
        }
        else {
            return true;
        }
    }
    else if (typeof str === 'boolean') {
        return str;
    }
}

function checkIsNull(val) {
    if (val == "null" || val == "undefined") {
        return true;
    }

    if (!val) {
        return true;
    }

    return false;
}

function applyColorOverride() {
    //apply bg color for webpart
    if (!checkIsNull(webPartConfig.bgColor)) {
        $('#s4-workspace').css('background-color', webPartConfig.bgColor);
    }

    //apply panel bg color
    if (!checkIsNull(webPartConfig.panelBgColor)) {
        $('.panel-details').css('background-color', webPartConfig.panelBgColor);
    }
}

function applyAppCSSOverride() {
    //override css for webpart
    if (webPartConfig.appPage == 'false') {
        //remove padding
        $('#contentRow').attr('style', 'padding-top:0px;');

        //remove margin
        $('#sp13custom-HeaderContainer').css('margin-bottom', '0px');

        //hide suitebar
        $('#suiteBarDelta').attr('style', 'display:none!important;');

        //hide name header
        $('.sp13custom-Logo.col.six').attr('style', 'display:none;');
        $('.sp13custom-Search-Centre').attr('style', 'display:none');

        //override width
        $('.sp13custom-Search.col.six').css('width', '100%');

        //apply overflow
        $('#s4-workspace').attr('style', 'overflow-y:hidden;');

        //remove padding
        $('#s4-bodyContainer').attr('style', 'padding-left:0px!important;padding-right:0px!important;');
    }
}

function applyProdStatusOverride() {
    if (webPartConfig.appPage == 'false') {
        //add header content class
        $('.header-content').removeClass('clientWebPart');
        $('.header-content').addClass('clientWebPart');

        $('.rate-container').css('width', '100%');
        $('.rate-container').css('padding-top', '0px');
    }

    postHeight();
}

function applyPanelSettings(obj) {
    //panelSelector, settings, callback
    //undefined settings for app page
    if (obj.settings == 'true' || obj.settings == undefined) {
        obj.callback();
    }
    else if (obj.settings == 'false') {
        if (typeof obj.falseCallback === 'function') {
            obj.falseCallback();
        }
        else {
            $(obj.panelSelector).css('display', 'none');
        }
    }
}

//to trim message body and append '...'
function textAbstract(text, length) {
    if (text == null || text =="") {
        return "";
    }
    if (text.length <= length) {
        return text;
    }
    text = text.substring(0, length);
    last = text.lastIndexOf(" ");

    //special handler for 1 word paragraph
    if (last == -1) {
        return text.substring(0, length - 3) + '...';
    }

    text = text.substring(0, last);
    if(text[text.length-1] == "," || text[text.length-1] == ":" || text[text.length-1] == "." ||text[text.length-1] == ";"){
    	text = text.substring(0, text.length-1);
    }
    return text + "...";
}

//to format ampm
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//return html logo img
function getFileLogoHTML(fileType) {
    var library = {
        xls: '../Content/img/excel.png', xlsx: '../Content/img/excel.png', ppt: '../Content/img/ppt.png', pptx: '../Content/img/ppt.png', one: '../Content/img/OneNotes.png', onepkg: '../Content/img/OneNotes.png',
        doc: '../Content/img/word.png', docx: '../Content/img/word.png', pdf: '../Content/img/pdf.png', png: '../Content/img/sharepoint.png', outlook: '../Content/img/Outlook.png'
    };

    if (library[fileType]) {
        return '<img src="' + library[fileType] + '"/>';
    }
    else {
        return '<img src="../Content/img/sharepoint.png" />';
    }
}

//to determine file type
function getFileLogo(fileType) {
    var library = {
        xls: 'excel', xlsx: 'excel', ppt: 'powerpoint', pptx: 'powerpoint', one: 'onenote', onepkg: 'onenote',
        doc: 'word', docx: 'word', pdf: 'pdf', png: 'png'
    };

    if (library[fileType]) {
        return library[fileType];
    }
    else {
        return 'generic';
    }
}

//return 'N/A' for null/undefined/empty string
function checkValue(a,nullVal) {
    if (a == null || a == "" || a == undefined) {
        if (nullVal) {
            return nullVal;
        }
        return 'N/A';
    }
    else {
        return a;
    }
}

//post total height message to iframe parent
function postHeight() {
    //only post when it is deployed in another app
    if (webPartConfig.appPage == 'false') {
        //post height into parent page
        var frameHeight = $('#s4-bodyContainer').height();
        console.log(frameHeight);

        if (webPartConfig.fixedHeight == 'false') {
            parent.postMessage({ 'height': frameHeight }, webPartConfig.SPHostUrl);
        }
        else {
            resizeMessage = '<message senderId=' + webPartConfig.SenderId + '>resize(100%, ' + frameHeight + ')</message>';
            parent.postMessage(resizeMessage, webPartConfig.SPHostUrl);
        }
    }
}

function getDefaultRSSFeed(arr) {
    return arr.filter(function (a) {
        return parseBool(a.active);
    })[0];
}

function getRSSFeedByTitle(arr, title) {
    return arr.filter(function (a) {
        return a.title == title;
    })[0];
}

function getLocalISOString(date) {
    var d;

    if (date) {
        d = date;
    }
    else {
        d = new Date();
    }

    var tzoffset = (d).getTimezoneOffset() * 60000; //offset in milliseconds
    var localISOTime = (new Date(d.getTime() - tzoffset)).toISOString().slice(0, -1);
    return localISOTime;
}

function getUtcISOString(date) {
    var d;

    if (date) {
        d = date;
    }
    else {
        d = new Date();
    }

    var UtcISOTime = d.toISOString();
    return UtcISOTime;
}

//timezone setter
Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
};

//add days
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

function getTimezoneOffset() {
    return -(new Date().getTimezoneOffset() / 60);
}

//can be date object or datestring
function convertUTCToLocale(datestring) {
    var date = null;
    if (typeof datestring === 'string') {
        date = new Date(datestring);
    }
    else {
        date = datestring;
    }

    return date.addHours(getTimezoneOffset());
}


//to get month string from index
function getMonthString(i) {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
    return months[i];
}

function getDateString(d, time) {
    //d.getUTCDate()
    var dateString = getMonthString(d.getMonth()) + ' ' + ("0" + d.getDate()).slice(-2) + ', ' + d.getFullYear();
    if (time) {
        dateString += ' ' + formatAMPM(new Date(d));
    }
    return dateString;
}

function getShortDateString(d, time) {
    //d.getUTCDate()
    var dateString = getMonthString(d.getMonth()) + ' ' + ("0" + d.getDate()).slice(-2) + ', ';
    if (time) {
        dateString += formatAMPM(new Date(d));
    }
    return dateString;
}

function setCookie(cname, cvalue, cexpires) {
    var expires = "";

    //in sec
    if (typeof cexpires == 'number') {
        var d = new Date();
        d.setTime(d.getTime() + (cexpires * 1000));
        expires = "expires=" + d.toUTCString() + ";";
    }
        //in date string
    else if (typeof cexpires == 'string') {
        expires = "expires=" + cexpires + ";";
    }

    document.cookie = cname + "=" + cvalue + "; " + expires + " path=/";
}

function eraseCookie(name) {
    setCookie(name, '', -1);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }

    return '';
}

function getAccessToken() {

    var token = getCookie('graph_access_token');

    //if no token in cookies, return dummy string
    if (checkIsNull(token)) {
        token = 'null';
    }

    return token;
}

function setAccessToken(accessToken) {
    if (accessToken) {
        setCookie('graph_access_token', accessToken);
    }
}

function convertIntoISOString(str) {
    if(str.toLowerCase().endsWith('z')){
        return str;
    }
    else{
        return str + 'Z';
    }
}

//2061006 need to modify this for graph api access token
//main token generation, validation, and refresh function
function refreshAccessToken(callback,error) {

    var accessToken = getAccessToken();

    //if no access token in the cookies, create a new one
    if (checkIsNull(accessToken)) {
        var endpointUrl = endpoint + "/api/token/generate?userId=" + encodeURIComponent(userEmail) + "&productCode=" + encodeURIComponent(productCode) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&tenantId=" + encodeURIComponent(tenantId);
        
        $.ajax({
            url: endpointUrl,
            method: 'GET',
            async: true,
            success: function (json) {

                if (json.StatusCode == 0) {
                    //save access token to cookies
                    setAccessToken(json.result)

                    if (typeof callback === 'function') {
                        callback();
                    }
                }
                //exception error
                else {
                    console.log("Error Code: " + json.error_code + ", message: " + json.error_message);
                    //pass the error to main func
                    if (typeof error === 'function') {
                        error(json.error_code);
                    }
                }
            },
            error: function (err) {
                console.log(err);
                //pass the error to main func
                if (typeof error === 'function') {
                    if (typeof err.error_code === 'string') {
                        error(err.error_code);
                    }
                    else {
                        error(err.message);
                    }
                }
            }
        });
    }
    //validate the token
    else {

        //callback();

        var endpointUrl = endpoint + "/api/token/validate?accessToken=" + encodeURIComponent(accessToken) + "&userId=" + encodeURIComponent(userEmail) + "&productCode=" + encodeURIComponent(productCode) + "&licenseKey=" + encodeURIComponent(licenseKey) + "&tenantId=" + encodeURIComponent(tenantId);

        $.ajax({
            url: endpointUrl,
            method: 'GET',
            async: true,
            success: function (json) {

                if (json.StatusCode != 1) {

                    //access token refreshed
                    if (json.StatusCode == 2) {
                        //save new access token to cookies
                        setAccessToken(json.result)
                    }

                    if (typeof callback === 'function') {
                        callback();
                    }
                }
                //exception error
                else {
                    console.log("Error Code: " + json.error_code + ", message: " + json.error_message);
                    //pass the error to main func
                    if (typeof error === 'function') {
                        error(json.error_code);
                    }
                }
            },
            error: function (err) {
                console.log(err);
                //pass the error to main func
                if (typeof error === 'function') {
                    if (typeof err.error_code === 'string') {
                        error(err.error_code);
                    }
                    else {
                        error(err.message);
                    }
                }
            }
        });
        
    }
}

function checkYammerStatus(callback) {
    
    if (!!yam) {

        if (!checkIsNull(yammerID)) {
            yam.config({ appId: yammerID });

            yam.platform.getLoginStatus(function (response) {
                if (response.authResponse) {
                    setCookie("yammer_access_token", response.access_token.token);
                    if (typeof callback === 'function') {
                        callback();
                    }

                } else {
                    eraseCookie("yammer_access_token");

                    $('#yamButtonContainer').attr('style', 'display:block;');
                    $('.configureSettings.yamButtonElement').hide();

                    $('#yamButton').on('click', function () {
                        console.log('yammerButtonClicked');
                        yam.login(function (response) {
                            if (response.authResponse) {
                                setCookie("yammer_access_token", response.access_token.token);
                                refreshYammer()
                            }
                        });
                    });
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            });
        }
        else {
            //added 20160518, for configure settings
            $('#yamButtonContainer').attr('style', 'display:block;');
            $('#yamButton').hide();

            $('.configureSettings').on('click', function () {
                    loadDetailedConfig('yammer');
            });

            if (typeof callback === 'function') {
                callback();
            }
        }
    }
    else {
        console.log("yam.js library is not included in the app");
    }
}

/*20160912 for Android yammer login*/
function getYammerAuthUrl() {
    //get yammer id
    var yamId = $("#yammerAppId").val();
    var oauthUrl = "https://www.yammer.com/dialog/authenticate?client_id={token}&display=popup";
    var returnVal = "";

    //check if it's null
    if (!checkIsNull(yamId)) {
        returnVal = oauthUrl.replace('{token}', yamId);
    }

    console.log(returnVal);
    return returnVal;
}

function loadSiteUsers() {
    var context = new SP.ClientContext.get_current();
    var userInfoList = context.get_web().get_siteUserInfoList();
    var query = new SP.CamlQuery();
    var viewXml = "<View> \
                    <Query> \
                    </Query> \
                  </View>";
    //<Eq><FieldRef Name='UserName' /><Value Type='Text'>" + userName + "</Value></Eq> \

    query.set_viewXml(viewXml);
    var items = userInfoList.getItems(query);
    context.load(items, 'Include(Department,EMail,FirstName,ID,JobTitle,LastName,Name,UserName)');
    context.executeQueryAsync(function () {
        var length = items.get_count();
        if (length > 0) {
            for (var i = 0; i < length - 1; i++) {
                var item = items.itemAt(i).get_fieldValues();
                if (item.FirstName != null && item.LastName != null) {
                    item.label = item.FirstName + ' ' + item.LastName;
                }
                else if (item.FirstName != null && item.LastName == null) {
                    item.label = item.FirstName;
                }
                else if (item.FirstName == null && item.LastName != null) {
                    item.label = item.LastName;
                }

                siteUserList.push(item);
            }
        }
            //no user found
        else {
            console.log('unable to load site users');
        }
    },
      function (sender, args) {
          console.log("error :" + args.get_message());
      }
    );
}


/*
function loadFlaggedMessages(email) {
    try {

        //add loading gif in left menu panel
        $('.left_menu_content').addClass('loading');

        var maxFlagged = 10;
        var numFlagged = 0;
        var html = "";
        var charLimit = 65;

        var accessToken = getAccessToken();

        //token not found in cookies
        if (checkIsNull(accessToken)) {
            refreshAccessToken(function () {
                loadFlaggedMessages(email);
            }, function (err) {
                if (err == 'EG0004') {
                    $('#messageslist').html('<div class="message-item"><div class="message-empty">You are not authorized to access this feature.</div></div>');
                }
            });
        }
        else {
            var endpointUrl = endpoint + "/api/mails/flagged?accessToken=" + encodeURIComponent(accessToken) + "&productCode=" + encodeURIComponent(productCode) + "&clientId=" + office365ID + "&tenantId=" + tenantId + "&email=" + email;

            $.ajax({
                url: endpointUrl,
                method: 'GET',
                async: true,
                success: function (json) {
                    if (json.StatusCode == 0) {
                        var data = json.result;
                        html = '';
                        numFlagged = 0;
                        $(data).each(function (index) {
                            if (this.Flagged) {
                                var tempBody = textAbstract(this.Body, charLimit);
                                html += '<div class="message-item">'
                                        + '<a href="' + this.WebLink + '" target="_blank">'
                                        + '<div class="message-sender">'
                                        + this.SenderName
                                        + '</div>'
                                        + '<div class="message-subject">'
                                        + this.Subject
                                        + '</div>'
                                        + '<div class="message-bodypreview">'
                                        + tempBody
                                        + '</div>'
                                        + '</a>'
                                        + '</div>';
                                numFlagged++;
                            }
                            if (maxFlagged <= numFlagged) {
                                return false;
                            }
                        });

                        $('.left_menu_content').removeClass('loading');

                        if (numFlagged) {
                            $('#messageslist').html(html);
                        } else {
                            $('#messageslist').html('<div class="message-item"><div class="message-empty">You do not have any flagged mail</div></div>');
                        }
                    }
                        //web service exception error
                    else {
                        //token invalid
                        if (json.error_code == 'ET0001') {
                            refreshAccessToken(function () {
                                loadFlaggedMessages(email);
                            },
                            function (err) {
                                if (err == 'EG0004') {
                                    $('#messageslist').html('<div class="message-item"><div class="message-empty">You are not authorized to access this feature.</div></div>');
                                }
                            });
                        }
                        //no credentials
                        else if (json.error_code == 'EG0004') {
                            $('#messageslist').html('<div class="message-item"><div class="message-empty">You are not authorized to access this feature.</div></div>');
                        }
                        else {
                            $('#messageslist').html('<div class="message-item"><div class="message-empty">You do not have any flagged mail</div></div>');
                        }

                        console.log("Error Code: " + json.error_code + ", message: " + json.error_message);
                        $('.left_menu_content').removeClass('loading');
                    }
                },
                error: function (err) {
                    console.log('error: ' + err.message);
                    $('.left_menu_content').removeClass('loading');
                }
            });
        }
	} 
	catch (err) 
	{
	    //no credentials
	    if (err.message == 'EG0004') {
	        $('#messageslist').html('<div class="message-item"><div class="message-empty">You are not authorized to access this feature.</div></div>');
	    }
	    console.log("Exception: " + err.message);
	    $('.owapanel').removeClass('loading');
	} 
}

function loadRecentDocs() {
    try {
        //add loading gif in left menu panel
        $('.left_menu_content').addClass('loading');

        var appWebUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.webServerRelativeUrl;
        //var endpointUrl = appWebUrl + '/_api/search/query?' + "querytext='Path:" + personalUrl + " IsDocument:true'&selectproperties='ModifiedBy%2cTitle%2cLastModifiedTime%2cFileType%2cPath%2cUniqueId'&rowlimit=5&sortlist='LastModifiedTime:descending'";
        var endpointUrl = appWebUrl + '/_api/search/query?' + "querytext='Path:" + personalUrl + " IsDocument:true'&selectproperties='ModifiedBy%2cTitle%2cLastModifiedTime%2cFileType%2cPath%2cServerRedirectedURL%2cServerRedirectedEmbedURL'&rowlimit=5&sortlist='LastModifiedTime:descending'";

        var day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $.ajax({
            url: endpointUrl,
            method: 'GET',
            dataType: 'json',
            async: true,
            success: function (json) {
                var results = json.PrimaryQueryResult.RelevantResults.Table.Rows;
                if (results.length > 0) {
                    $(results).each(function (index) {
                        var buffer = {};
                        var dataKeys = this.Cells;
                        $(dataKeys).each(function (ind) {
                            buffer[this["Key"]] = this["Value"];
                        });

                        var modifiedBy = buffer["ModifiedBy"].split('\n');

                        //remove empty
                        for (var i = 0; i < modifiedBy.length; i++) {
                            if (modifiedBy[i] == '') {
                                modifiedBy.splice(i,1);
                            }
                        }

                        var modifiedDate = new Date(buffer["LastModifiedTime"]);

                        var modified = "Modified on " + day[modifiedDate.getDay()] + ', ' + getDateString(modifiedDate, true) + " by " + modifiedBy.join(', ');

                        var html = '<div class="document-item">'
                                +'<a href="' + getFilePath(buffer) + '" target="_blank">'
								//+ '<a href="' + buffer["Path"] + '" target="_blank">'
                                + '<div class="document-thumb ' + getFileLogo(buffer["FileType"]) + '">'
                                + '</div>'
                                + '<div class="document-content">'
                                + '<div class="document-title">'
                                + buffer["Title"]
                                + '</div>'
                                + '<div class="document-modified">'
                                + modified
                                + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</div>';

                        $('#documentlist').append(html);
                    });
                }
                else {
                    $('#documentlist').html('<div class="message-item"><div class="message-empty">You do not have any recent documents</div></div>');
                }

                //$('.left_menu_content').removeClass('loading');
            },
            error: function (err) {
                console.log('error: ' + err.message);
                $('.left_menu_content').removeClass('loading');
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }
}
*/

//needs to handle mobile/desktop
function getFilePath(buff) {
    //if (buff["FileType"] == 'one') {
    if (buff["ServerRedirectedURL"]) {
        return buff["ServerRedirectedURL"];
    }
    else if (buff["ServerRedirectedEmbedURL"]) {
        return buff["ServerRedirectedEmbedURL"];
    }
    else if (buff["ServerRedirectedPreviewURL"]) {
        return buff["ServerRedirectedPreviewURL"];
    }
    else {
        return buff["Path"];
    }
}


function ensureUserByEmail(userEmail, success, error) {
    $('.popup-content.addTask .task-loading').attr('style', 'display:inline-block');
    var context = new SP.ClientContext.get_current();
    var userInfoList = context.get_web().get_siteUserInfoList();
    var query = new SP.CamlQuery();
    var viewXml = "<View> \
                    <Query> \
                       <Where> \
                           <Eq><FieldRef Name='EMail' /><Value Type='Text'>" + userEmail + "</Value></Eq> \
                       </Where>  \
                    </Query> \
                    <RowLimit>1</RowLimit> \
                  </View>";
    //<Eq><FieldRef Name='UserName' /><Value Type='Text'>" + userName + "</Value></Eq> \

    query.set_viewXml(viewXml);
    var items = userInfoList.getItems(query);
    context.load(items, 'Include(Department,EMail,FirstName,ID,JobTitle,LastName,Name,UserName)');
    context.executeQueryAsync(function () {
        if (items.get_count() > 0) {
            var item = items.itemAt(0).get_fieldValues();
            if (typeof success === "function") {
                success(item.FirstName + ' ' + item.LastName);
            }
        }
        //no user found
        else {
            console.log('no user found');
            if (typeof error === "function") {
                error();
            }
        }
        $('.popup-content.addTask .task-loading').attr('style', null);
    },
      function (sender, args) {
          console.log("error :" + args.get_message());
          if (typeof error === "function") {
              error();
          }
          $('.popup-content.addTask .task-loading').attr('style', null);
      }
    );
}

function loadCustomIcon(token) {
    try {
            yam.platform.setAuthToken(token);

            yam.platform.request({
                url: "https://api.yammer.com/api/v1/users/current.json",
                method: "GET",

                success: function (obj) {
                    var imgURL = obj['mugshot_url'];
                    var name = obj['name'];	//full_name
                    var email = obj['email'];
                    //var htmlIcon = '<div class="custom-icon"><img class="custom-icon-img" src="' + imgURL + '"/><div class="custom-icon-name"><span class="icon-name">' + name + '</span><span class="icon-productivity" style="color:#e67300">@productivity</span></div></div>';
                    var htmlIcon = '<div class="custom-icon"><img class="custom-icon-img" src="' + imgURL + '"/><span class="icon name">' + name + '</span><span class="icon productivity" style="color:#e67300">@productivity</span></div>';
                    $('#DeltaSiteLogo .ms-siteicon-a').html(htmlIcon);

                    //moved to config.js
                    /*
                    //add a hidden field to put email
                    $('body').append('<div id="hidden-email"></div>');
                    $('#hidden-email').html(email);
                    */
                },
                error: function (msg) {
                    console.log("Error");
                }
            });
    }
    catch (err) {
        console.log("Exception: " + err.message);
    }
}

function pulsate(el) {
    if (typeof el === 'string') {
        el = $(el);
    }
        el.animate({ opacity: 0.3 }, 1000, 'linear')
                     .animate({ opacity: 1 }, 1000, 'linear', function () {
                         pulsate(el)
                     })
                     .click(function () {
                         $(this).animate({ opacity: 1 }, 1000, 'linear');
                         $(this).stop();
                     });
}

//turn non value into empty string, 0 int
function parseIntoString(a) {
    if (typeof a === 'string') {
        return a;
    }

    if (!a) {
        return "";
    }
}

//escape HTML string
String.prototype.escape = function () {
    if (this) {
        var tagsToReplace = {
            //'&': '&amp;',
            '<': '&lt;',
            '>': '&gt;' 
        };
        return this.replace(/[<>]/g, function (tag) {
            return tagsToReplace[tag] || tag;
        });
    }
    else {
        return this;
    }
};

//dropdown list ms
(function ($) {
    $.fn.Dropdown = function () {

        /** Go through each dropdown we've been given. */
        return this.each(function () {

            var $dropdownWrapper = $(this),
                $originalDropdown = $dropdownWrapper.children('.ms-Dropdown-select'),
                $originalDropdownOptions = $originalDropdown.children('option'),
                originalDropdownID = this.id,
                newDropdownTitle = '',
                newDropdownItems = '',
                newDropdownSource = '';

            /** Go through the options to fill up newDropdownTitle and newDropdownItems. */
            $originalDropdownOptions.each(function (index, option) {

                /** If the option is selected, it should be the new dropdown's title. */
                if (option.selected) {
                    newDropdownTitle = option.text;
                }

                /** Add this option to the list of items. */
                newDropdownItems += '<li class="ms-Dropdown-item' + ((option.disabled) ? ' is-disabled"' : '"') + '>' + option.text + '</li>';

            });

            /** Insert the replacement dropdown. */
            newDropdownSource = '<span class="ms-Dropdown-title">' + newDropdownTitle + '</span><ul class="ms-Dropdown-items">' + newDropdownItems + '</ul>';
            $dropdownWrapper.append(newDropdownSource);

            function _openDropdown(evt) {
                if (!$dropdownWrapper.hasClass('is-disabled')) {

                    /** First, let's close any open dropdowns on this page. */
                    $dropdownWrapper.find('.is-open').removeClass('is-open');

                    /** Stop the click event from propagating, which would just close the dropdown immediately. */
                    evt.stopPropagation();

                    /** Before opening, size the items list to match the dropdown. */
                    var dropdownWidth = $(this).parents(".ms-Dropdown").width();
                    $(this).next(".ms-Dropdown-items").css('width', dropdownWidth + 'px');

                    /** Go ahead and open that dropdown. */
                    $dropdownWrapper.toggleClass('is-open');
                    $('.ms-Dropdown').each(function () {
                        if ($(this)[0] !== $dropdownWrapper[0]) {
                            $(this).removeClass('is-open');
                        }
                    });

                    /** Temporarily bind an event to the document that will close this dropdown when clicking anywhere. */
                    $(document).bind("click.dropdown", function (event) {
                        $dropdownWrapper.removeClass('is-open');
                        $(document).unbind('click.dropdown');
                    });
                }
            };

            /** Toggle open/closed state of the dropdown when clicking its title. */
            $dropdownWrapper.on('click', '.ms-Dropdown-title', function (event) {
                _openDropdown(event);
            });

            /** Keyboard accessibility */
            $dropdownWrapper.on('keyup', function (event) {
                var keyCode = event.keyCode || event.which;
                // Open dropdown on enter or arrow up or arrow down and focus on first option
                if (!$(this).hasClass('is-open')) {
                    if (keyCode === 13 || keyCode === 38 || keyCode === 40) {
                        _openDropdown(event);
                        if (!$(this).find('.ms-Dropdown-item').hasClass('is-selected')) {
                            $(this).find('.ms-Dropdown-item:first').addClass('is-selected');
                        }
                    }
                }
                else if ($(this).hasClass('is-open')) {
                    // Up arrow focuses previous option
                    if (keyCode === 38) {
                        if ($(this).find('.ms-Dropdown-item.is-selected').prev().siblings().size() > 0) {
                            $(this).find('.ms-Dropdown-item.is-selected').removeClass('is-selected').prev().addClass('is-selected');
                        }
                    }
                    // Down arrow focuses next option
                    if (keyCode === 40) {
                        if ($(this).find('.ms-Dropdown-item.is-selected').next().siblings().size() > 0) {
                            $(this).find('.ms-Dropdown-item.is-selected').removeClass('is-selected').next().addClass('is-selected');
                        }
                    }
                    // Enter to select item
                    if (keyCode === 13) {
                        if (!$dropdownWrapper.hasClass('is-disabled')) {

                            // Item text
                            var selectedItemText = $(this).find('.ms-Dropdown-item.is-selected').text()

                            $(this).find('.ms-Dropdown-title').html(selectedItemText);

                            /** Update the original dropdown. */
                            $originalDropdown.find("option").each(function (key, value) {
                                if (value.text === selectedItemText) {
                                    $(this).prop('selected', true);
                                } else {
                                    $(this).prop('selected', false);
                                }
                            });
                            $originalDropdown.change();

                            $(this).removeClass('is-open');
                        }
                    }
                }

                // Close dropdown on esc
                if (keyCode === 27) {
                    $(this).removeClass('is-open');
                }
            });

            /** Select an option from the dropdown. */
            $dropdownWrapper.on('click', '.ms-Dropdown-item', function () {
                if (!$dropdownWrapper.hasClass('is-disabled')) {

                    /** Deselect all items and select this one. */
                    $(this).siblings('.ms-Dropdown-item').removeClass('is-selected')
                    $(this).addClass('is-selected');

                    /** Update the replacement dropdown's title. */
                    $(this).parents().siblings('.ms-Dropdown-title').html($(this).text());

                    /** Update the original dropdown. */
                    var selectedItemText = $(this).text();
                    $originalDropdown.find("option").each(function (key, value) {
                        if (value.text === selectedItemText) {
                            $(this).prop('selected', true);
                        } else {
                            $(this).prop('selected', false);
                        }
                    });
                    $originalDropdown.change();
                }
            });

        });
    };
})(jQuery);
